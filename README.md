# Piggy WooCommerce Plugin
The [Piggy for WooCommerce](https://wordpress.org/plugins/piggy-for-woocommerce/) plugin smoothly integrates your webshop with your Piggy loyalty system. Now, your customers can easily save Piggy points on every order they place.

## Start a development environment
To use the development environment you need have the following tools installed on your machine:
- [Ngrok](https://ngrok.com/)
- [Docker](https://www.docker.com/)
- [Yarn](https://yarnpkg.com/)  or [Npm](https://www.npmjs.com/)
- [Composer](https://getcomposer.org/)

To start the development environment, you first need to install the packages.

Install the packages:
```
composer install  -d ./piggy-for-woocommerce

yarn install -d ./piggy-for-woocommerc/public
```


Start the development container:
```
docker-compose -f docker-compose.yml up
```

To develop functionalities for the payment hook, we need to succcessfully do a test order. The payment needs to be done on a valid url, that is why we use ngrok to tunnel our localhost.
```
ngrok http 3000
```

All request made through localhost:3000 is now redirect to [hash].ngrok.io.
You will need to let wordpress know that the site url has changed. 

This is done by the following query:
```
UPDATE wp_options
SET option_value = '[hash].ngrok.io'
WHERE option_name = 'siteurl' 
AND option_name = 'home';
```
### Switch between versions
To switch between versions edit the .env file or use the following command:
```
PFW_PHP_VERSION=7.4 docker-compose up --force-recreate --build
```
##### Version details:
```
PFW_PHP_VERSION       ->     PHP version
PFW_WP_VERSION        ->     Wordpress version
PFW_WC_VERSION        ->     WooCommerce version
PFW_MOLLIE_VERSION    ->     Mollie plugin version
PFW_ENV               ->     the environment variable for wordpress

TEST_PHP_VERISON      ->     PHP version for test container
TEST_WP_VERSION       ->     Wordpress version for test container
TEST_WC_VERSION       ->     WooCommerce version for test container
```
##### .ENV File
```
PFW_ENV=development
PFW_PHP_VERSION=7.4
PFW_WP_VERSION=5.5.0
PFW_WC_VERISON=4.4.1
PFW_MOLLIE_VERSION=5.8.2

TEST_PHP_VERSION=7.3
TEST_WP_VERSION=5.5
TEST_WC_VERSION=4.3.1
```

## Start a testing environment
To run the backend tests use the following command:
```
docker-compose -f docker-compose.tests.yml
```