#!/bin/bash

mkdir -p plugin-build/includes && cp -r piggy-for-woocommerce/includes plugin-build
mkdir -p plugin-build/public && cp -r piggy-for-woocommerce/public/dist/* plugin-build/public
mkdir -p plugin-build/public/images && cp -r piggy-for-woocommerce/public/images/* plugin-build/public/images
mkdir -p plugin-build/src && cp -r piggy-for-woocommerce/src plugin-build
mkdir -p plugin-build/storage/pfw-logs
mkdir -p plugin-build/vendor && cp -r piggy-for-woocommerce/vendor plugin-build
mkdir -p plugin-build/vendor-mozart && cp -r piggy-for-woocommerce/vendor-mozart plugin-build

cp -r piggy-for-woocommerce/piggy-for-woocommerce.php plugin-build
cp -r piggy-for-woocommerce/index.php plugin-build
cp -r piggy-for-woocommerce/readme.txt plugin-build