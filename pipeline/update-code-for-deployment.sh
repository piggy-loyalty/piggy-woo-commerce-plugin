#!/bin/bash

# Change version in code
VERSION=$(git describe --abbrev=0 --tags | sed 's/* //'  );

echo $VERSION;

sed "s/Version:           0.0.0/Version:           ${VERSION}/" piggy-for-woocommerce/piggy-for-woocommerce.php > piggy-for-woocommerce/piggy-for-woocommerce.changed.php &&
mv piggy-for-woocommerce/piggy-for-woocommerce.changed.php piggy-for-woocommerce/piggy-for-woocommerce.php
sed "s/define( 'PFW_VERSION', '0.0.0' );/define( 'PFW_VERSION', '${VERSION}' );/" piggy-for-woocommerce/piggy-for-woocommerce.php > piggy-for-woocommerce/piggy-for-woocommerce.changed.php &&
mv piggy-for-woocommerce/piggy-for-woocommerce.changed.php piggy-for-woocommerce/piggy-for-woocommerce.php

# ENV variable development to production.
sed "s/define( 'PFW_ENV', 'development');/define( 'PFW_ENV', 'production');/" piggy-for-woocommerce/piggy-for-woocommerce.php > piggy-for-woocommerce/piggy-for-woocommerce.changed.php &&
mv piggy-for-woocommerce/piggy-for-woocommerce.changed.php piggy-for-woocommerce/piggy-for-woocommerce.php

sed "s/define( 'PFW_STYLES_PATH', plugin_dir_url( __FILE__ ) . '\/public\/dist\/');/define( 'PFW_STYLES_PATH', plugin_dir_url( __FILE__ ) . '\/public\/');/" piggy-for-woocommerce/piggy-for-woocommerce.php > piggy-for-woocommerce/piggy-for-woocommerce.changed.php &&
mv piggy-for-woocommerce/piggy-for-woocommerce.changed.php piggy-for-woocommerce/piggy-for-woocommerce.php

sed "s/define( 'PFW_SCRIPTS_PATH', plugin_dir_url( __FILE__ ) . '\/public\/dist\/');/define( 'PFW_SCRIPTS_PATH', plugin_dir_url( __FILE__ ) . '\/public\/');/" piggy-for-woocommerce/piggy-for-woocommerce.php > piggy-for-woocommerce/piggy-for-woocommerce.changed.php &&
mv piggy-for-woocommerce/piggy-for-woocommerce.changed.php piggy-for-woocommerce/piggy-for-woocommerce.php