#!/bin/bash

cd ../

bash pipeline/update-code-for-deployment.sh

cd piggy-for-woocommerce/public

yarn install
yarn build:prod

cd ../../
cd piggy-for-woocommerce

rm -rf vendor
rm composer.lock

composer clear-cache
composer install --no-dev --prefer-dist
composer dumpautoload -o -vvv

find . -type f -name bootstrap80.php -exec sed -i .bak -f bootstrap80.php.sed {} +
find . -name "*.bak" -exec rm {} +

cd ../

mkdir -p plugin-build/includes && cp -r piggy-for-woocommerce/includes plugin-build
mkdir -p plugin-build/public && cp -r piggy-for-woocommerce/public/dist/* plugin-build/public
mkdir -p plugin-build/public/images && cp -r piggy-for-woocommerce/public/images/* plugin-build/public/images
mkdir -p plugin-build/src && cp -r piggy-for-woocommerce/src plugin-build
mkdir -p plugin-build/storage/pfw-logs
mkdir -p plugin-build/vendor && cp -r piggy-for-woocommerce/vendor plugin-build
mkdir -p plugin-build/vendor-mozart && cp -r piggy-for-woocommerce/vendor-mozart plugin-build

cp -r piggy-for-woocommerce/piggy-for-woocommerce.php plugin-build
cp -r piggy-for-woocommerce/index.php plugin-build
cp -r piggy-for-woocommerce/readme.txt plugin-build