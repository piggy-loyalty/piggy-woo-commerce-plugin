const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const env = process.env.NODE_ENV;

module.exports = {
    watch: env === "development",
    entry: {
        admin: './src/admin.tsx',
        setup: './src/setup.tsx',
        customer: './src/customer.tsx',
    },
    optimization: {
        splitChunks: {
            chunks: 'all',
            name: "vendor-chunk",
        },
    },
    resolve: {
        plugins: [
            new TsconfigPathsPlugin(),
        ],
        extensions: [".ts", ".tsx", ".js", ".jsx"]
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: [{ loader: "ts-loader" }],
                exclude: [/node_modules/, /\.test.tsx?$/],
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: "css-loader",
                        options: { sourceMap: env !== "production" },
                    },
                    "postcss-loader",
                    "resolve-url-loader",
                    {
                        loader: "sass-loader",
                        options: { sourceMap: true, implementation: require("sass") },
                    },
                ],
            },
            {
                test: /\.css$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: "css-loader",
                        options: { sourceMap: env !== "production" },
                    },
                ],
            },
            {
                test: /\.(woff|woff2)$/,
                use: {
                    loader: "url-loader",
                    options: {
                        mimetype: "application/font-woff",
                    },
                }
            },
            {
                test: /\.(png|jp[e]?g)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            limit: 10000,
                            name: "images/[name].[ext]",
                        },
                    },
                ],
            },
            {
                test: /\.svg$/,
                use: [{
                    loader: "@svgr/webpack",
                    options: {
                        svgoConfig: {
                            plugins: {
                                removeViewBox: false
                            }
                        }
                    }
                }, "url-loader"],
            },
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "[name].css",
            disable: env !== "production",
        }),
    ],
};