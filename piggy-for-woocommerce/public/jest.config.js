// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

module.exports = {
  verbose: true,
  preset: "ts-jest",
  moduleDirectories: ["node_modules", "src"],

  transform: {
    "^.+\\.(ts|tsx)?$": "ts-jest",
    "^.+\\.(js|jsx)$": "babel-jest",
  },

  transformIgnorePatterns: ["node_modules/(?!(@piggy-loyalty|react-toast-notifications)/)"],

  // Automatically clear mock calls and instances between every test
  clearMocks: true,

  // The directory where Jest should output its coverage files
  coverageDirectory: "coverage",

  // Indicates which provider should be used to instrument code for coverage
  coverageProvider: "v8",

  // A map from regular expressions to module names or to arrays of module names that allow to stub out resources with a single module
  moduleNameMapper: {
    '\\.svg$': '<rootDir>/src/tests/__mocks__/file-mock.js',
  },

  // The test environment that will be used for testing
  testEnvironment: "jsdom",

  snapshotSerializers: ["enzyme-to-json/serializer"],

  setupFiles: ["./src/setupTests.js"],
};
