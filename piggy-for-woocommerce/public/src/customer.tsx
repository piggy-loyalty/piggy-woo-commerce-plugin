import React from "react";
import ReactDOM from "react-dom";
import "shared/i18n/i18n";
import "customer/assets/index.sass";
import Customer from "customer/modules";
import {MemoryRouter} from "react-router";

process.env = {
    nonce: env.nonce,
    wp_locale: env.wp_locale,
    component: env.component,
    plugin_dir_url: env.plugin_dir_url,
    base_url: env.base_url,
};

window.addEventListener("load", init);

function init() {
    ReactDOM.render(
        <React.Suspense fallback="">
            <MemoryRouter initialEntries={["/customer/" + process.env.component]}>
                <Customer/>
            </MemoryRouter>
        </React.Suspense>,
        document.getElementById("pfw-customer")
    );
}