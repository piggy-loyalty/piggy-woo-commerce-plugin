import React from "react";
import {Route} from "react-router";
import Credentials from "setup/modules/Credentials";
import HealthCheck from "setup/modules/HealthCheck";
import SelectShop from "setup/modules/SelectShop";

export const Routes = () => {
    return (
        <>
            <Route exact path="/setup/credentials">
                <Credentials />
            </Route>
            <Route path="/setup/healthCheck">
                <HealthCheck />
            </Route>
            <Route path="/setup/shops">
                <SelectShop />
            </Route>
        </>
    );
};