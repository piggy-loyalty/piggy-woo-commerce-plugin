import React from "react";
import {Button, Card, useToast, FormSelect} from "shared/components";
import {useEffect, useState} from "react";
import {formatToSelectValues} from "shared/services/helpers";
import {ShopsRepository} from "shared/repositories";
import {SetupRepository} from "shared/repositories";
import {ShopInterface} from "shared/models";
import {useTrans} from "@piggy-loyalty/foundation/lib/components";

export default function SelectShop() {
	const {t} = useTrans();
	const {errorToast} = useToast();
	const [shops, setShops]: any = useState([]);
	const [selectedShop, setSelectedShop]: any = useState("");
	const [hasShops, setHasShops] = useState(true);
	const [pending, setPending] = useState(false);

	useEffect(() => {
		setPending(true);
		getShops();
	}, []);

	const getShops = () => {
		ShopsRepository.index()
			.then((shops: ShopInterface[]) => {
				setShops(shops);
				setHasShops(shops.length > 0);
				setPending(false);
			})
			.catch((error: any) => {
				errorToast(error);
				setPending(false);
			})
	};

	const submit = (event: any) => {
		event.preventDefault();
		setPending(true);

		SetupRepository.update(selectedShop)
			.then((res: any) => {
				window.location.replace(res.redirect_url);
			})
			.catch((err: any) => {
				errorToast(err);
				setPending(false);
			});
	};

	return (
		<Card title={t("step") + " 3"}>
				{hasShops ? (
                    <form className="setup-form" onSubmit={submit}>
	                    <div className="setup-form__heading">
		                    <h1>{t("selectShop")}</h1>
		                    <p> Only shops with type webshop are visible. </p>
	                    </div>
	                    <div className="setup-form__select">
		                    <FormSelect
								placeholder={"select"}
								name={"shops"}
								value={selectedShop}
								label={t('webshops')}
								onChange={(event: any) => setSelectedShop(event.target.value)}
								options={formatToSelectValues(shops, "id", "name")}
							/>
	                    </div>
						<div className="group setup-form__footer">
							<Button
								type="submit"
								style="primary"
								label={t('chooseShop')}
								disabled={pending}
								pending={pending}
							/>
							<Button
			                    style="primary-ghost"
			                    label={t('refreshShops')}
			                    onClick={() => getShops()}
			                    disabled={false}
			                    pending={false}
		                    />
						</div>
                    </form>
				) : (
					<h4 dangerouslySetInnerHTML={{ __html: t("pleaseCreateShopsFirst", {url: "https://business.piggy.eu"}) }} />
				)}
		</Card>
	)
}