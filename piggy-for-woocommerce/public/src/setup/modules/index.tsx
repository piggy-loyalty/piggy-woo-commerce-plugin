import React from "react";
import { Route, Switch } from "react-router";
import { ToastProvider } from "react-toast-notifications";
import Credentials from "./Credentials";
import HealthCheck from "./HealthCheck";
import SelectShop from "./SelectShop";

export default function Index() {

    return (
        <div id="piggy-for-woocommerce" className="setup">
            <ToastProvider autoDismiss={true}>
                <Switch>
                    <Route exact path="/setup/credentials">
                        <Credentials />
                    </Route>
                    <Route path="/setup/healthCheck">
                        <HealthCheck />
                    </Route>
                    <Route path="/setup/shops">
                        <SelectShop />
                    </Route>
                </Switch>
            </ToastProvider>
        </div>
    );
}