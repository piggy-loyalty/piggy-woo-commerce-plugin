import React from "react";
import {useHistory} from "react-router";
import {Button, Card, useToast} from "shared/components";
import {useState} from "react";
import {SetupRepository} from "shared/repositories";
import {useTrans} from "@piggy-loyalty/foundation/lib/components";

export default function HealthCheck() {
    const history = useHistory();
    const {t} = useTrans();
    const {errorToast} = useToast();
    const [pending, setPending] = useState(false);

    const submit = (event: any) => {
        event.preventDefault();

        SetupRepository.healthCheck()
            .then(() => {
                history.push("/setup/shops");
            })
            .catch( (error: any) => {
                setPending(false);
                errorToast(error);
            });
    };

    return (
        <Card title={t("step") + " 2"}>
            <form className="setup-form" onSubmit={submit}>
                <Button
                    type="submit"
                    style="primary"
                    label={t('doHealthCheck')}
                    disabled={pending}
                    pending={pending}
                />
            </form>
        </Card>
    )
}