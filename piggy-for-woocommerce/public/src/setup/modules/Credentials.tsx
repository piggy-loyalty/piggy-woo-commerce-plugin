import React, {useState} from "react";
import {Card, FormInput, Button, useToast} from "shared/components";
import {useHistory} from "react-router";
import {SetupRepository} from "shared/repositories";
import {useTrans} from "@piggy-loyalty/foundation/lib/components";
import {imageToWpUrl} from "shared/services/helpers";

export default function Credentials() {
	const history = useHistory();
	const {t} = useTrans();
	const {successToast, errorToast} = useToast();
	const [pending, setPending] = useState(false);
	const [credentials, setCredentials] = useState({
		key: "",
		secret: "",
	});
	
	const onChange = (fieldName: string, value: string) => {
		setCredentials((prevState: any) => {
			return ({
				...prevState,
				[fieldName]: value
			})
		});
	};

	const submit = (event: any) => {
		event.preventDefault();

		SetupRepository.authenticate(credentials)
			.then(() => {
				setPending(true);
				history.push("/setup/healthCheck");
			})
			.catch((error: any) => {
				errorToast(error);
				setPending(false);
			});
	};

	return (
		<Card classes={"credentials"} title={t("step") + " 1"}>
			<div className="group">
				<div className="left xs12 sm6">
					<form className="setup-form" onSubmit={submit}>
						<div className="group checkout__heading">
							<div className="xxs12">
								<h1>{t("credentials")}</h1>
							</div>
							<div className="xxs12">
								<FormInput
									label={"ClientID"}
									value={credentials.key}
									name={"key"}
									onChange={(event: any) => onChange("key", event.target.value)}
									required={true}
									outlined
								/>
								<FormInput
									label={"Secret"}
									value={credentials.secret}
									name={"secret"}
									onChange={(event: any) => onChange("secret", event.target.value)}
									required={true}
									outlined
								/>
								<br/>
							</div>
						</div>
						<div className="group checkout__footer">
							<div className="xxs12">
								<Button
									type="submit"
									style="primary"
									label={t('continue')}
									disabled={pending}
									pending={pending}
								/>
							</div>
							<div className="xxs12 description">
								<p dangerouslySetInnerHTML={{__html: t("clientidAndSecretCanBeCreatedHere", {url: "https://business.piggy.eu/management/oauth-clients" })}} />
								<p> {t("yourRedirectUrl")}: <strong>{process.env.redirect_url}</strong></p>
							</div>
						</div>
					</form>
				</div>
				<div className="right xs12 sm6">
					<div className="cta__image">
						<img src={ imageToWpUrl('homepage_products.webp') } />
					</div>
					<div className="cta__button">
						<Button
							type="submit"
							style="primary-ghost"
							label={t('createFreeAccount')}
							disabled={false}
							pending={false}
							onClick={() => window.open("https://business.piggy.eu/sign-up", "_blank")}
						/>
					</div>
				</div>
			</div>
		</Card>
	);
}