import HttpClient from "shared/services/HttpClient";
import ApiErrorMapper from "shared/mappers/ApiErrorMapper";

class AuthRepository {

    get() {
        return HttpClient.get('oauth')
            .then((res: any) => {
                return res.data.data;
            })
            .catch((res: any) => {
               throw ApiErrorMapper(res);
            });
    }

    remove() {
        return HttpClient.post('oauth/delete')
            .then((res: any) => {
                return res.data.data;
            })
            .catch((res: any) => {// write new error mapper.
                throw ApiErrorMapper(res);
            });
    }

}

export default new AuthRepository();