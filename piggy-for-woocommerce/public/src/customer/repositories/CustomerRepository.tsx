import ApiErrorMapper from "shared/mappers/ApiErrorMapper";
import {CustomerMapper} from "shared/mappers";
import HttpClient from "shared/services/HttpClient";

class CustomerRepository {
    show() {
        return HttpClient.get('customer/profile')
            .then((res: any) => {
                return {
                    customer: CustomerMapper(res.data.data.customer),
                    creditBalance: res.data.data.credit_balance ? res.data.data.credit_balance.balance : 0,
                }
            })
            .catch((res: any) => {// write new error mapper.
                throw ApiErrorMapper(res);
            });
    }
}

export default new CustomerRepository();