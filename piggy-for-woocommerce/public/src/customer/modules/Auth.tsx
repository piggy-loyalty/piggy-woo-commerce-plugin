import React, {useEffect, useState} from "react";
import {Button, useToast} from "shared/components";
import {useHistory} from "react-router";
import {AuthRepository} from "customer/repositories";
import {useTrans} from "@piggy-loyalty/foundation/lib/components";

export default function Auth() {
    const {t} = useTrans();
    const [pending, setPending] = useState(false);
    const {successToast, errorToast} = useToast();
    const history = useHistory();

    const redirectToOauth = () => {
        const options = 'location=0,status=0,width=800,height=600';

        setPending(true);

        AuthRepository.get()
            .then((url: string) => {
                window.open(url, 'ConnectWithOAuth', options);
            })
            .catch((error: any) => {
                errorToast(error);
            })
            .finally(() => {
                setPending(false);
            });
    };

    useEffect(() => {
        window.addEventListener('storage', () => {
            if(localStorage.getItem("piggy-link-event-successful") == "true") {
                successToast(t("linkedSuccessfully"));
                localStorage.removeItem("piggy-link-event-successful");
                history.push("/customer/" + process.env.component);
            }

            if(localStorage.getItem("piggy-link-event-successful") == "false") {
                errorToast(t("errorTryAgain"));
                localStorage.removeItem("piggy-link-event-successful");
            }
        });
    }, []);

    return (
        <>
            <div className="container">
                <div className="xxs12">
                    <div className="connect__heading">
                        <Button
                            style="primary"
                            label={t("connectWithPiggy")}
                            onClick={redirectToOauth}
                            disabled={pending}
                            pending={pending}
                        />
                    </div>
                </div>
            </div>
        </>
    );
}