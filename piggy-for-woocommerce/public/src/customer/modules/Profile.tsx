import React, {useEffect, useState} from "react";
import {CustomerRepository, AuthRepository} from "customer/repositories";
import {Card, Loader, useToast} from "shared/components";
import {useHistory} from "react-router";
import {Customer} from "shared/models";
import {Avatar} from "customer/components";
import {useTrans} from "@piggy-loyalty/foundation/lib/components";
import Logo from "../../../images/logo.svg";

type Props = {
    showActions: boolean;
};

export default function Profile(props: Props) {
    const {t} = useTrans();
    const [pending, setPending] = useState(true);
    const [customer, setCustomer] = useState(new Customer());
    const [creditBalance, setCreditBalance] = useState("0");
    const {successToast, errorToast} = useToast();
    const { showActions } = props;
    const history = useHistory();

    useEffect(() => {
        CustomerRepository.show()
            .then((res: any) => {
                setCustomer(res.customer);
                setCreditBalance(res.creditBalance);
                setPending(false);
            })
            .catch((error: any) => {
                history.push("/customer/auth");
            });

        return () => {
            setPending(false);
        }
    }, []);

    const unlink = () => {
        const confirmation = confirm(t("areYouSureYouWantToUnlinkPiggyAccount"));
        if(!confirmation) {
            return;
        }

        AuthRepository.remove()
            .then((res: any) => {
                successToast(t("unlinkedSuccessfully"));
                window.localStorage.removeItem("piggy-token");
                history.push("/customer/auth");
            })
            .catch((error: any) => {
                errorToast(error);
            });
    };

    return !pending ? (
        <Card classes="profile">
            <div className="container">
                <div className="profile__heading">
                    <div className="profile__logo">
                        <a href="https://piggy.eu/app" target="blank">
                            <img src={Logo} alt="Piggy" />
                        </a>
                    </div>
                    <div className="profile__credits">
                        <h2 className="credits__title">{creditBalance}<span>P</span></h2>
                    </div>
                </div>
                <div className="profile__content">
                    <div className="profile__avatar">
                        <Avatar customer={{picture:  customer.picture, fullName: customer.firstName + " " + customer.lastName}} />
                    </div>
                    <div className="profile__user">
                        <h2 className="profile__title">{customer.firstName + " " + customer.lastName}</h2>
                        <h4 className="profile__subtitle">{customer.email}</h4>
                    </div>
                </div>

                {showActions && (
                    <div className="profile__footer">
                        <a className="profile__link" href="https://customer.piggy.eu/profile" target="_blank">{t("editProfile")}</a>
                        <br />
                        <a className="profile__link" onClick={unlink}>{t("logout")}</a>
                    </div>
                )}
            </div>
        </Card>
    ) : null;
}