import React from "react";
import {Route, Switch} from "react-router";
import { ToastProvider } from "react-toast-notifications";
import Profile from "./Profile";
import Auth from "./Auth";

export default function Index() {
    return (
        <div id="piggy-for-woocommerce" className="customer">
            <ToastProvider autoDismiss={true}>
                <Switch>
                    <Route exact path="/customer/profile">
                        <Profile showActions={true} />
                    </Route>
                    <Route exact path="/customer/checkout">
                        <Profile showActions={false} />
                    </Route>
                    <Route exact path="/customer/auth">
                        <Auth />
                    </Route>
                </Switch>
            </ToastProvider>
        </div>
    );
}