import * as React from "react";
import { useRef, useState } from "react";
import {Loader} from "shared/components";

type Props = {
    customer: any;
};

export default function Avatar(props: Props) {
    const [pending, setPending] = useState(false);
    const { customer } = props;

    return (
        <div className="avatar">
            {customer.picture ? (
                pending ? (
                    <Loader />
                ) : (
                    <img alt={"avatar"} className="avatar__image" src={customer.picture} />
                )
            ) : (
                <div className="avatar__initials">
                    {customer
                        .fullName
                        .split(" ")
                        .map((n: string) => n[0])
                        .join("")}
                </div>
            )}
        </div>
    );
}