declare module "*.jpg";
declare module "*.png";
declare module "*.mp4";
declare module "*.svg" {
	const ReactComponent: SvgrComponent;
	export { ReactComponent };
	const src: string;
	export default src;
}