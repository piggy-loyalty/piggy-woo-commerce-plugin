import HttpClient from "shared/services/HttpClient";
import ApiErrorMapper from "shared/mappers/ApiErrorMapper";

class StagedCreditReceptionRepository {

    send(payload: any) {
        return HttpClient.post("admin/staged-credit-receptions", {
            email: payload.email,
            purchase_amount: payload.purchaseAmount,
            credits: payload.credits
        })
            .then((res: any) => {
                return res.data.data;
            })
            .catch((err: any) => {
                console.log(err);
                throw ApiErrorMapper(err);
            });
    }

}

export default new StagedCreditReceptionRepository();