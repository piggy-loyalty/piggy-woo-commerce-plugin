import HttpClient from "shared/services/HttpClient";
import ApiErrorMapper from "shared/mappers/ApiErrorMapper";
import ShopMapper from "shared/mappers/ShopMapper";

class AdminRepository {
    index() {
        return HttpClient.get('admin')
            .then((res: any) => {
                return ShopMapper(res.data.data);
            })
            .catch((res: any) => {
                console.log(res);
                // write new error mapper.
                throw ApiErrorMapper(res);
            });
    }

    statusCheck() {
        return HttpClient.get('admin/status')
            .then((res: any) => {
                return res;
            })
            .catch((res: any) => {
                console.log(res);
                // write new error mapper.
                throw ApiErrorMapper(res);
            });
    }
}

export default new AdminRepository();