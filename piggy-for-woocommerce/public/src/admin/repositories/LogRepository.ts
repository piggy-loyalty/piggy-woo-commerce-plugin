import HttpClient from "shared/services/HttpClient";
import ApiErrorMapper from "shared/mappers/ApiErrorMapper";

class LogRepository {

    index(page: number, limit: number = 10) {
        return HttpClient.get(`admin/logs?page=${page}&limit=${limit}`)
            .then((res: any) => {
                return res.data.data;
            })
            .catch((res: any) => {
                console.log(res);
                // write new error mapper.
                throw ApiErrorMapper(res);
            });
    }

    download() {
        return HttpClient.post(`admin/logs/download`)
        .then((res: any) => {
            return res.data.data;
        })
        .catch((res: any) => {
            console.log(res);
            // write new error mapper.
            throw ApiErrorMapper(res);
        });
    }

}

export default new LogRepository();