import HttpClient from "shared/services/HttpClient";
import ApiErrorMapper from "shared/mappers/ApiErrorMapper";

class LanguageSettingsRepository {

    index() {
        return HttpClient.get('admin/settings/language')
            .then((res: any) => {
                return res.data.data;
            })
            .catch((res: any) => {
                // write new error mapper.
                throw ApiErrorMapper(res);
            });
    }

    update(payload: any) {
        return HttpClient.post('admin/settings/language/update', payload)
            .then((res: any) => {
                return res.data.data;
            })
            .catch((res: any) => {
                console.log(res);
                // write new error mapper.
                throw ApiErrorMapper(res);
            });
    }

}

export default new LanguageSettingsRepository();