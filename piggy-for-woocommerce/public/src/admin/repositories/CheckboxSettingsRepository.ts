import HttpClient from "shared/services/HttpClient";
import ApiErrorMapper from "shared/mappers/ApiErrorMapper";

class CheckboxSettingsRepository {

	index() {
		return HttpClient.get('admin/settings/checkbox')
			.then((res: any) => {
				return res.data.data;
			})
			.catch((res: any) => {
				// write new error mapper.
				throw ApiErrorMapper(res);
			});
	}

	update(payload: any) {
		return HttpClient.post('admin/settings/checkbox/update', payload)
			.then((res: any) => {
				return res.data.data;
			})
			.catch((res: any) => {
				console.log(res);
				// write new error mapper.
				throw ApiErrorMapper(res);
			});
	}

}

export default new CheckboxSettingsRepository();