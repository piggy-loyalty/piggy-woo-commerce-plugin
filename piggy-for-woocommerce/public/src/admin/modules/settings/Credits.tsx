import React, {useEffect, useState} from "react";
import {Button, Card, FormCheckbox, useToast, useTrans} from "shared/components";
import { CreditsSettingsRepository } from "admin/repositories";
import {BackButton} from "admin/components/BackButton";

export default function Credits()
{
	const { t } = useTrans();
	const { successToast, errorToast } = useToast();
	const [pending, setPending] = useState(true);
	const [includeTaxes, setIncludeTaxes] = useState(false);
	const [includeShippingCost, setIncludeShippingCost] = useState(false);

	useEffect(() => {
		CreditsSettingsRepository.index()
			.then((res: any) => {
				setIncludeTaxes(res.credits_include_taxes);
				setIncludeShippingCost(res.credits_include_shipping_cost);
			})
			.catch((error: any) => {
				errorToast(error);
			})
			.finally(() => {
				setPending(false);
			});
	}, []);

	const submit = () => {
		setPending(true);

		const payload = {
			credits_include_taxes: includeTaxes,
			credits_include_shipping_cost: includeShippingCost,
		};

		CreditsSettingsRepository.update(payload)
			.then((res: any) => {
				setIncludeTaxes(res.credits_include_taxes);
				setIncludeShippingCost(res.credits_include_shipping_cost);
				successToast(t("success"));
			})
			.catch((err: any) => {
				errorToast(err);
			})
			.finally(() => {
				setPending(false);
			});
	};

	return (
		<div className="container--fluid">
			<div className="group settings--flow">
				<BackButton backRoute={"/admin/settings"} />

				<Card title={t('creditsSettings')}>
					<form className="admin-form points">
						<FormCheckbox
							label={t("creditCalculationIncludesTaxes")}
							name="creditCalculationIncludesTaxes"
							type="switch"
							onChange={() => setIncludeTaxes(!includeTaxes)}
							checked={includeTaxes}
						/>
						<FormCheckbox
							label={t("creditCalculationIncludesShippingCost")}
							name="creditCalculationIncludesShippingCost"
							type="switch"
							onChange={() => setIncludeShippingCost(!includeShippingCost)}
							checked={includeShippingCost}
						/>
						<div className="xxs12 settings__footer">
							<Button
								style="primary"
								label={t("save")}
								onClick={submit}
								disabled={pending}
								pending={pending}
							/>
						</div>
					</form>
				</Card>
			</div>
		</div>
	);
}