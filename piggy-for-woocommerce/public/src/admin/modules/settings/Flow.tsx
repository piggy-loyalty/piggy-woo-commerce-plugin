import React, {useEffect, useState} from "react";
import {Button, Card, FormCheckbox, useToast, useTrans} from "shared/components";
import {FormRadio} from "@piggy-loyalty/foundation/lib/components";
import { FlowSettingsRepository } from "admin/repositories";
import { BackButton } from "admin/components/BackButton";

export default function Flow()
{
	const { t } = useTrans();
	const {errorToast, successToast} = useToast();
	const [pending, setPending] = useState(true);
	const [guestFlow, setGuestFlow] = useState("");
	const [userFlow, setUserFlow] = useState("");
	const [allowLinking, setAllowLinking] = useState(false);

	useEffect(() => {
		FlowSettingsRepository.index()
			.then((res: any) => {
				setAllowLinking(res.allow_linking);
				setGuestFlow(res.guest_flow);
				setUserFlow(res.user_flow);
			})
			.catch((error: any) => {
				errorToast(error);
			})
			.finally(() => {
				setPending(false);
			});
	}, []);

	const submit = () => {
		setPending(true);

		const payload = {
			guest_flow: guestFlow,
			user_flow: userFlow,
			allow_linking: allowLinking,
		};

		FlowSettingsRepository.update(payload)
			.then((res: any) => {
				setAllowLinking(res.allow_linking);
				setGuestFlow(res.guest_flow);
				setUserFlow(res.user_flow);
				successToast(t("success"));
				setPending(false);
			})
			.catch((err: any) => {
				errorToast(err);
				setPending(false);

			});
	};

	return (
		<div className="container--fluid">
			<div className="group settings--flow">
				<BackButton backRoute={"/admin/settings"} />

				<Card title={`Flow ${t("settings")}`}>
					<form className="admin-form flow">
						<div className="xxs12">
							<FormCheckbox
								label={t("allowCustomersToLinkAccountToPiggy")}
								name="allowLinking"
								type="switch"
								onChange={() => setAllowLinking(!allowLinking)}
								checked={allowLinking}
							/>
						</div>
						<div className="xxs12">
							<h4>{t('sendPointsToGuestsByEmailAfterCheckoutCompleted')}</h4>
							<FormRadio
								label={t("optional")}
								name="guest"
								value="optional"
								checked={guestFlow}
								onChange={(event: any) => setGuestFlow(event.target.value)}
							/>
							<FormRadio
								label={t("always")}
								name="guest"
								value="always"
								checked={guestFlow}
								onChange={(event: any) => setGuestFlow(event.target.value)}
							/>
							<FormRadio
								label={t("never")}
								name="guest"
								value="never"
								checked={guestFlow}
								onChange={(event: any) => setGuestFlow(event.target.value)}
							/>
						</div>
						<div className="group">
							<div className="xxs12">
								<h4>{t('sendPointsToCustomersByEmailWhenPiggyAccountIsNotLinked')}</h4>
								<FormRadio
									name="unlinkedUser"
									value="optional"
									checked={userFlow}
									label={t("optional")}
									onChange={(event: any) => setUserFlow(event.target.value)}
								/>
								<FormRadio
									name="unlinkedUser"
									value="always"
									checked={userFlow}
									label={t("always")}
									onChange={(event: any) => setUserFlow(event.target.value)}
								/>
								<FormRadio
									name="unlinkedUser"
									value="never"
									checked={userFlow}
									label={t("never")}
									onChange={(event: any) => setUserFlow(event.target.value)}
								/>
							</div>
						</div>
						{allowLinking ? (
							<div className="group">
								<div className="xxs12">
									<h4>{t('sendPointsToCustomersByEmailWhenPiggyAccountIsLinked')}</h4>
									<FormRadio
										name="linkedUser"
										value="always"
										checked={"always"}
										label={t("always")}
										onChange={() => {}} // When this is set to null, it gives an error "provide checkbox but onchange is null"
									/>
								</div>
							</div>
						) : null }
						<div className="xxs12 settings__footer">
						    <Button
						        style="primary"
						        label={t("save")}
						        onClick={submit}
						        disabled={pending}
						        pending={pending}
						    />
						</div>
					</form>
				</Card>
			</div>
		</div>
	);
}