import React, {useEffect, useState} from "react";
import {Button, Card, FormSelect, Loader, useToast, useTrans} from "shared/components";
import { LanguageSettingsRepository } from "admin/repositories";
import {BackButton} from "admin/components/BackButton";

export default function Language()
{
	const { t } = useTrans();
	const {errorToast, successToast} = useToast();
	const [pending, setPending] = useState(true);
	const [languages, setLanguages] = useState<[]>([]);
	const [selectedLanguage, setSelectedLanguage] = useState("");

	useEffect(() => {
		LanguageSettingsRepository.index()
		.then((res: any) => {
			setLanguages(res.language_options);
			setSelectedLanguage(res.language);
		})
		.catch((error: any) => {
			errorToast(error);
		})
		.finally(() => {
			setPending(false);
		});
	}, []);

	const formatToSelectValuesWithTranslation = (object: any, keyNameForValue: string, keyNameForLabel: string) => {
		return object.map((item: any) => {
			return {
				value: item[keyNameForValue],
				label: t(item[keyNameForLabel]),
			};
		});
	};

	const handleChange = (value: string) => {
		languages.forEach((language: any) => {
			if(language.id == value) {
				setSelectedLanguage(language.id);
			}
		});
	};

	const submit = () => {
		setPending(true);

		const payload = {
			language: selectedLanguage,
		};

		LanguageSettingsRepository.update(payload)
			.then((res: any) => {
				setSelectedLanguage(res.language);
				successToast(t("success"));
			})
			.catch((err: any) => {
				errorToast(err);
			})
			.finally(() => {
				setPending(false);
			});
	};

	return (
		<div className="container--fluid">
			<div className="group settings--flow">
				<BackButton backRoute={"/admin/settings"} />

				<Card title={t('languageSettings')}>
					<form className="admin-form flow">
						{languages.length > 0 ? (
							<FormSelect value={selectedLanguage} name={"language"} label={t("selectLanguage")} onChange={(event: any) => handleChange(event.target.value)} options={formatToSelectValuesWithTranslation(languages, "id", "label")} />
						) : (
							<Loader />
						)}
						<div className="settings__footer">
							<Button
								style="primary"
								label={t("save")}
								onClick={submit}
								disabled={pending}
								pending={pending}
							/>
						</div>
					</form>
				</Card>
			</div>
		</div>
	)
}