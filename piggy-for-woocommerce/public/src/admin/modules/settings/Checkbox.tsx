import React, {useEffect, useState} from "react";
import {Button, Card, FormInput, useToast, useTrans} from "shared/components";
import {CheckboxSettingsRepository} from "admin/repositories";
import {BackButton} from "admin/components/BackButton";

export default function Checkbox()
{
	const { t } = useTrans();
	const { errorToast, successToast } = useToast();
	const [pending, setPending] = useState(true);
	const [checkboxText, setCheckboxText] = useState("");
	const [moreInformationPage, setMoreInformationPage] = useState("");
	const [moreInformationText, setMoreInformationText] = useState("");

	useEffect(() => {
		CheckboxSettingsRepository.index()
			.then((res: any) => {
				setCheckboxText(res.staged_checkbox_text);
				setMoreInformationPage(res.staged_checkbox_url);
				setMoreInformationText(res.staged_checkbox_url_text);
			})
			.catch((error: any) => {
				errorToast(error);
			})
			.finally(() => {
				setPending(false);
			});
	}, []);

	const submit = () => {
		setPending(true);

		const payload = {
			staged_checkbox_text: checkboxText,
			staged_checkbox_url: moreInformationPage,
			staged_checkbox_url_text: moreInformationText,
		};

		CheckboxSettingsRepository.update(payload)
			.then((res: any) => {
				setCheckboxText(res.staged_checkbox_text);
				setMoreInformationPage(res.staged_checkbox_url);
				setMoreInformationText(res.staged_checkbox_url_text);
				successToast(t("success"));
			})
			.catch((err: any) => {
				errorToast(err);
			})
			.finally(() => {
				setPending(false);
			});
	};

	return (
		<div className="container--fluid">
			<div className="group settings--flow">
				<BackButton backRoute={"/admin/settings"} />

				<Card title={t("checkboxSettings")}>
					<form className="admin-form checkbox">
						<div className="xxs12 md4 input__wrapper">
							<FormInput value={checkboxText} name="text" label={t("checkboxText")} onChange={(event: any) => setCheckboxText(event.target.value)} />
							<FormInput value={moreInformationText} name="url_text" label={t("checkboxMoreInformationText")} onChange={(event: any) => setMoreInformationText(event.target.value)} />
							<FormInput value={moreInformationPage} name="page" label={t("checkboxMoreInformationPage")} onChange={(event: any) => setMoreInformationPage(event.target.value)} />
						</div>
						<div className="xxs12 settings__footer">
							<Button
								style="primary"
								label={t("save")}
								onClick={submit}
								disabled={pending}
								pending={pending}
							/>
						</div>
					</form>
				</Card>
			</div>
		</div>
	);
}