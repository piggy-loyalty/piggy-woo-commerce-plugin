import React from "react";
import {ActionCard} from "shared/components";
import {useTrans} from "@piggy-loyalty/foundation/lib/components";
import {imageToWpUrl} from "shared/services/helpers";
import {useHistory} from "react-router";
import {BackButton} from "admin/components/BackButton";

export default function Overview() {
    const { t } = useTrans();
    const history = useHistory();

    return (
        <div className="container--fluid">
            <BackButton backRoute={"/"} />

            <div className="group settings">
                <ActionCard
                    classes="xxs12 md3 lg2"
                    onClick={() => history.push('/admin/settings/flow')}
                    imgSrc={imageToWpUrl('flow_settings.svg')}
                    title={"Flow " + t("settings")}
                    buttonLabel={t("continue")}
                />
                <ActionCard
                    classes="xxs12 md3 lg2"
                    onClick={() => history.push('/admin/settings/checkbox')}
                    imgSrc={imageToWpUrl('rules.svg')}
                    title={t("checkboxSettings")}
                    buttonLabel={t("continue")}
                />
                <ActionCard
                    classes="xxs12 md3 lg2"
                    onClick={() => history.push('/admin/settings/language')}
                    imgSrc={imageToWpUrl('form.svg')}
                    title={t("languageSettings")}
                    buttonLabel={t("continue")}
                />
                <ActionCard
                    classes="xxs12 md3 lg2"
                    onClick={() => history.push('/admin/settings/credits')}
                    imgSrc={imageToWpUrl('staged.svg')}
                    title={t("creditsSettings")}
                    buttonLabel={t("continue")}
                />
            </div>
        </div>
    );
}