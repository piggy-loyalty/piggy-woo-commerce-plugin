import React, {useEffect, useState} from "react";
import {Button, Card, useToast} from "shared/components";
import {AdminRepository} from "admin/repositories";
import {SetupRepository} from "shared/repositories";
import {ShopInterface} from "shared/models";
import {useTrans} from "@piggy-loyalty/foundation/lib/components";

export default function StatusCard()
{
	const [pending, setPending] = useState(true);
	const [shop, setShop] = useState<ShopInterface>();
	const [status, setStatus] = useState<string>('');
	const {errorToast, successToast} = useToast();
	const { t } = useTrans();

	useEffect(() => {
		AdminRepository.index()
			.then((res: any) => {
				setShop(res);
				setStatus('connected');
			})
			.catch((error: any) => {
				errorToast(error);
			})
			.finally(() => {
				setPending(false);
			});
	}, []);

	const checkStatus = () => {
		setPending(true);
		AdminRepository.statusCheck()
			.then((res: any) => {
				setStatus('connected');
				successToast();
			})
			.catch((err: any) => {
				errorToast(err);
				setStatus('connectionFailed');
			})
			.finally(() => {
				setPending(false);
			});
	};

	const submit = () => {
		setPending(true);
		SetupRepository.destroy()
			.then((res: any) => {
				window.location.replace(res.redirect_url);
			})
			.catch((err: any) => {
				errorToast(err);
			})
			.finally(() => {
				setPending(false);
			});
	};

	return (
		<Card>
			<div className="group status__container">
				<div className="status__row xxs12">
					{t('shop')}:
					<span>{ shop ? shop.name : '' }</span>
				</div>
				<div className={`status__row xxs12 ${status}`}>
					{t('status')}:
					<span>{ status ? t(status) : '' }</span>
				</div>
			</div>
			<div className="group status__buttons">
				<div className="xxs12">
					<Button
						type="submit"
						style="primary"
						label={t("checkStatus")}
						disabled={pending}
						pending={pending}
						onClick={checkStatus}
					/>
					<Button
						type="submit"
						style="danger"
						label={t("deleteConfiguration")}
						disabled={pending}
						pending={pending}
						onClick={submit}
					/>
				</div>
			</div>
		</Card>
	);
}