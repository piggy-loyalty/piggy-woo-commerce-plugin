import React from "react";
import {ActionCard } from "shared/components";
import {useTrans} from "@piggy-loyalty/foundation/lib/components";
import {imageToWpUrl} from "shared/services/helpers";
import {useHistory} from "react-router";
import StatusCard from "admin/modules/overview/StatusCard";

export default function Overview() {
    const { t } = useTrans();
    const history = useHistory();

    return (
        <div className="container--fluid">
            <div className="group">
                <div className="xxs12 md8 lg6">
                    <StatusCard />
                </div>
            </div>

            <div className="group action-card__container">
                <ActionCard
                    classes="xxs12 md3 lg2"
                    onClick={() => history.push('/admin/tests')}
                    imgSrc={imageToWpUrl('test_suite.svg')}
                    title={"Test suite"}
                    buttonLabel={t("continue")}
                />
                <ActionCard
                    classes="xxs12 md3 lg2"
                    onClick={() => history.push('/admin/logs')}
                    imgSrc={imageToWpUrl('form.svg')}
                    title={"Logs"}
                    buttonLabel={t("continue")}
                />
                <ActionCard
                    classes="xxs12 md3 lg2"
                    onClick={() => history.push('/admin/settings')}
                    imgSrc={imageToWpUrl('settings.svg')}
                    title={"Settings"}
                    buttonLabel={t("continue")}
                />
            </div>
        </div>
    );
}