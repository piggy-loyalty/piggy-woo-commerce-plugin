import React, {useState} from "react";
import {Button, Card, FormInput, useToast} from "shared/components";
import {StagedCreditReceptionRepository} from "admin/repositories";
import {useTrans} from "@piggy-loyalty/foundation/lib/components";
import { BackButton } from "admin/components/BackButton";

export default function Testsuite() {
    const {successToast, errorToast} = useToast();
    const {t} = useTrans();
    const [pending, setPending] = useState(false);
    const [stagedCreditReceptionData, setStagedCreditReceptionData] = useState({
        email: "",
        purchaseAmount: "",
        credits: "",
    });

    const onChangeStagedCreditReception = (fieldName: string, value: string) => {
        setStagedCreditReceptionData((prevState: any) => {
            return ({
                ...prevState,
                [fieldName]: value
            })
        })
    };

    const submit = (event: any) => {
        event.preventDefault();
        setPending(true);

        StagedCreditReceptionRepository.send(stagedCreditReceptionData)
            .then((res: any) => {
                successToast(t("successfullySendCreditsToEmail", {credits: res.staged_credit_reception.credits, email: res.email}));
            })
            .catch((error: any) => {
                errorToast(error);
            })
            .finally(() => {
                setPending(false);
            });
    };

    return (
        <div className="container--fluid">
            <div className="group">
                <BackButton backRoute={"/"} />

                <div className="xxs12">
                    <Card classes="testsuite__wrapper" title={"Test suite"}>
                        <form className="admin-form" onSubmit={submit}>
                            <FormInput
                                label={t("email") + "*"}
                                value={stagedCreditReceptionData.email}
                                name={"email"}
                                onChange={(event: any) => onChangeStagedCreditReception("email", event.target.value)}
                                required={true}
                                outlined
                            />

                            <FormInput
                                label={t("purchaseAmount") + "*"}
                                value={stagedCreditReceptionData.purchaseAmount}
                                name={"purchaseAmount"}
                                onChange={(event: any) => onChangeStagedCreditReception("purchaseAmount", event.target.value)}
                                required={true}
                                outlined
                            />

                            <FormInput
                                label={t("credits")}
                                value={stagedCreditReceptionData.credits}
                                name={"credits"}
                                onChange={(event: any) => onChangeStagedCreditReception("credits", event.target.value)}
                                outlined
                            />

                            <Button
                                type="submit"
                                style="primary"
                                label={t("sendStagedCreditReception")}
                                disabled={pending}
                                pending={pending}
                            />
                        </form>
                    </Card>
                </div>
            </div>
        </div>
    );
}