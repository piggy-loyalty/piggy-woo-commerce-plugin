import React from "react";
import {ToastProvider} from "react-toast-notifications";
import {Route, Switch} from "react-router";

import Overview from "./overview/Overview";
import Testsuite from "./testsuite/Testsuite";
import Logs from "./logs/Logs";

import SettingsOverview from "./settings/Overview";
import SettingsFlow from "./settings/Flow";
import SettingsLanguage from "./settings/Language";
import SettingsCredits from "./settings/Credits";
import SettingsCheckbox from "./settings/Checkbox";

export default function Index() {

    return (
        <div id="piggy-for-woocommerce" className="admin admin__container">
            <ToastProvider autoDismiss={true}>
                <Switch>
                    <Route exact path="/">
                        <Overview />
                    </Route>
                    <Route exact path="/admin/overview">
                        <Overview />
                    </Route>
                    <Route exact path="/admin/logs">
                        <Logs />
                    </Route>
                    <Route exact path="/admin/tests">
                        <Testsuite />
                    </Route>
                    <Route path="/admin/settings">
                        <Route path="/admin/settings" exact component={SettingsOverview} />
                        <Route path="/admin/settings/flow" exact component={SettingsFlow}/>
                        <Route path="/admin/settings/language" exact component={SettingsLanguage}/>
                        <Route path="/admin/settings/credits" exact component={SettingsCredits}/>
                        <Route path="/admin/settings/checkbox" exact component={SettingsCheckbox}/>
                    </Route>
                </Switch>
            </ToastProvider>
        </div>
    );
}