import React, {useEffect, useState} from "react";
import {Accordion, Button, Card, Loader, useToast, useTrans} from "shared/components";
import {LogRepository} from "admin/repositories";
import {BackButton} from "admin/components/BackButton";

export default function Logs() {
	const [pending, setPending] = useState(true);
	const [logs, setLogs] = useState([]);
	const [page, setPage] = useState(1);
	const [numberOfLogs, setNumberOfLogs] = useState<number>(1);
	const {errorToast} = useToast();
	const { t } = useTrans();

	useEffect(() => {
		setPending(true);
		LogRepository.index(page)
			.then((res: {files: [], count: number}) => {
				setLogs(res.files);
				setNumberOfLogs(res.count);
			})
			.catch((error: any) => {
				errorToast(error);
			})
			.finally(() => {
				setPending(false);
			});
	}, [page]);

	const downloadLogs = () => {
		LogRepository.download()
			.then((download_url: string) => {
				window.open(download_url);
			})
			.catch((error: any) => {
				errorToast(error);
			});
	};

	const handlePaginationButton = (action: string) => {
		if(action == "+") {
			setPage(page + 1);
		}

		if(action == "-") {
			if(page > 1) {
				setPage(page - 1);
			}
		}
	};

	const formatContent = (content: []): any => {
		return content.map((line: string, i: number) => {
			return (
				<div key={i}>
					<span>{line}</span>
					<br/>
				</div>
			)
		});
	};

	const PaginationInfo = () => {
		const numberOfPagesLeft = Math.ceil(numberOfLogs / 10);

		return (
			<span className="pagination__content"> {page} - {numberOfPagesLeft > 0 ? numberOfPagesLeft : 1} </span>
		);
	};

	return (
		<div className="container--fluid">
			<div className="group">
				<div className="xxs12 button-actions__container">
					<BackButton backRoute={"/"} />
					<Button style={'primary'} label={t('download')} disabled={pending} />
				</div>
				<div className="xxs12">
					<Card classes={"xxs12 logs__container"} title={"Logs"}>
						<div className="group accordion__wrapper">
							{!pending ? logs.map((log: any) =>
								<Accordion key={log.name} title={log.name} content={formatContent(log.content)}/>
							) : (
								<Loader />
							)}
						</div>
						<div className="group pagination__wrapper">
							<Button style="primary-ghost" label="<" disabled={pending} onClick={() => handlePaginationButton("-")} />
							<PaginationInfo />
							<Button style="primary-ghost" label=">" disabled={pending} onClick={() => handlePaginationButton("+")} />
						</div>
					</Card>
				</div>
			</div>
		</div>
	);
}