import * as React from "react";
import { Header as FoundationHeader } from "@piggy-loyalty/foundation/lib/components";
import { imageToWpUrl } from "shared/services/helpers";
import {Button, useTrans} from "shared/components";

export const PiggyHeader = () => {
    const { t } = useTrans();
    const items = [
        { label: "Overview", path: "/admin/overview" },
        { label: "Test suite", path: "/admin/tests" },
        { label: "Settings", path: "/admin/settings"},
        { label: "Logs", path: "/admin/logs" }
    ];

    return <FoundationHeader desktopItems={items} desktopActions={[]} logo={imageToWpUrl("logo.svg")} />;
};