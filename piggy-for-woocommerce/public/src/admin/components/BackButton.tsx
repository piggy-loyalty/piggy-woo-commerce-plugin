import {imageToWpUrl} from "shared/services/helpers";
import * as React from "react";
import {useHistory} from "react-router";
import {useTrans} from "shared/components";

type Props = {
	backRoute: string;
}

export const BackButton = (props: Props) => {
	const { backRoute } = props;
	const { t } = useTrans();
	const history = useHistory();

	const goBack = () => {
		if (backRoute) {
			return history.push(backRoute);
		}

		return history.goBack();
	};

	return (
		<button className="header__top-back" onClick={() => goBack()}>
			<img src={imageToWpUrl('arrow-left.svg')} />
			<span>{t('back')}</span>
		</button>
	)
};