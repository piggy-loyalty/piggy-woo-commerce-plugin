import React from "react";

module.exports = {
    ToastProvider: jest.fn(({ children }) => (
        <div data-testid="toastProvider">{children}</div>
    )),
    useToasts: () => ({
        successToast: () => null,
        errorToast: () => null,
        addToast: () => null,
    }),
};