const { addEventListener } = window;

function mock() {
	delete window.addEventListener;
	window.addEventListener = jest.fn();
	return window.addEventListener;
}

function restore() {
	window.addEventListener = addEventListener;

	return window.addEventListener;
}

export default { mock, restore };