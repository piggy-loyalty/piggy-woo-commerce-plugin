const { location } = window;

function mock() {
	delete window.open;

	window.open = jest.fn();

	return window.open;
}

function restore() {
	window.open = open;

	return window.open;
}

export default { mock, restore };