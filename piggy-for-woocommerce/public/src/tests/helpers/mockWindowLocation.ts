const { location } = window;

function mock(method: string) {
	delete window.location;
	window.location = {
		...location,
		[method]: jest.fn()
	};

	return window.location;
}

function restore() {
	window.location = location;

	return window.location;
}

export default { mock, restore };