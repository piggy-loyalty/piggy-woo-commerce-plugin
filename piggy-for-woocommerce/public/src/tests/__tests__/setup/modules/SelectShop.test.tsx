import React from "react";
import "@testing-library/jest-dom";
import {render, waitFor, screen, act} from "@testing-library/react";
import SelectShop from "setup/modules/SelectShop";
import userEvent from "@testing-library/user-event";
import windowLocation from "tests/helpers/mockWindowLocation";

let mockedShopsReturnValue: Promise<any> = Promise.resolve([]);
let mockedSetupReturnValue: Promise<any> = Promise.resolve({ redirect_url: "redirect url" });
jest.mock('shared/repositories', (): any => ({
	...jest.requireActual('shared/repositories'),
	ShopsRepository: {
		index: () => mockedShopsReturnValue
	},
	SetupRepository: {
		update: () => mockedSetupReturnValue
	}
}));

const mockErrorToast = jest.fn();
jest.mock('shared/components', (): any => ({
	...jest.requireActual('shared/components'),
	useToast: () => ({
		errorToast: mockErrorToast,
	}),
}));

describe("SelectShop.tsx", () => {
	it("Renders correctly", async () => {
		mockedShopsReturnValue = Promise.resolve([{ id: "1", name: "webshop 1"}]);

		render( <SelectShop/> );

		await waitFor(() => expect(screen.getByLabelText("webshops")).toBeInTheDocument());
		await waitFor(() => expect(screen.getByText("chooseShop")).toBeInTheDocument());
		await waitFor(() => expect(screen.getByText("refreshShops")).toBeInTheDocument());
	});

	it("Shows message please create shop first when no shops found", async () => {
		mockedShopsReturnValue = Promise.resolve([]);

		render( <SelectShop/> );

		await waitFor(() => expect(screen.getByText("pleaseCreateShopsFirst")).toBeInTheDocument());
	});

	it("Shows error toast when index call went wrong", async () => {
		mockedShopsReturnValue = Promise.reject("Error message.");

		render( <SelectShop/> );

		await waitFor(() => expect(mockErrorToast).toHaveBeenCalledWith("Error message."));
	});

	it("Redirect user to admin url, when shops selected", async () => {
		window.location = windowLocation.mock("replace");
		mockedShopsReturnValue = Promise.resolve([{ id: "1", name: "webshop 1"}]);

		render( <SelectShop/> );

		await waitFor( () => userEvent.click(screen.getByText("chooseShop")));
		await waitFor(() => expect(window.location.replace).toHaveBeenCalledWith("redirect url"));

		window.location = windowLocation.restore();
	});

	// it("Shows error toast when update call went wrong", async () => {
	// 	mockedShopsReturnValue = Promise.resolve([{ id: "1", name: "webshop 1"}]);
	// 	mockedSetupReturnValue = Promise.reject("Error message.");
	//
	// 	render( <SelectShop/> );
	//
	// 	userEvent.click(await waitFor(() => screen.getByText("chooseShop")));
	//
	// 	await waitFor(() => expect(mockErrorToast).toHaveBeenCalledWith("Error message."));
	// });
});