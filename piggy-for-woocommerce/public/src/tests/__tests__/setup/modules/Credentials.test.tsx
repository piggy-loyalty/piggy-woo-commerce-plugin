import React from "react";
import "@testing-library/jest-dom";
import Credentials from "setup/modules/Credentials";
import {render, screen, waitFor} from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { MemoryRouter } from 'react-router';

const mockHistoryPush = jest.fn();
jest.mock('react-router', (): any => ({
	...jest.requireActual('react-router'),
	useHistory: () => ({
		push: mockHistoryPush,
	}),
}));

const mockErrorToast = jest.fn();
jest.mock('shared/components', (): any => ({
	...jest.requireActual('shared/components'),
	useToast: () => ({
		errorToast: mockErrorToast,
	}),
}));

let mockAuthenticate: any = jest.fn();
jest.mock('shared/repositories', () => ({
	SetupRepository: { authenticate: () => mockAuthenticate }
}));

describe("Credentials.tsx", () => {
	beforeEach(() => {
		render(
			<MemoryRouter>
				<Credentials/>
			</MemoryRouter>
		);
	});

	it("Renders correctly", () => {
		expect(screen.getByLabelText("ClientID")).toBeInTheDocument();
		expect(screen.getByLabelText("Secret")).toBeInTheDocument();
		expect(screen.getByText("continue")).toBeInTheDocument();
	});

	it("authenticates with clientId and Secret", async () => {
		mockAuthenticate = Promise.resolve({ data: {} });

		userEvent.type(screen.getByLabelText("ClientID"), "1");
		userEvent.type(screen.getByLabelText("Secret"), "2");
		userEvent.click(screen.getByText("continue"));

		await waitFor(() => expect(mockHistoryPush).toHaveBeenCalledWith("/setup/healthCheck"));
	});

	it("shows error toast when authentication failed", async () => {
		mockAuthenticate = Promise.reject( "Error message." );

		userEvent.type(screen.getByLabelText("ClientID"), "1");
		userEvent.type(screen.getByLabelText("Secret"), "2");
		userEvent.click(screen.getByText("continue"));

		await waitFor(() => expect(mockErrorToast).toHaveBeenCalledWith("Error message."));
	});
});