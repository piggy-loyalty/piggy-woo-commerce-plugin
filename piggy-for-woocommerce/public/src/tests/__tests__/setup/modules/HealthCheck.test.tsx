import React from "react";
import "@testing-library/jest-dom";
import {render, screen, waitFor} from "@testing-library/react";
import {MemoryRouter} from "react-router";
import HealthCheck from "setup/modules/HealthCheck";
import userEvent from "@testing-library/user-event";

const mockHistoryPush = jest.fn();
jest.mock('react-router', (): any => ({
	...jest.requireActual('react-router'),
	useHistory: () => ({
		push: mockHistoryPush,
	}),
}));

const mockErrorToast = jest.fn();
jest.mock('shared/components', (): any => ({
	...jest.requireActual('shared/components'),
	useToast: () => ({
		errorToast: mockErrorToast,
	}),
}));

let mockHealthCheck: any = jest.fn();
jest.mock('shared/repositories', () => ({
	SetupRepository: { healthCheck: () => mockHealthCheck }
}));

describe("HealthCheck.tsx", () => {
	it("Renders correctly", () => {
		render(
			<MemoryRouter>
				<HealthCheck />
			</MemoryRouter>
		);

		expect(screen.getByText("doHealthCheck")).toBeInTheDocument();
	});

	it("Redirect user to step 3 when health check succeeded", async () => {
		render(
			<MemoryRouter>
				<HealthCheck />
			</MemoryRouter>
		);

		mockHealthCheck = Promise.resolve();

		userEvent.click(screen.getByText("doHealthCheck"));

		await waitFor(() => expect(mockHistoryPush).toHaveBeenCalledWith("/setup/shops"));
	});

	it("Shows error message when health check failed", async () => {
		render(
			<MemoryRouter>
				<HealthCheck />
			</MemoryRouter>
		);

		mockHealthCheck = Promise.reject( "Error message." );

		userEvent.click(screen.getByText("doHealthCheck"));

		await waitFor(() => expect(mockErrorToast).toHaveBeenCalledWith("Error message."));
	});
});