import React from "react";
import "@testing-library/jest-dom";
import Index from "setup/modules/index";
import {MemoryRouter} from "react-router";
import {render, screen} from "@testing-library/react";

describe("Index.tsx", () => {
	test("Expect to load Credentials page", () => {
		render(
			<MemoryRouter initialEntries={["/setup/credentials"]}>
				<Index />
			</MemoryRouter>
		);

		expect(screen.getByText("step 1")).toBeInTheDocument();
	});

	test("Expect to load HealthCheck page", () => {
		render(
			<MemoryRouter initialEntries={["/setup/healthCheck"]}>
				<Index />
			</MemoryRouter>
		);

		expect(screen.getByText("step 2")).toBeInTheDocument();
	});

	test("Expect to load SelectShop page", () => {
		render(
			<MemoryRouter initialEntries={["/setup/shops"]}>
				<Index />
			</MemoryRouter>
		);

		expect(screen.getByText("step 3")).toBeInTheDocument();
	});
});