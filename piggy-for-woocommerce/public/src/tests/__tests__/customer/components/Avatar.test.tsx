import React from "react";
import Avatar from "../../../../customer/components/Avatar";
import { shallow } from "enzyme";

describe('<Avatar />', () => {

	it('renders avatar component with picture correctly.',() => {
		const customer = {
			picture: "https://i.stack.imgur.com/6XhjY.jpg",
			fullName: "Mike Edinger"
		};

		const wrapper = shallow(<Avatar customer={customer} />);
		expect(wrapper).toMatchSnapshot();
	});

	it('when avatar has no picture create initials',() => {
		const customer = {
			picture: "",
			fullName: "Mike Edinger"
		};

		const wrapper = shallow(<Avatar customer={customer} />);
		expect(wrapper).toMatchSnapshot();
	});
});