import React from "react";
import "@testing-library/jest-dom";
import {render, screen, waitFor} from "@testing-library/react";
import Profile from "customer/modules/Profile";
import userEvent from "@testing-library/user-event";

let mockedCustomerShow: Promise<any> = Promise.resolve({
	customer: {
		picture: "",
		firstName: "john",
		lastName: "doe",
		email: "johndoe@gmail.com"
	},
	creditBalance: 1,
});
let mockedAuthRemove: Promise<any> = Promise.resolve();
jest.mock('customer/repositories', (): any => ({
	...jest.requireActual('customer/repositories'),
	CustomerRepository: { show: () => mockedCustomerShow },
	AuthRepository: { remove: () => mockedAuthRemove}
}));


const mockHistoryPush = jest.fn();
jest.mock('react-router', (): any => ({
	...jest.requireActual('react-router'),
	useHistory: () => ({
		push: mockHistoryPush,
	}),
}));

const mockErrorToast = jest.fn();
const mockSuccessToast = jest.fn();
jest.mock('shared/components', (): any => ({
	...jest.requireActual('shared/components'),
	useToast: () => ({
		errorToast: mockErrorToast,
		successToast: mockSuccessToast,
	}),
}));

window.confirm = jest.fn(() => true);

describe("Profile.tsx", () => {
	it("Renders correctly", async () => {
		render(
			<Profile showActions={true} />
		);

		await waitFor(() => expect(screen.getByText("john doe")));
		await waitFor(() => expect(screen.getByText("jd")));
		await waitFor(() => expect(screen.getByText("johndoe@gmail.com")));
		await waitFor(() => expect(screen.getByText("1")));
	});

	it("Returns user back to auth page when profile show call failed", async () => {
		mockedCustomerShow = Promise.reject("Something went wrong.");

		render(
			<Profile showActions={true} />
		);

		await waitFor(() => expect(mockHistoryPush).toHaveBeenCalledTimes(1));
	});
	//
	// NEED TO FIX THESE TEXTS ASAP...
	//
	// it("Shows success toast when unlink call succeeded", async () => {
	// 	render(
	// 		<Profile showActions={true} />
	// 	);
	//
	// 	userEvent.click(await waitFor(() => screen.getByText("logout")));
	//
	// 	await waitFor(() => expect(mockSuccessToast).toHaveBeenCalledWith("unlinkedSuccessfully"));
	// });
	//
	// it("Redirects user back to auth page when unlinked piggy account", async () => {
	// 	render(
	// 		<Profile showActions={true} />
	// 	);
	//
	// 	userEvent.click(await waitFor(() => screen.getByText("logout")));
	//
	// 	await waitFor(() => expect(mockHistoryPush).toHaveBeenCalledTimes(1));
	// });

	// it("Shows error toast when unlinking piggy account fails", async () => {
	// 	mockedAuthRemove = Promise.reject("Something went wrong.");
	//
	// 	render(
	// 		<Profile />
	// 	);
	//
	// 	userEvent.click(await waitFor(() => screen.getByText("unlinkPiggyAccount")));
	//
	// 	await waitFor(() => expect(mockErrorToast).toHaveBeenCalledWith("Something went wrong."));
	// });
});