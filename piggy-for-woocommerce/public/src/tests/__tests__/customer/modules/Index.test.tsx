import React from "react";
import "@testing-library/jest-dom";
import Index from "customer/modules/index";
import {render, screen, waitFor} from "@testing-library/react";
import {MemoryRouter} from "react-router";

let mockedCustomerShow: any = jest.fn();
jest.mock('customer/repositories', (): any => ({
	...jest.requireActual('customer/repositories'),
	CustomerRepository: { show: () => mockedCustomerShow },
	CheckoutRepository: { show: () => mockedCustomerShow },
}));

describe("Test customer routing", () => {

	test("Expect to load profile page", async() => {
		mockedCustomerShow = Promise.resolve({
			customer: {
				picture: "",
				firstName: "john",
				lastName: "doe",
				email: "johndoe@gmail.com"
			},
			creditBalance: 1,
			purchaseAmount: 20,
		});

		render(
			<MemoryRouter initialEntries={["/customer/profile"]}>
				<Index />
			</MemoryRouter>
		);

		await waitFor(() => expect(screen.getByText("john doe")).toBeInTheDocument());
	});

	test("Expect to load checkout page", async () => {
		render(
			<MemoryRouter initialEntries={["/customer/checkout"]}>
				<Index />
			</MemoryRouter>
		);

		await waitFor(() => expect(screen.getByText("john doe")).toBeInTheDocument());
	});

	test("Expect to load auth page", () => {
		render(
			<MemoryRouter initialEntries={["/customer/auth"]}>
				<Index />
			</MemoryRouter>
		);

		expect(screen.getByText("connectWithPiggy")).toBeInTheDocument();
	});
});