import React from "react";
import "@testing-library/jest-dom";
import {render, screen, waitFor} from "@testing-library/react";
import Auth from "customer/modules/Auth";

let mockedAuthGet: Promise<any> = Promise.resolve("auth redirect url");
jest.mock('customer/repositories', (): any => ({
	...jest.requireActual('customer/repositories'),
	AuthRepository: { get: () => mockedAuthGet },
}));
describe("Auth.tsx", () => {
	it("Renders correctly", () => {
		render(
			<Auth />
		);


	});
});