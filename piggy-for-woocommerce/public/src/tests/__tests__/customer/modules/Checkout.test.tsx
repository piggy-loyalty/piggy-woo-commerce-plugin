import React from "react";
import "@testing-library/jest-dom";
import {render, screen, waitFor} from "@testing-library/react";
import Profile from "customer/modules/Profile";


let mockedCustomerShow: Promise<any> = Promise.resolve({
	customer: {
		picture: "",
		firstName: "john",
		lastName: "doe",
		email: "johndoe@gmail.com"
	},
	creditBalance: 1,
	purchaseAmount: 99,
});
jest.mock('customer/repositories', (): any => ({
	...jest.requireActual('customer/repositories'),
	CustomerRepository: { show: () => mockedCustomerShow },
}));

const mockHistoryPush = jest.fn();
jest.mock('react-router', (): any => ({
	...jest.requireActual('react-router'),
	useHistory: () => ({
		push: mockHistoryPush,
	}),
}));

const mockErrorToast = jest.fn();
const mockSuccessToast = jest.fn();
jest.mock('shared/components', (): any => ({
	...jest.requireActual('shared/components'),
	useToast: () => ({
		errorToast: mockErrorToast,
		successToast: mockSuccessToast,
	}),
}));
describe("Checkout.tsx", () => {
	it("Renders correctly",  async () => {
		render(
			<Profile showActions={false} />
		);

		await waitFor(() => expect(screen.getByText("john doe")));
		await waitFor(() => expect(screen.getByText("jd")));
		await waitFor(() => expect(screen.getByText("johndoe@gmail.com")));
	});

	it("it redirects back to auth page when checkout show call failed ", async () => {
		mockedCustomerShow = Promise.reject("Something went wrong.");
		render(
			<Profile showActions={false} />
		);

		await waitFor(() => expect(mockHistoryPush).toHaveBeenCalledTimes(1));
		await waitFor(() => expect(mockHistoryPush).toHaveBeenCalledWith("/customer/auth"));
	});

});