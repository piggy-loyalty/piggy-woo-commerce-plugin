import React from "react";
import "@testing-library/jest-dom";
import {render, screen, waitFor} from "@testing-library/react";
import {MemoryRouter} from "react-router";
import Index from "admin/modules/index";

let mockedAdminReturnValue: Promise<any> = Promise.resolve(null);
let mockedLogReturnValue: Promise<any> = Promise.resolve(null);
let mockedSettingsReturnValue: Promise<any> = Promise.resolve(null);
jest.mock('admin/repositories', (): any => ({
	...jest.requireActual('admin/repositories'),
	AdminRepository: { index: () => mockedAdminReturnValue },
	LogRepository: { index: () => mockedLogReturnValue },
	SettingsRepository: { index: () => mockedSettingsReturnValue }
}));
jest.mock('shared/components', (): any => ({
	...jest.requireActual('shared/components'),
	Accordion: (props: {key: string, title: string, content: string}) => <div>{props.title}</div>,
	FormColor: () => null,
}));

describe("Test admin routing", () => {
	test("Expect to load overview page", async () => {
		mockedAdminReturnValue = Promise.resolve({id: "1", name: "webshop 1"});
		render(
			<MemoryRouter initialEntries={["/admin/overview"]} >
				<Index />
			</MemoryRouter>
		);

		await waitFor(() => expect(screen.getByText("deleteConfiguration")).toBeInTheDocument());
	});

	test("Expect to load overview page when on home", async () => {
		mockedAdminReturnValue = Promise.resolve({id: "1", name: "webshop 1"});
		render(
			<MemoryRouter initialEntries={["/"]} >
				<Index />
			</MemoryRouter>
		);

		await waitFor(() => expect(screen.getByText("deleteConfiguration")).toBeInTheDocument());
	});

	test("Expect to load logs page", async () => {
		mockedLogReturnValue = Promise.resolve({
			files: [
				{ name: "some name 1", content: ["some line 1"]},
				{ name: "some name 2", content: ["some line 2"]}
			],
			count: 2
		});
		render(
			<MemoryRouter initialEntries={["/admin/logs"]} >
				<Index />
			</MemoryRouter>
		);

		const query = screen.getByText((content, element) => {
			return element.tagName.toLowerCase() === 'h3' && content.startsWith('Logs')
		});
		await waitFor(() => expect(query).toBeInTheDocument());
	});

	test("Expect to load testsuite page", () => {
		render(
			<MemoryRouter initialEntries={["/admin/tests"]} >
				<Index />
			</MemoryRouter>
		);

		const query = screen.getByText((content, element) => {
			return element.tagName.toLowerCase() === 'h3' && content.startsWith('Test suite')
		});
		expect(query).toBeInTheDocument();
	});

	// test("Expect to load settings page",  async () => {
	// 	mockedSettingsReturnValue = Promise.resolve({
	// 		credits_include_taxes: true,
	// 		credits_include_shipping_cost: true,
	// 		allow_linking: true,
	// 		guest_flow: "optional",
	// 		user_flow: "optional",
	// 		language: "en",
	// 		language_options: [],
	// 		checkbox_layout: {
	// 			title: "Piggy",
	// 			titleColor: "#000",
	// 			subtitle: "Yes, I want to save points.",
	// 			subtitleColor: "#000"
	// 		}
	// 	});
	//
	// 	render(
	// 		<MemoryRouter initialEntries={["/admin/settings"]} >
	// 			<Index />
	// 		</MemoryRouter>
	// 	);
	//
	// 	const query = screen.getByText((content, element) => {
	// 		return element.tagName.toLowerCase() === 'h4' && content.startsWith('Flow settings')
	// 	});
	// 	await waitFor(() => expect(query).toBeInTheDocument());
	// });

	test("Expect to load settings page",  async () => {
		render(
			<MemoryRouter initialEntries={["/admin/settings"]} >
				<Index />
			</MemoryRouter>
		);

		await waitFor(() => expect(screen.getByText('Flow settings')).toBeInTheDocument());
		await waitFor(() => expect(screen.getByText('creditsSettings')).toBeInTheDocument());
		await waitFor(() => expect(screen.getByText('languageSettings')).toBeInTheDocument());
	});

	test("Expect to load flow settings page",  async () => {
		mockedSettingsReturnValue = Promise.resolve({
			allow_linking: true,
			guest_flow: "optional",
			user_flow: "optional",
		});

		render(
			<MemoryRouter initialEntries={["/admin/settings/flow"]} >
				<Index />
			</MemoryRouter>
		);

		const query = screen.getByText((content, element) => {
			return element.tagName.toLowerCase() === 'h3' && content.startsWith('Flow settings')
		});
		await waitFor(() => expect(query).toBeInTheDocument());
	});

	test("Expect to load credits settings page",  async () => {
		mockedSettingsReturnValue = Promise.resolve({
			credits_include_taxes: true,
			credits_include_shipping_cost: true,
		});

		render(
			<MemoryRouter initialEntries={["/admin/settings/credits"]} >
				<Index />
			</MemoryRouter>
		);

		const query = screen.getByText((content, element) => {
			return element.tagName.toLowerCase() === 'h3' && content.startsWith('creditsSettings')
		});
		await waitFor(() => expect(query).toBeInTheDocument());
	});

	test("Expect to load language settings page",  async () => {
		mockedSettingsReturnValue = Promise.resolve({
			credits_include_taxes: true,
			credits_include_shipping_cost: true,
		});

		render(
			<MemoryRouter initialEntries={["/admin/settings/language"]} >
				<Index />
			</MemoryRouter>
		);

		const query = screen.getByText((content, element) => {
			return element.tagName.toLowerCase() === 'h3' && content.startsWith('languageSettings')
		});
		await waitFor(() => expect(query).toBeInTheDocument());
	});
});