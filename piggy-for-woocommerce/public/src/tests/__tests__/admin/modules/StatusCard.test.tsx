import React from "react";
import "@testing-library/jest-dom";
import {render, screen, waitFor} from "@testing-library/react";
import windowLocation from "tests/helpers/mockWindowLocation";
import userEvent from "@testing-library/user-event";
import StatusCard from "admin/modules/overview/StatusCard";

let mockedAdminReturnValue: Promise<any> = Promise.resolve(null);
jest.mock('admin/repositories', (): any => ({
	...jest.requireActual('admin/repositories'),
	AdminRepository: { index: () => mockedAdminReturnValue },
}));

let mockedSetupReturnValue: Promise<any> = Promise.resolve({redirect_url: "redirect url"});
jest.mock('shared/repositories', (): any => ({
	...jest.requireActual('shared/repositories'),
	SetupRepository: { destroy: () => mockedSetupReturnValue },
}));

const mockedErrorToast = jest.fn();
jest.mock('shared/components', (): any => ({
	...jest.requireActual('shared/components'),
	useToast: () => ({
		errorToast: mockedErrorToast,
	}),
}));

describe("StatusCard.tsx", () => {
	it("Shows webshop name that is linked", async () => {
		mockedAdminReturnValue = Promise.resolve({ id: "1", name: "webshop 1"});

		render( <StatusCard /> );

		await waitFor(() => expect(screen.getByText("webshop 1")).toBeInTheDocument());
	});

	it("shows error toast when request failed", async () => {
		mockedAdminReturnValue = Promise.reject("Something went wrong.");
		render( <StatusCard /> );

		await waitFor(() => expect(mockedErrorToast).toHaveBeenCalledWith("Something went wrong."));
	});

	// it("Redirects user to setup after clicked on button", async () => {
	// 	window.location = windowLocation.mock("replace");
	//
	// 	render( <StatusCard /> );
	//
	// 	await waitFor(() => userEvent.click(screen.getByText("deleteConfiguration")));
	// 	await waitFor(() => expect(window.location.replace).toHaveBeenCalledWith("redirect url"));
	//
	// 	window.location = windowLocation.restore();
	// });
	//
	// it("Shows error toast when delete configuration call failed", async () => {
	// 	const message = "Something went wrong in delete configuration call.";
	// 	mockedSetupReturnValue = Promise.reject(message);
	//
	// 	render( <StatusCard /> );
	//
	// 	await waitFor(() => userEvent.click(screen.getByText("deleteConfiguration")));
	// 	await waitFor(() => expect(mockedErrorToast).toHaveBeenCalledWith(message));
	// });
});