import React from "react";
import "@testing-library/jest-dom";
import {render, screen, waitFor} from "@testing-library/react";
import Logs from "admin/modules/logs/Logs";
import windowLocation from "tests/helpers/mockWindowLocation";
import mockWindowOpen from "tests/helpers/mockWindowOpen";

let mockedLogReturnValue: Promise<any> = Promise.resolve(null);
let mockedDownloadUrl: Promise<any> = Promise.resolve(null);
jest.mock('admin/repositories', (): any => ({
	...jest.requireActual('admin/repositories'),
	LogRepository: {
		index: () => mockedLogReturnValue,
		download: () => mockedDownloadUrl
	},
}));

const mockedErrorToast = jest.fn();
jest.mock('shared/components', (): any => ({
	...jest.requireActual('shared/components'),
	Accordion: (props: {key: string, title: string, content: string}) => <div>{props.title}</div>,
	useToast: () => ({
		errorToast: mockedErrorToast,
	}),
}));

describe("Logs.tsx", () => {
	it("logs index call and loads content into component", async () => {
		mockedLogReturnValue = Promise.resolve({
			files: [
				{ name: "some name 1", content: ["some line 1"]},
				{ name: "some name 2", content: ["some line 2"]}
			],
			count: 2
		});

		render( <Logs /> );

		await waitFor(() => expect(screen.getByText("some name 1")).toBeInTheDocument());
		await waitFor(() => expect(screen.getByText("some name 2")).toBeInTheDocument());
	});

	it("shows error toast when logs index call fails", async () => {
		mockedLogReturnValue = Promise.reject("Something went wrong.");

		render( <Logs />);

		await waitFor(() => expect(mockedErrorToast).toBeCalledWith("Something went wrong."));
	});

	// TEST PAGINATION
	// TEST DOWNLOAD BUTTON

	// it("Gets the download url and opens it", async() => {
	// 	mockedLogReturnValue = Promise.resolve({
	// 		files: [
	// 			{ name: "some name 1", content: ["some line 1"]},
	// 			{ name: "some name 2", content: ["some line 2"]}
	// 		],
	// 		count: 2
	// 	});
	// 	mockedDownloadUrl = Promise.resolve( "redirect url" );
	// 	window.open = mockWindowOpen.mock();
	//
	// 	render( <Logs /> );
	//
	// 	await waitFor(() => expect(window.open).toHaveBeenCalledWith("redirect url"));
	//
	// 	window.open = mockWindowOpen.restore();
	// });
});