import React from "react";
import '@testing-library/jest-dom'
import {act, render, screen, waitFor} from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Flow from "admin/modules/settings/Flow";

let mockedSettingsIndexReturnValue: Promise<any> = Promise.resolve(null);
let mockedSettingsUpdateReturnValue: Promise<any> = Promise.resolve(null);
jest.mock('admin/repositories', (): any => ({
	...jest.requireActual('admin/repositories'),
	FlowSettingsRepository: {
		index: () => mockedSettingsIndexReturnValue,
		update: () => mockedSettingsUpdateReturnValue
	}
}));

const mockErrorToast = jest.fn();
const mockSuccessToast = jest.fn();
jest.mock('shared/components', (): any => ({
	...jest.requireActual('shared/components'),
	useToast: () => ({
		errorToast: mockErrorToast,
		successToast: mockSuccessToast,
	}),
}));

describe("Settings.tsx", () => {
	it("Renders correctly", async () => {
		render(<Flow />);

		await waitFor(() => expect(screen.getAllByText("Flow settings")).toHaveLength(1));
		await waitFor( () => expect(screen.getByText("allowCustomersToLinkAccountToPiggy")).toBeInTheDocument());
		await waitFor( () => expect(screen.getAllByLabelText("optional")).toHaveLength(2));
		await waitFor( () => expect(screen.getAllByLabelText("never")).toHaveLength(2));
		await waitFor( () => expect(screen.getAllByLabelText("always")).toHaveLength(2));
	});

	it("Sets values correctly after index call", async () => {
		mockedSettingsIndexReturnValue = Promise.resolve({
			allow_linking: true,
			guest_flow: "always",
			user_flow: "always",
		});

		render(<Flow />);

		await waitFor(() => expect(screen.getByLabelText("allowCustomersToLinkAccountToPiggy")).toBeChecked);
		await waitFor(() => expect(screen.getAllByLabelText("always")[0]).toBeChecked);
		await waitFor(() => expect(screen.getAllByLabelText("always")[1]).toBeChecked);
		await waitFor(() => expect(screen.getAllByLabelText("always")[2]).toBeChecked);
	});

	it("Shows error toast when index call failed", async () => {
		mockedSettingsIndexReturnValue = Promise.reject("Something went wrong.");

		render(<Flow />);

		await waitFor(() => expect(mockErrorToast).toBeCalledWith("Something went wrong."));
	});

	it("Updates settings when clicked on save button", async () => {
		mockedSettingsIndexReturnValue = Promise.resolve({
			allow_linking: true,
			guest_flow: "always",
			user_flow: "always",
		});
		mockedSettingsUpdateReturnValue = Promise.resolve({
			allow_linking: false,
			guest_flow: "never",
			user_flow: "never",
		});

		render(<Flow />);

		userEvent.click(screen.getByLabelText("allowCustomersToLinkAccountToPiggy"));
		userEvent.click(screen.getAllByLabelText("never")[0]);
		userEvent.click(screen.getAllByLabelText("never")[1]);

		const button = await waitFor(() => screen.getByText("save"));
		userEvent.click(button);

		await waitFor(() => expect(screen.getByLabelText("allowCustomersToLinkAccountToPiggy")).not.toBeChecked);
		await waitFor(() => expect(screen.getAllByLabelText("never")[0]).toBeChecked);
		await waitFor(() => expect(screen.getAllByLabelText("never")[1]).toBeChecked);

		await waitFor(() => expect(mockSuccessToast).toHaveBeenCalledTimes(1));
	});
});