import React from "react";
import '@testing-library/jest-dom'
import {render, screen} from "@testing-library/react";
import Overview from "admin/modules/settings/Overview";

// Work around because something goes wrong in FormColor from foundation.
// jest.mock('shared/components', (): any => ({
// 	...jest.requireActual('shared/components'),
// 	FormColor: () => null,
// }));
describe("Overview.test.tsx", () => {
	it("Shows 3 action cards",   () => {

		render(<Overview />);

		expect(screen.getByText("Flow settings")).toBeInTheDocument();
		expect(screen.getByText("creditsSettings")).toBeInTheDocument();
		expect(screen.getByText("languageSettings")).toBeInTheDocument();
	});
});