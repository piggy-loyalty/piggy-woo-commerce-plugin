import React from "react";
import '@testing-library/jest-dom'
import {act, render, screen, waitFor} from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Language from "admin/modules/settings/Language";

let mockedSettingsIndexReturnValue: Promise<any> = Promise.resolve(null);
let mockedSettingsUpdateReturnValue: Promise<any> = Promise.resolve(null);
jest.mock('admin/repositories', (): any => ({
	...jest.requireActual('admin/repositories'),
	LanguageSettingsRepository: {
		index: () => mockedSettingsIndexReturnValue,
		update: () => mockedSettingsUpdateReturnValue
	}
}));

const mockErrorToast = jest.fn();
const mockSuccessToast = jest.fn();
jest.mock('shared/components', (): any => ({
	...jest.requireActual('shared/components'),
	useToast: () => ({
		errorToast: mockErrorToast,
		successToast: mockSuccessToast,
	}),
}));

describe("Language.tsx", () => {
	it("Renders correctly", async () => {

		render(<Language />);

		await waitFor(() => expect(screen.getByText("languageSettings")).toBeInTheDocument());
	});

	it("Sets values correctly after index call", async () => {
		mockedSettingsIndexReturnValue = Promise.resolve({
			language: "en",
			language_options: [{ id: "en", label: "english"}],
		});

		render(<Language />);

		await waitFor(() => expect(screen.getByText("selectLanguage")).toBeInTheDocument());
	});

	it("Shows error toast when index call failed", async () => {
		mockedSettingsIndexReturnValue = Promise.reject("Something went wrong.");

		render(<Language />);

		await waitFor(() => expect(mockErrorToast).toBeCalledWith("Something went wrong."));
	});

	it("Updates settings when clicked on save button", async () => {
		mockedSettingsIndexReturnValue = Promise.resolve({
			language: "en",
			language_options: [{ id: "de", label: "german"}],
		});
		mockedSettingsUpdateReturnValue = Promise.resolve({
			language: "de",
		});

		render(<Language />);

		const button = await waitFor(() => screen.getByText("save"));
		userEvent.click(button);

		await waitFor(() => expect(screen.getByText("german")).toBeInTheDocument());
		await waitFor(() => expect(mockSuccessToast).toHaveBeenCalledTimes(1));
	});
});