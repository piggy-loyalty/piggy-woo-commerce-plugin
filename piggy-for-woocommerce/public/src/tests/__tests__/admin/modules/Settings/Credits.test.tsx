import React from "react";
import '@testing-library/jest-dom'
import {act, render, screen, waitFor} from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Credits from "admin/modules/settings/Credits";

let mockedSettingsIndexReturnValue: Promise<any> = Promise.resolve(null);
let mockedSettingsUpdateReturnValue: Promise<any> = Promise.resolve(null);
jest.mock('admin/repositories', (): any => ({
	...jest.requireActual('admin/repositories'),
	CreditsSettingsRepository: {
		index: () => mockedSettingsIndexReturnValue,
		update: () => mockedSettingsUpdateReturnValue
	}
}));

const mockErrorToast = jest.fn();
const mockSuccessToast = jest.fn();
jest.mock('shared/components', (): any => ({
	...jest.requireActual('shared/components'),
	useToast: () => ({
		errorToast: mockErrorToast,
		successToast: mockSuccessToast,
	}),
}));

describe("Credits.tsx", () => {
	it("Renders correctly", async () => {

		render(<Credits />);

		await waitFor(() => expect(screen.getByText("creditsSettings")).toBeInTheDocument());
	});

	it("Sets values correctly after index call", async () => {
		mockedSettingsIndexReturnValue = Promise.resolve({
			credits_include_taxes: true,
			credits_include_shipping_cost: true,
		});

		render(<Credits />);

		await waitFor(() => expect(screen.getByLabelText("creditCalculationIncludesTaxes")).toBeChecked);
		await waitFor(() => expect(screen.getByLabelText("creditCalculationIncludesShippingCost")).toBeChecked);
	});

	it("Shows error toast when index call failed", async () => {
		mockedSettingsIndexReturnValue = Promise.reject("Something went wrong.");

		render(<Credits />);

		await waitFor(() => expect(mockErrorToast).toBeCalledWith("Something went wrong."));
	});

	it("Updates settings when clicked on save button", async () => {
		mockedSettingsIndexReturnValue = Promise.resolve({
			credits_include_taxes: true,
			credits_include_shipping_cost: true,
		});
		mockedSettingsUpdateReturnValue = Promise.resolve({
			credits_include_taxes: false,
			credits_include_shipping_cost: false,
		});

		render(<Credits />);

		userEvent.click(screen.getByLabelText("creditCalculationIncludesShippingCost"));
		userEvent.click(screen.getByLabelText("creditCalculationIncludesTaxes"));

		const button = await waitFor(() => screen.getByText("save"));
		userEvent.click(button);

		await waitFor(() => expect(screen.getByLabelText("creditCalculationIncludesShippingCost")).not.toBeChecked);
		await waitFor(() => expect(screen.getByLabelText("creditCalculationIncludesTaxes")).not.toBeChecked);

		await waitFor(() => expect(mockSuccessToast).toHaveBeenCalledTimes(1));
	});
});