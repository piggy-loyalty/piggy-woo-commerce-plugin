import React from "react";
import "@testing-library/jest-dom";
import {render, screen, waitFor} from "@testing-library/react";
import Testsuite from "admin/modules/testsuite/Testsuite";
import userEvent from "@testing-library/user-event";

let mockedStagedReturnValue: Promise<any> = Promise.resolve(null );
jest.mock('admin/repositories', (): any => ({
	...jest.requireActual('admin/repositories'),
	StagedCreditReceptionRepository: { send: () => mockedStagedReturnValue },
}));


const mockErrorToast = jest.fn();
const mockSuccessToast = jest.fn();
jest.mock('shared/components', (): any => ({
	...jest.requireActual('shared/components'),
	useToast: () => ({
		errorToast: mockErrorToast,
		successToast: mockSuccessToast,
	}),
}));
describe("Testsuite.tsx", () => {

	it("Renders correctly", () => {
		render( <Testsuite /> );

		expect(screen.getByLabelText("email*")).toBeInTheDocument();
		expect(screen.getByLabelText("purchaseAmount*")).toBeInTheDocument();
		expect(screen.getByLabelText("credits")).toBeInTheDocument();
		expect(screen.getByText("sendStagedCreditReception")).toBeInTheDocument();
	});

	it("Sets all form data correctly", async () => {
		// mockedStagedReturnValue = Promise.resolve({ email: "test@test.nl", })
		render( <Testsuite /> );

		userEvent.type(screen.getByLabelText("email*"), "test@test.nl");
		userEvent.type(screen.getByLabelText("purchaseAmount*"), "10");
		userEvent.type(screen.getByLabelText("credits"), "11");

		expect(screen.getByDisplayValue("test@test.nl")).toBeInTheDocument();
		expect(screen.getByDisplayValue("10")).toBeInTheDocument();
		expect(screen.getByDisplayValue("11")).toBeInTheDocument();
	});

	it("Shows success message when staged call succeeded", async () => {
		mockedStagedReturnValue = Promise.resolve({ email: "test@test.nl", staged_credit_reception: { credits: "10"} });
		render( <Testsuite /> );

		userEvent.type(screen.getByLabelText("email*"), "test@test.nl");
		userEvent.type(screen.getByLabelText("purchaseAmount*"), "10");
		userEvent.type(screen.getByLabelText("credits"), "11");
		userEvent.click(screen.getByText("sendStagedCreditReception"));

		await waitFor(() => expect(mockSuccessToast).toBeCalledWith("successfullySendCreditsToEmail"));
	});

	it("Shows error message when staged call fails", async () => {
		mockedStagedReturnValue = Promise.reject("Something went wrong.");
		render( <Testsuite />);

		userEvent.type(screen.getByLabelText("email*"), "test@test.nl");
		userEvent.type(screen.getByLabelText("purchaseAmount*"), "10");
		userEvent.type(screen.getByLabelText("credits"), "11");
		userEvent.click(screen.getByText("sendStagedCreditReception"));

		await waitFor(() => expect(mockErrorToast).toBeCalledWith("Something went wrong."));
	});
});