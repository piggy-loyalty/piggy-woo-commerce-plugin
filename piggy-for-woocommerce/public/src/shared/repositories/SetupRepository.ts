import HttpClient from "shared/services/HttpClient";
import ApiErrorMapper from "shared/mappers/ApiErrorMapper";

class SetupRepository {

    healthCheck() {
        return HttpClient.get('setup')
            .catch((res: any) => {
                throw ApiErrorMapper(res);
            });
    }

    authenticate(credentials: {key: string, secret: string}) {
        return HttpClient.post('setup/authenticate', { key: credentials.key, secret: credentials.secret })
        .catch((res: any) => {
            throw ApiErrorMapper(res);
        });
    }

    destroy() {
        return HttpClient.post('setup/delete')
            .then((res: any) => {
                return res.data.data;
            })
            .catch((res: any) => {
                throw ApiErrorMapper(res);
            });
    }

    update(id: number) {
        return HttpClient.post('setup/update', { shop_id: id})
            .then((res: any) => {
                return res.data.data;
            })
            .catch((res: any) => {
                throw ApiErrorMapper(res);
            });
    }
}

export default new SetupRepository();