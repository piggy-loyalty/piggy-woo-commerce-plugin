import HttpClient from "shared/services/HttpClient";
import {ShopsMapper} from "shared/mappers/index";
import ApiErrorMapper from "shared/mappers/ApiErrorMapper";

class ShopsRepository {

	index() {
		return HttpClient.get('setup/shops')
			.then((res: any) => {
				return ShopsMapper(res.data.data);
			})
			.catch((res: any) => {
				throw ApiErrorMapper(res);
			});
	}
}

export default new ShopsRepository();