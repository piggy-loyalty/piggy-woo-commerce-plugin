export default interface ApiErrorInterface {
	message: string;
	errors: any[];
	inputErrors: any;
}