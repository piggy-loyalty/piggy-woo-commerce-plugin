import ApiErrorInterface from "shared/models/ApiErrorInterface";

export default class ApiError extends Error implements ApiErrorInterface {
	message: string;
	errors: any[];
	inputErrors: any;

	constructor(message: string) {
		super();
		this.message = message;
		this.errors = [];
		this.inputErrors = {};
	}
}