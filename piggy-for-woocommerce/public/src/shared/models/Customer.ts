import {CustomerInterface} from "shared/models";

export default class Customer implements CustomerInterface {
    id: number = 0;
    email: string = "";
    firstName: string = "";
    lastName: string = "";
    picture: string = "";

    getFullName(): string {
        if (this.firstName && this.lastName) {
            return this.firstName + " " + this.lastName;
        }

        if (this.firstName) {
            return this.firstName;
        }

        if (this.lastName) {
            return this.lastName;
        }

        return "";
    }
}
