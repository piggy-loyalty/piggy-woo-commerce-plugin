export default interface CustomerInterface {
    id: number;
    email: string;
    firstName: string;
    lastName: string;
    picture: string;

    getFullName(): string;
}
