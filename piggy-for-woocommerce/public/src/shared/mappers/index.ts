export { default as ShopsMapper } from "@piggy-loyalty/foundation/lib/mappers/shop/ShopsMapper";
export { default as ShopMapper } from "@piggy-loyalty/foundation/lib/mappers/shop/ShopMapper";
export { default as ApiErrorMapper } from "./ApiErrorMapper";
export { default as CustomerMapper } from "./CustomerMapper";
