import { Customer, CustomerInterface } from "shared/models";

const CustomerMapper = (res: any): CustomerInterface => {
    const customer = new Customer();

    customer.id = res.id;
    customer.email = res.email;
    customer.firstName = res.first_name ? res.first_name : "";
    customer.lastName = res.last_name ? res.last_name : "";
    customer.picture = res.picture ? res.picture : "";

    return customer;
};

export default CustomerMapper;
