import ShopInterface from "@piggy-loyalty/foundation/lib/models/shop/ShopInterface";
import {Shop} from "@piggy-loyalty/foundation/lib/models";

const ShopMapper = (res: any): ShopInterface => {
	const shop = new Shop();

	shop.id = res.id ? res.id : "";
	shop.name = res.name ? res.name : "";

	return shop
};

export default ShopMapper;
