import flatten from "lodash/flatten";
import ApiError from "shared/models/ApiError";

const ApiErrorMapper = (exception: any) => {
	const response = exception.response;

	let message = "error";

	if (response.data && response.data.message) {
		message = response.data.message;
	}

	if (response.message) {
		message = response.message;
	}

	const error = new ApiError(message);

	// Laravel input validation error mapping.
	if (response.data && response.data.errors) {
		// for each entry create an array, then flatten.
		error.errors = flatten(Object.values(response.data.errors));
		error.inputErrors = response.data.errors;
	}

	return error;
};

export default ApiErrorMapper;
