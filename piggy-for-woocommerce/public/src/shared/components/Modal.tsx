import React, {useEffect} from "react";
import {Button} from "shared/components/index";

type Props = {
    visible: boolean;
    title: string;
    children: any;
    content?: any;
    pending?: any;
    submitAction: () => void;
    submitLabel?: string;
    cancelAction: () => void;
    cancelLabel?: string;
};

export default function Modal(props: Props) {
    const {visible, title, content, pending, children, submitAction, submitLabel, cancelAction, cancelLabel} = props;

    useEffect(() => {
        if (visible) {
            document.onkeydown = (evt: KeyboardEvent) => {
                if ("key" in evt) {
                    const isEscape = evt.key === "Escape" || evt.key === "Esc";
                    if (isEscape) {
                        return cancelAction();
                    }
                }
            };
        }
    }, [visible]);

    return (
        <div className={`modal ${visible && "is-visible"}`}>
            <div className="modal__box">
                <div className="modal__header">
                    <h1 className="modal__heading">{title}</h1>
                    <button className="modal__close" type={"button"} onClick={cancelAction}>
                        <span>{"close"}</span>
                    </button>
                </div>
                {children}
                <form onSubmit={() => submitAction}>
                    <div className="modal__content">{content}</div>
                    <div className="modal__footer">
                        <Button style="primary" onClick={cancelAction} label={cancelLabel ? cancelLabel : "cancel"} disabled={pending} pending={pending}/>
                        <Button type={"submit"} style={"secondary"} label={submitLabel ? submitLabel : "submit"} disabled={pending} pending={pending}/>
                    </div>
                </form>
            </div>
            <div className="modal__overlay" onClick={cancelAction}/>
        </div>
    );
}