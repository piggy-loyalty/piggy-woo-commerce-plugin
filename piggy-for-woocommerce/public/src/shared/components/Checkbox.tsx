import * as React from "react";

type Props = {
	name: string;
	onChange: any;
	onClick: any;
	label?: string;
	type?: "checkbox" | "switch";
	checked: boolean;
	required?: boolean;
	hasError?: boolean;
	value?: string;
	disabled?: boolean;
	helperLeft?: string;
	helperRight?: string;
};

export default function FormCheckbox(props: Props) {
	const { name, onChange, label, type, checked, required, hasError, value, disabled, helperLeft, helperRight, onClick } = props;

	return (
		<div className={`input input--${type ? type : "checkbox"} ${disabled ? "is-disabled" : ""}`}>
			<label className="input__label">
				<input
					className={`input__element ${checked ? "is-checked" : ""} ${hasError ? "has-errors" : ""}`}
					type="checkbox"
					name={name}
					checked={checked}
					value={value}
					onChange={onChange}
					required={required}
					disabled={disabled}
					onClick={onClick}
				/>
				<div className={`input__${type ? type : "checkbox"}`} />
				{label && <div className="input__placeholder">{label} {required && <span className="input__placeholder-required">*</span>}</div>}
			</label>
			<div className="input__helpers">
				{helperLeft && <div className="input__helper input__helper--left">{helperLeft}</div>}
				{helperRight && <div className="input__helper input__helper--right">{helperRight}</div>}
			</div>
		</div>
	);
}