export function formatToSelectValues(object: any, keyNameForValue: string, keyNameForLabel: string) {
	return object.map((item: any) => {
		return {
			value: item[keyNameForValue],
			label: item[keyNameForLabel],
		};
	});
}

export const imageToWpUrl = (image: string) => {
	return `../wp-content/plugins/piggy-for-woocommerce/public/images/${image}`;
};