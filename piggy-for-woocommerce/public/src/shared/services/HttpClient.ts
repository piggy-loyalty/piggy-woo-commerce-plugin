import HttpClient from "@piggy-loyalty/foundation/lib/services/Http/HttpClient";

// @ts-ignore
const baseUrl = (typeof env === "undefined" ? window.location.origin : env.base_url);
// @ts-ignore
const nonce = (typeof env === "undefined" ? "" : env.nonce);

const httpClient = new HttpClient(
	baseUrl,
	{
		"X-WP-Nonce": nonce,
	},
	"token"
);

export default httpClient;