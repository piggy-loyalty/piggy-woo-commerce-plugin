import i18n from "i18next";
import {initReactI18next} from "react-i18next";
import Backend from "i18next-xhr-backend";
import React from "react";

export default i18n
    .use(Backend)
    .use(initReactI18next)
    .init({
        //@ts-ignore
        lng: env.wp_locale,
        debug: false,
        fallbackLng: "en",
        whitelist: ["nl", "de", "en"],
        backend: {
            loadPath: "https://storage.googleapis.com/translations.piggy.nl/webshop-plugins/{{lng}}.json",
        },
    });