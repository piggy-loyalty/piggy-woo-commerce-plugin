import React from "react";
import ReactDOM from "react-dom";
import "shared/i18n/i18n";
import "setup/assets/index.sass";
import Setup from "setup/modules";
import {MemoryRouter} from "react-router-dom";

window.addEventListener("load", init);

process.env = {
    nonce: env.nonce,
    wp_locale: env.wp_locale,
    redirect_url: env.redirect_url,
    plugin_dir_url: env.plugin_dir_url,
    base_url: env.base_url,
};

function init() {
    ReactDOM.render(
        <React.Suspense fallback={"Loading..."}>
            <MemoryRouter initialEntries={["/setup/credentials"]}>
                <Setup />
            </MemoryRouter>
        </React.Suspense>,
        document.getElementById("pfw-setup")
    );
}