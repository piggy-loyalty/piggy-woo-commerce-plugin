=== Piggy for WooCommerce ===
Contributors: edingermike
Donate link:
Tags: Piggy, Loyalty, Marketing
Requires at least: 5.2
Tested up to: 5.8
Stable tag: 1.2.1
Requires PHP: 7.2
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

The Piggy for WooCommerce plugin smoothly integrates your webshop with your Piggy loyalty system.

== Description ==

<b>What is Piggy?</b>

<a href="https://www.piggy.eu/">Piggy</a> is a digital loyalty platform that lets your customers collect points with every purchase, which they can then redeem for rewards.

<b>How will Piggy help grow your business?</b>

The Piggy loyalty platform helps you transform one-and-done customers into repeat customers. This then reduces your ad spend, while also increasing average cart value - and LifeTime Value.

<b>What can you do with Piggy?</b>
- Set your own rewards and change them anytime
- Automatically collect valuable customer data
- Create professional Emails and newsletters
- Offer digital giftcards
- Enjoy added visibility on the Piggy smartphone app

<b>What makes Piggy better than the competition?</b>

Unlike other WooCommerce loyalty plugins, Piggy is already an established platform, with 8,000 existing merchants and over 2,000,000 users.

The Piggy for WooCommerce plugin smoothly integrates your webshop with your Piggy loyalty system. It’s a flexible system that gives you complete control, every step of the way.

<b>How do I introduce Piggy to my customers?</b>

Once installed, you can either:
- Ask customers if they want to collect points at your webshop before checkout.
- Send points to customers, via email, after order completion, which they can then claim.
- Or both.

<b>Why should I choose Piggy?</b>

It’s free to get started. So download Piggy for a trial and see if it works for your business.

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload the plugin file `piggy-for-woocommerce.zip`, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Use the Settings->Piggy screen to configure the plugin.
4. Create your ClientID and Secret through the your (<a href="https://businness.piggy.eu">dashboard</a>)
5. Go back to your wordpress admin and do the health check to see if you are connected.
6. Choose your Piggy webshop to connect with your webshop. note: only piggy shops with type webshop are visible.

Now the plugin is active and configured! <br />

To configure more settings, go to the settings page in the plugin menu.

== Frequently Asked Questions ==

= How do I create a ClientID and Secret? =

You can create your ClientID and Secret in your dashboard (business.piggy.eu) under "account".

In the account section, you can find the tab "oauth clients" where you can create your client id and secret.
note: do not forget to set your redirect url.

= How do I make my "piggy account" page visible? =

You have to enable the following setting: "Allow customers to link their account to piggy.". If it is still not visible, try to flush your permalinks.

= I have created my credentials but customers can not link their account? =

If you do not set your redirect url, customers are not able to connect their account. Therefore set the redirect url in your dashboard (https://business.piggy.eu).

= My shops is not visible at the setup? =

Only shops of the type "webshop" are visible in the setup. If you do not see your shop, create a new shop in your dashboard


== Screenshots ==

1. Here you can find your settings in the webshops admin.
2. This is a customer who connected their piggy account with the webshop.
3. This is the widget that will be shown at checkout. Here you see how many points you will be saving.
4. The customer can decide if they want to save points, these points will be send to their email.

== Translations included ==

Nederlands (Dutch)
Deutsch (German)
English (US)

== Changelog ==

=1.0.3=
* Refresh shops button at setup step 3
* Create account link at setup step 1

=1.0.4=
* Minor bug fixes
* Added multiple languages for emails

=1.0.5=
* Connecting piggy account for different languages.

=1.0.6=
* Minor bug fixes

=1.0.7=
* Fix dependency bug

=1.0.8=
* Add restyling for admin panel

=1.0.9=
* Staged Credit Reception Checkbox settings.

=1.0.11=
* Minor bug fixes

=1.1.3=
* Fixed dependency conflict

== Upgrade Notice ==
