<?php

namespace PFW\Admin;

use PFW\Piggy\Controllers\CheckboxSettingsController;
use PFW\Piggy\Controllers\CreditsSettingsController;
use PFW\Piggy\Controllers\FlowSettingsController;
use PFW\Piggy\Controllers\LanguageSettingsController;
use PFW\Piggy\Controllers\LogController;
use PFW\Piggy\Controllers\SetupController;
use PFW\Piggy\Controllers\StagedCreditReceptionsController;
use PFW\Piggy\Controllers\StatusController;
use PFW\Piggy\Controllers\WebshopsController;
use WP_REST_Controller;
use WP_REST_Server;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class Routes
 * @package PFW\Admin
 */
class Routes extends WP_REST_Controller
{
    /**
     * @var string
     */
    protected $namespace;

    /**
     * @var
     */
    protected $base;

    /**
     * @var StagedCreditReceptionsController
     */
    protected $stagedCreditReceptionController;

    /**
     * @var CheckboxSettingsController
     */
    protected $checkboxSettingsController;

    /**
     * @var LogController
     */
    protected $logController;

    /**
     * @var FlowSettingsController
     */
    protected $flowSettingsController;

    /**
     * @var LanguageSettingsController
     */
    protected $languageSettingsController;

    /**
     * @var CreditsSettingsController
     */
    protected $creditsSettingsController;

    /**
     * @var WebshopsController
     */
    protected $webshopsController;

    /**
     * @var StatusController
     */
    protected $statusController;

    /**
     * @var SetupController
     */
    protected $setupController;

    /**
     * Routes constructor.
     */
    public function __construct()
    {
        $this->namespace = PFW_SLUG . '/v1';

        $this->webshopsController = new WebshopsController();
        $this->statusController = new StatusController();
        $this->stagedCreditReceptionController = new StagedCreditReceptionsController();
        $this->checkboxSettingsController = new CheckboxSettingsController();
        $this->logController = new LogController();
        $this->languageSettingsController = new LanguageSettingsController();
        $this->flowSettingsController = new FlowSettingsController();
        $this->creditsSettingsController = new CreditsSettingsController();
        $this->setupController = new SetupController();
    }

    /**
     *
     */
    public function register_routes()
    {
        /**
         * /wp-json/pfw/v1/admin
         */
        register_rest_route( $this->namespace, '/admin', [
            [
                'methods'   => WP_REST_Server::READABLE,
                'callback'  => [$this->webshopsController, 'show'],
                'permission_callback' => [$this, 'admin_permissions_check'],
                'args'      => [],
            ]
        ]);

        /**
         * /wp-json/pfw/v1/admin/status
         */
        register_rest_route( $this->namespace, '/admin/status', [
            [
                'methods'   => WP_REST_Server::READABLE,
                'callback'  => [$this->statusController, 'index'],
                'permission_callback' => [$this, 'admin_permissions_check'],
                'args'      => [],
            ]
        ]);

        /**
         * /wp-json/pfw/v1/admin/logs
         */
        register_rest_route( $this->namespace, '/admin/logs', [
            [
                'methods'   => WP_REST_Server::READABLE,
                'callback'  => [$this->logController, 'index'],
                'permission_callback' => [$this, 'admin_permissions_check'],
                'args'      => $this->get_endpoint_args_for_item_schema(true),
            ],
        ]);

        /**
         * /wp-json/pfw/v1/admin/logs
         */
        register_rest_route( $this->namespace, '/admin/logs/download', [
            [
                'methods'   => WP_REST_Server::CREATABLE,
                'callback'  => [$this->logController, 'download'],
                'permission_callback' => [$this, 'admin_permissions_check'],
                'args'      => [],
            ],
        ]);

        /**
         * /wp-json/pfw/v1/admin/staged-credit-receptions
         */
        register_rest_route( $this->namespace, '/admin/staged-credit-receptions', [
            [
                'methods'   => WP_REST_Server::CREATABLE,
                'callback'  => [$this->stagedCreditReceptionController, 'send'],
                'permission_callback' => [$this, 'admin_permissions_check'],
                'args'      => $this->get_endpoint_args_for_item_schema(true),
            ]
        ]);

        /** ----------------- SETTINGS ROUTES ----------------- */

        /**
         * /wp-json/pfw/v1/admin/settings/credits
         */
        register_rest_route( $this->namespace, '/admin/settings/credits', [
            [
                'methods'   => WP_REST_Server::READABLE,
                'callback'  => [$this->creditsSettingsController, 'index'],
                'permission_callback' => [$this, 'admin_permissions_check'],
                'args'      => [],
            ]
        ]);

        /**
         * /wp-json/pfw/v1/admin/settings/credits/update
         */
        register_rest_route( $this->namespace, '/admin/settings/credits/update', [
            [
                'methods'   => WP_REST_Server::CREATABLE,
                'callback'  => [$this->creditsSettingsController, 'update'],
                'permission_callback' => [$this, 'admin_permissions_check'],
                'args'      => $this->get_endpoint_args_for_item_schema(true),
            ]
        ]);

        /**
         * /wp-json/pfw/v1/admin/settings/checkbox
         */
        register_rest_route( $this->namespace, '/admin/settings/checkbox', [
            [
                'methods'   => WP_REST_Server::READABLE,
                'callback'  => [$this->checkboxSettingsController, 'index'],
                'permission_callback' => [$this, 'admin_permissions_check'],
                'args'      => [],
            ]
        ]);

        /**
         * /wp-json/pfw/v1/admin/settings/checkbox/update
         */
        register_rest_route( $this->namespace, '/admin/settings/checkbox/update', [
            [
                'methods'   => WP_REST_Server::CREATABLE,
                'callback'  => [$this->checkboxSettingsController, 'update'],
                'permission_callback' => [$this, 'admin_permissions_check'],
                'args'      => $this->get_endpoint_args_for_item_schema(true),
            ]
        ]);

        /**
         * /wp-json/pfw/v1/admin/settings/language
         */
        register_rest_route( $this->namespace, '/admin/settings/language', [
            [
                'methods'   => WP_REST_Server::READABLE,
                'callback'  => [$this->languageSettingsController, 'index'],
                'permission_callback' => [$this, 'admin_permissions_check'],
                'args'      => [],
            ]
        ]);

        /**
         * /wp-json/pfw/v1/admin/settings/language/update
         */
        register_rest_route( $this->namespace, '/admin/settings/language/update', [
            [
                'methods'   => WP_REST_Server::CREATABLE,
                'callback'  => [$this->languageSettingsController, 'update'],
                'permission_callback' => [$this, 'admin_permissions_check'],
                'args'      => $this->get_endpoint_args_for_item_schema(true),
            ]
        ]);

        /**
         * /wp-json/pfw/v1/admin/settings/flow
         */
        register_rest_route( $this->namespace, '/admin/settings/flow', [
            [
                'methods'   => WP_REST_Server::READABLE,
                'callback'  => [$this->flowSettingsController, 'index'],
                'permission_callback' => [$this, 'admin_permissions_check'],
                'args'      => [],
            ]
        ]);

        /**
         * /wp-json/pfw/v1/admin/settings/flow/update
         */
        register_rest_route( $this->namespace, '/admin/settings/flow/update', [
            [
                'methods'   => WP_REST_Server::CREATABLE,
                'callback'  => [$this->flowSettingsController, 'update'],
                'permission_callback' => [$this, 'admin_permissions_check'],
                'args'      => $this->get_endpoint_args_for_item_schema(true),
            ]
        ]);

        /** ----------------- SETUP ROUTES ----------------- */

        /**
         * /wp-json/pfw/v1/setup
         */
        register_rest_route($this->namespace, '/setup', [
            [
                'methods'   => WP_REST_Server::READABLE,
                'callback'  => [$this->setupController, 'ping'],
                'permission_callback' => [$this, 'setup_permission_check'],
                'args' => [],
            ]
        ]);

        /**
         * /wp-json/pfw/v1/setup/update
         */
        register_rest_route($this->namespace, '/setup/update', [
            [
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => [$this->setupController, 'update'],
                'permission_callback' => [$this, 'setup_permission_check'],
                'args'      => $this->get_endpoint_args_for_item_schema(true),
            ]
        ]);

        /**
         * /wp-json/pfw/v1/setup/delete
         */
        register_rest_route($this->namespace, '/setup/delete', [
            [
                'methods'   => WP_REST_Server::CREATABLE,
                'callback'  => [$this->setupController, 'delete'],
                'permission_callback' => [$this, 'delete_configuration_permissions_check'],
                'args'      => [],
            ]
        ]);

        /**
         * /wp-json/pfw/v1/setup/authenticate
         */
        register_rest_route($this->namespace, '/setup/authenticate', [
            [
                'methods'   => WP_REST_Server::CREATABLE,
                'callback'  => [$this->setupController, 'authenticate'],
                'permission_callback' => [$this, 'setup_permission_check'],
                'args'      => $this->get_endpoint_args_for_item_schema(true),
            ]
        ]);

        /**
         * /wp-json/pfw/v1/setup/shops
         */
        register_rest_route($this->namespace, '/setup/shops', [
            [
                'methods' => WP_REST_Server::READABLE,
                'callback' => [$this->webshopsController, 'index'],
                'permission_callback' => [$this, 'setup_permission_check'],
                'args' => [],
            ]
        ]);
    }

    /**
     * @return bool
     */
    public function admin_permissions_check()
    {
        if(! current_user_can('administrator') ) {
            return false;
        }

        $accessToken = get_option(PFW_SLUG . "_client_token");
        $shopId = get_option(PFW_SLUG . "_shop_id");

        if ($accessToken && $shopId) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function setup_permission_check()
    {
        if(! current_user_can('administrator') ) {
            return false;
        }

        $clientToken = get_option(PFW_SLUG . "_client_token");
        $shopId = get_option(PFW_SLUG . "_shop_id");

        if ($clientToken && $shopId) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function delete_configuration_permissions_check()
    {
        if(! current_user_can('administrator') ) {
            return false;
        }

        $accessToken = get_option(PFW_SLUG . "_client_token");
        $shopId = get_option(PFW_SLUG . "_shop_id");

        if ($accessToken && $shopId) {
            return true;
        }

        return false;
    }

    /**
     * @return SetupController
     */
    public function getSetupController()
    {
        return $this->setupController;
    }

    /**
     * @return WebshopsController
     */
    public function getWebshopsController()
    {
        return $this->webshopsController;
    }
}