<?php

namespace PFW\Admin;
defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

use PFW\Base;
use PFW\Piggy\Services\OAuthService;

/**
 * Class Admin
 * @package PFW\Admin
 */
class Admin extends Base
{
    /**
     * @var string
     */
    protected $componentId = PFW_SLUG . "-admin";

    /**
     * @var string
     */
    protected $script = "admin.js";

    /**
     * Admin constructor.
     */
    public function __construct()
    {
        parent::__construct();

        if(!$this->pluginIsConfigured()) {
           $this->componentId = PFW_SLUG . "-setup";
           $this->script = "setup.js";
        }
    }

    /**
     *
     */
    public function enqueue_scripts()
    {
        parent::enqueue_scripts();

        $env_variables = [
            "wp_locale" => substr(get_locale(), 0,2),
            "nonce" => wp_create_nonce("wp_rest"),
            "base_url" => get_site_url(null, '/wp-json/pfw/v1/'),
            "redirect_url" => (new OAuthService())->getRedirectUri(),
            "plugin_dir_url" => PFW_PLUGIN_DIR
        ];

        wp_register_script($this->componentId . "-script", PFW_SCRIPTS_PATH . $this->script, array(), false, true);
        wp_enqueue_script($this->componentId . "-script");
        wp_localize_script( $this->componentId . "-script", "env", $env_variables );
    }

    /**
     *
     */
    public function add_to_menu()
    {
        add_menu_page($this->componentId, "Piggy", "manage_options", $this->componentId, [
            $this,
            "render_component_library"
        ], PFW_PUBLIC_PATH . "/images/logo_p.svg", 59);
    }

    /**
     * Render the content of the menu page.
     */
    public function render_component_library()
    {
        echo '<div id="' . $this->componentId . '" class="wrap"></div>';
    }


    /**
     * @return string
     */
    public function getComponentId()
    {
        return $this->componentId;
    }

    /**
     * @return string
     */
    public function getScript()
    {
        return $this->script;
    }

}