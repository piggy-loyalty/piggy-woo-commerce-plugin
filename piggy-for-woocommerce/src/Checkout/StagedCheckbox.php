<?php

namespace PFW\Checkout;

use PFW\Base;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class StagedCheckbox
 * @package PFW\Checkout
 */
class StagedCheckbox extends Base
{
    /**
     * @var string
     */
    private $componentId = PFW_SLUG . '-staged-credit-reception-checkbox';

    /**
     * @var string
     */
    private $subtitle;

    /**
     * @var string
     */
    private $moreInfoDescription;

    /**
     * @var string
     */
    private $moreInfoUrl;

    /**
     * Checkbox constructor.
     * @param $form
     */
    public function __construct()
    {
        $this->subtitle = get_option(PFW_SLUG . "_staged_checkbox_text") ? get_option(PFW_SLUG . "_staged_checkbox_text") : "Ja, ik wil graag punten sparen.";
        $this->moreInfoDescription = get_option(PFW_SLUG . "_staged_checkbox_url_text");
        $this->moreInfoUrl = get_option(PFW_SLUG . "_staged_checkbox_url");
    }

    /**
     * @param $form
     */
    public function render_checkbox($form)
    {
        echo '<div class="cw_custom_class" id="piggy_checkout_checkbox">';
        echo '    <div class="card">';
        echo '        <div><a class="logo" href="https://www.piggy.eu/app" target="blank"><img src="' . site_url() . '/wp-content/plugins/' . PFW_PLUGIN_NAME . '/public/images/logo.svg" alt="Piggy" /></a></div>';
        echo '        <div>';
                            woocommerce_form_field($this->componentId, array(
                                'type' => 'checkbox',
                                'label' => $this->subtitle,
                                'required' => false,
                            ),  $form->get_value($this->componentId));
        echo '        </div>';

        if($this->moreInfoUrl && $this->moreInfoDescription) {
            echo '  <a class="custom_anchor" href="' . $this->moreInfoUrl . '" target="blank"><span class="checkbox__title">' . $this->moreInfoDescription .'</span></a>';
        }
        echo '    </div>';
        echo '</div>';
    }

    /**
     * @return string
     */
    public function getComponentId(): string
    {
        return $this->componentId;
    }
}