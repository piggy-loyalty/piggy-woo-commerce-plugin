<?php

namespace PFW\Checkout;
use PFW\Base;
use PFW\Piggy\Repositories\OAuthTokenRepository;
use PFW\Piggy\Repositories\UserRepository;
use PFW\Piggy\Services\OAuthService;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class CustomerWidget
 * @package PFW\Checkout
 */
class CustomerWidget extends Base
{
    /**
     * @var string
     */
    private $componentId = PFW_SLUG . '-customer';

    /**
     * CustomerWidget constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     */
    public function enqueue_scripts()
    {
        parent::enqueue_scripts();

        if ($_SERVER["REQUEST_URI"] == is_checkout()) {

            wp_register_script(PFW_SLUG . "-customer-script", PFW_SCRIPTS_PATH . "customer.js", array(), false, true);
            wp_enqueue_script(PFW_SLUG . "-customer-script");

            $env_variables = [
                "wp_locale" => substr(get_locale(), 0,2),
                "base_url" => get_site_url(null, '/wp-json/pfw/v1/'),
                "client_id" => get_option(PFW_SLUG . "_client_id"),
                "redirect_uri" => (new OAuthService())->getRedirectUri(),
                "nonce" => wp_create_nonce("wp_rest"),
                "component" => "checkout"
            ];

            wp_localize_script(PFW_SLUG . "-customer-script", "env", $env_variables);
        }
    }

    /**
     *
     */
    public function render_component_library()
    {
        echo '<div id="' . $this->componentId . '" class="wrap"></div>';
    }
}