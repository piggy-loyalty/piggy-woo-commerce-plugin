<?php

namespace PFW\Checkout;
defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

use Exception;
use PFW\Base;
use PFW\Piggy\Services\CreditReceptionsService;
use PFW\Piggy\Services\OAuthService;
use PFW\Piggy\Services\StagedCreditReceptionsService;
use WC_Abstract_Order;
use WC_Order;

/**
 * Class Checkout
 * @package PFW\Public
 */
class Checkout extends Base
{
    /**
     * @var StagedCreditReceptionsService
     */
    private $stagedCreditReceptionsService;

    /**
     * @var CreditReceptionsService
     */
    private $creditReceptionsService;

    /**
     * @var OAuthService
     */
    private $oauthService;
    /**
     * @var StagedCheckbox
     */
    private $stagedCheckbox;
    /**
     * @var CustomerWidget
     */
    private $customerWidget;

    /**
     * Checkout constructor.
     */
    public function __construct()
    {
        $this->stagedCreditReceptionsService = new StagedCreditReceptionsService();
        $this->creditReceptionsService = new CreditReceptionsService();
        $this->oauthService = new OAuthService();
        $this->customerWidget = new CustomerWidget();
        $this->stagedCheckbox = new StagedCheckbox();

        parent::__construct();
    }

    /**
     * @param $form
     */
    public function add_to_form($form)
    {
        if (!$this->pluginIsConfigured()) {
            return;
        }

        if($this->shouldAddOAuthWidget()) {
            $this->customerWidget->render_component_library();
            return;
        }

        if($this->shouldAddStagedWidget()) {
            // show checkbox to save points.
            $this->stagedCheckbox->render_checkbox($form);
        }

        return;
    }

    /**
     * @param int $orderId
     */
    public function save_order_meta($orderId)
    {
        $order = wc_get_order($orderId);
        if (!$order instanceof WC_Order) {
            $this->logger->error( "Order not found for order: " . $orderId);
            return;
        }

        if (isset($_POST[$this->stagedCheckbox->getComponentId()])) {
            $order->add_meta_data(PFW_SLUG . "_has_enabled_staged_checkbox", true);
            $this->logger->info("Saved order meta, checkbox checked : true");
        } else {
            $order->add_meta_data(PFW_SLUG . "_has_enabled_staged_checkbox", false);
            $this->logger->info("Saved order meta, checkbox checked : false");
        }

        $order->save();
    }

    /**
     * @param WC_Order $order
     * @throws Exception
     */
    public function on_payment_completed($order)
    {
        $order = wc_get_order($order);
        if (!$order instanceof WC_Order) {
            $this->logger->error("Order not found.");
            return;
        }

        $allowLinking = get_option(PFW_SLUG . "_allow_linking");
        $flow = get_option(PFW_SLUG . "_guest_flow_option");
        $email = $order->get_billing_email();
        $credits = $this->getCredits($order);
        $checkboxChecked = $order->get_meta(PFW_SLUG . "_has_enabled_staged_checkbox");
        $userId = $order->get_user_id();

        // Check if account linking is permitted.
        if($allowLinking) {
            // check if logged in user is linked
            if($customer = $this->oauthService->profile($userId)) {
                $this->log("info", "Customer " . $customer->getEmail() . " is linked, and will receive " . $credits . " credits.");
                $this->creditReceptionsService->handle($customer->getEmail(), $credits);    // sendCR

                return;
            }
        }

        if($userId) {
            // change guest flow to logged in user flow
            $this->log("info", "Change flow to user flow because customer is logged in.");
            $flow = get_option(PFW_SLUG . "_user_flow_option");
        }

        if($flow == "never") {
            $this->log("info", "Flow is never, will not send staged cr.");
            return;
        }

        if($flow == "optional") {
            if (!$checkboxChecked) {
                $this->log("info", "Customer decided to not check the checkbox for staged: " . $email);
                return;
            }
        }

        $this->log("info", "Going to send staged CR to " . $email);

        // handle staged CR.
        $this->stagedCreditReceptionsService->handle($email, $credits);

        return;
    }

    /**
     * @param WC_Order $order
     * @return int
     */
    public function getCredits($order)
    {
        $amount = $order->get_subtotal();
        $taxAmount = $order->get_total_tax();
        $shippingCost = $order->get_shipping_total();

        $credits = $this->creditReceptionsService->calculateCredits($amount, $taxAmount, $shippingCost);

        return $credits;
    }

    /**
     * @param StagedCreditReceptionsService $stagedCreditReceptionsService
     */
    public function setStagedCreditReceptionsService($stagedCreditReceptionsService)
    {
        $this->stagedCreditReceptionsService = $stagedCreditReceptionsService;
    }

    /**
     * @param CreditReceptionsService $creditReceptionsService
     */
    public function setCreditReceptionsService($creditReceptionsService)
    {
        $this->creditReceptionsService = $creditReceptionsService;
    }

    /**
     * @param OAuthService $oauthService
     */
    public function setOauthService($oauthService)
    {
        $this->oauthService = $oauthService;
    }

    /**
     * @param CustomerWidget $customerWidget
     */
    public function setCustomerWidget(CustomerWidget $customerWidget): void
    {
        $this->customerWidget = $customerWidget;
    }

    /**
     * @param StagedCheckbox $stagedCheckbox
     */
    public function setStagedCheckbox(StagedCheckbox $stagedCheckbox): void
    {
        $this->stagedCheckbox = $stagedCheckbox;
    }
}