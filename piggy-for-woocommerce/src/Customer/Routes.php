<?php

namespace PFW\Customer;

use PFW\Piggy\Controllers\CustomerController;
use WP_REST_Controller;
use WP_REST_Server;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class Routes
 * @package PFW\Customer
 */
class Routes extends WP_REST_Controller
{
    /**
     * @var string
     */
    protected $namespace;

    /**
     * @var CustomerController
     */
    protected $customerController;

    /**
     * Routes constructor.
     */
    public function __construct()
    {
        $this->namespace = PFW_SLUG . '/v1';
        $this->base = "customer";
        $this->customerController = new CustomerController();
    }

    public function register_routes()
    {
        /**
         * /wp-json/pfw/v1/customer/profile
         */
        register_rest_route( $this->namespace, '/customer/profile', [
            [
                'methods'   => WP_REST_Server::READABLE,
                'callback'  => [$this->customerController, 'show'],
                'permission_callback' => [],
                'args'      => $this->get_endpoint_args_for_item_schema(true),
            ]
        ]);
    }
}