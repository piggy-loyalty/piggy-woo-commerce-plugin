<?php

namespace PFW\Customer;
defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

use PFW\Base;

/**
 * Class Customer
 * @package PFW\Customer
 */
class Customer extends Base
{
    /**
     * @var string
     */
    protected $pageSlug;

    /**
     * @var string
     */
    protected $componentId;

    /**
     * Customer constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->pageSlug = "piggy-account";
        $this->componentId = PFW_SLUG . "-customer";
    }

    /**
     *
     */
    public function enqueue_scripts()
    {
        parent::enqueue_scripts();

        if ($_SERVER["REQUEST_URI"] == is_account_page()) {
            wp_register_script(PFW_SLUG . "-customer-script", PFW_SCRIPTS_PATH . "customer.js", array(), false, true);
            wp_enqueue_script(  PFW_SLUG . "-customer-script");

            $env_variables = [
                "wp_locale" => substr(get_locale(), 0,2),
                "nonce" => wp_create_nonce("wp_rest"),
                "base_url" => get_site_url(null, '/wp-json/pfw/v1/'),
                "component" => "profile",
                "plugin_dir_url" => PFW_PLUGIN_DIR
            ];
            wp_localize_script( PFW_SLUG . "-customer-script", "env", $env_variables );
        }
    }

    /**
     *
     */
    public function add_customer_page()
    {
        if($this->pluginIsConfigured()) {
            if (get_option(PFW_SLUG . "_allow_linking")) {
                add_filter("woocommerce_account_menu_items", [$this, "add_menu_item"]);
                add_action("init", [$this, "add_endpoint"]);
                add_filter("woocommerce_account_" . $this->pageSlug . "_endpoint", [$this, "render_component_library"]);
            }
        }
    }

    /**
     *
     */
    public function render_component_library()
    {
        echo '<div id="' . $this->componentId . '" class="wrap"></div>';
    }

    /**
     * @param $menu_links
     * @return array
     */
    public function add_menu_item($menu_links)
    {
        $new = [$this->pageSlug => "Piggy Account"];

        $menu_links_start = array_slice($menu_links, 0, 1, true);
        $menu_links_end = array_slice($menu_links, 1, NULL, true);
        $menu_links_start = array_merge($menu_links_start, $new);
        $menu_links = array_merge($menu_links_start, $menu_links_end);

        return $menu_links;
    }

    /**
     *
     */
    public function add_endpoint()
    {
        add_rewrite_endpoint($this->pageSlug, EP_ROOT | EP_PAGES);
        if(!get_option(PFW_SLUG . "_flushed_rewrite_rules")) {
            flush_rewrite_rules();
            update_option(PFW_SLUG . "_flushed_rewrite_rules", true);
        }
    }
}