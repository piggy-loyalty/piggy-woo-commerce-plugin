<?php

namespace PFW;

use PFW\Piggy\Logger;
use PFW\Piggy\Services\OAuthService;
use PFW\Piggy\Session;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class Base
 * @package PFW\Includes\Core\Base
 */
class Base
{
    /**
     * The name of this plugin.
     */
    protected $plugin_name;

    /**
     * The version of this plugin.
     */
    protected $version;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var OAuthService
     */
    protected $authService;

    /**
     * Base constructor.
     * @param $plugin_name
     * @param $version
     */
    public function __construct()
    {
        $this->plugin_name = PFW_PLUGIN_NAME;
        $this->version = PFW_VERSION;
        $this->logger = new Logger();
        $this->session = new Session();
        $this->authService = new OAuthService();
    }

    /**
     * Register the stylesheets for the admin area.
     */
    public function enqueue_styles()
    {
        $style = "";

        // Because get_admin_page_title() function is not found on webshop.
        if (is_admin()) {
            if (get_admin_page_title() == "pfw-setup") {
                $style = 'setup.css';
            } else if (get_admin_page_title() == "pfw-admin") {
                $style = 'admin.css';
            }
        } else {
            if (is_checkout() || is_account_page()) {
                $style = 'customer.css';
            }
        }

        if ($style) {
            wp_register_style(PFW_SLUG . '-style', PFW_STYLES_PATH . $style);
            wp_enqueue_style(PFW_SLUG . '-style');
        }
    }

    /**
     * Register the JavaScript for the admin area.
     */
    public function enqueue_scripts()
    {
        // Because get_admin_page_title() function is not found on webshop.
        if (is_admin()) {
            if (get_admin_page_title() == "pfw-setup" || get_admin_page_title() == "pfw-admin") {
                wp_register_script(PFW_SLUG . '-vendor-script', PFW_SCRIPTS_PATH . 'vendor-chunk.js', array(), false, true);
                wp_enqueue_script(PFW_SLUG . '-vendor-script');
            }
        } else {
            if (is_checkout() || is_account_page()) {
                wp_register_script(PFW_SLUG . '-vendor-script', PFW_SCRIPTS_PATH . 'vendor-chunk.js', array(), false, true);
                wp_enqueue_script(PFW_SLUG . '-vendor-script');
            }
        }
    }

    /**
     * @param $type
     * @param $message
     */
    public function log($type, $message)
    {
        $this->logger->$type($message);
    }

    /**
     * @return bool
     */
    public function pluginIsConfigured()
    {
        $accessToken = get_option(PFW_SLUG . "_client_token");
        $shopId = get_option(PFW_SLUG . "_shop_id");

        if ($accessToken && $shopId) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function shouldAddStagedWidget()
    {
        $userFlow = get_option(PFW_SLUG . "_user_flow_option");
        $guestFlow = get_option(PFW_SLUG . "_guest_flow_option");

        if (get_current_user_id()) {
            if ($userFlow == "optional") {
                return true;
            }

            return false;
        }

        if ($guestFlow == "optional") {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function shouldAddOAuthWidget()
    {
        $allowLinking = get_option(PFW_SLUG . "_allow_linking");
        if ($allowLinking && $this->userIsLinked()) {
            return true;
        }

        return false;
    }

    /**
     * @param null $userId
     * @return bool
     */
    public function userIsLinked($userId = null)
    {
        if(!get_current_user_id()) {
            return false;
        }

        if (!$userId) {
            $userId = get_current_user_id();
        }

        return $this->authService->isUserLinked($userId);
    }

    /**
     * @param Logger $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param OAuthService $authService
     */
    public function setAuthService($authService)
    {
        $this->authService = $authService;
    }
}