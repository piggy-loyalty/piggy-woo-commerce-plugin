<?php

namespace PFW\Piggy\Models;

use PFW\Piggy\Models\Interfaces\UserInterface;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class User
 * @package PFW\Piggy\Models
 */
class User implements UserInterface
{
    /**
     * @var
     */
    private $id;

    /**
     * @var
     */
    private $username;

    /**
     * @var
     */
    private $slug;

    /**
     * @var
     */
    private $email;

    /**
     * @var
     */
    private $status;

    /**
     * @var
     */
    private $OAuthToken;
    
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getOAuthToken()
    {
        return $this->OAuthToken;
    }

    /**
     * @param $token
     */
    public function setOAuthToken($token)
    {
        $this->OAuthToken = $token;
    }

}