<?php

namespace PFW\Piggy\Models\Interfaces;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Interface OAuthTokenInterface
 * @package PFW\Piggy\Models\Interfaces
 */
interface OAuthTokenInterface
{
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @param mixed $id
     */
    public function setId($id);

    /**
     * @return mixed
     */
    public function getUser();

    /**
     * @param mixed $user
     */
    public function setUser($user);

    /**
     * @return mixed
     */
    public function getAccessToken();

    /**
     * @param mixed $accessToken
     */
    public function setAccessToken($accessToken);

    /**
     * @return mixed
     */
    public function getRefreshToken();

    /**
     * @param mixed $refreshToken
     */
    public function setRefreshToken($refreshToken);

    /**
     * @return mixed
     */
    public function getExpiresIn();
    /**
     * @param mixed $expiresIn
     */
    public function setExpiresIn($expiresIn);

    /**
     * @return mixed
     */
    public function getCreatedAt();

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt);

    /**
     * @return mixed
     */
    public function getUpdatedAt();

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt);
}