<?php

namespace PFW\Piggy\Models\Interfaces;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Interface UserInterface
 * @package PFW\Piggy\Models\Interfaces
 */
interface UserInterface
{
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return mixed
     */
    public function getUsername();

    /**
     * @param mixed $username
     */
    public function setUsername($username);

    /**
     * @return mixed
     */
    public function getSlug();

    /**
     * @param mixed $slug
     */
    public function setSlug($slug);

    /**
     * @return mixed
     */
    public function getEmail();

    /**
     * @param mixed $email
     */
    public function setEmail($email);

    /**
     * @return mixed
     */
    public function getStatus();

    /**
     * @param mixed $status
     */
    public function setStatus($status);

    /**
     * @return mixed
     */
    public function getOAuthToken();

    /**
     * @param $token
     */
    public function setOAuthToken($token);
}