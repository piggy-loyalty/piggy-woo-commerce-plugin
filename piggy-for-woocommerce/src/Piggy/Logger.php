<?php

namespace PFW\Piggy;
defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

use PFW\Container\Monolog\Formatter\LineFormatter;
use PFW\Container\Monolog\Handler\StreamHandler;
use PFW\Container\Monolog\Logger as Monolog;

/**
 * Class Logger
 * @package PFW\Piggy
 */
class Logger
{
    /**
     * @var Monolog
     */
    private $log;

    /**
     * Logger constructor.
     */
    public function __construct()
    {
        $formatter = new LineFormatter(
            null, // Format of message in log, default [%datetime%] %channel%.%level_name%: %message% %context% %extra%\n
            null, // Datetime format
            true, // allowInlineLineBreaks option, default false
            true  // discard empty Square brackets in the end, default false
        );

        $log = new Monolog(PFW_PLUGIN_NAME);

        $filename = PFW_PLUGIN_DIR . 'storage/' . PFW_SLUG . '-logs/' . current_time('d-m-Y', '') . '.log';

        $handler = new StreamHandler($filename);
        $handler->setFormatter($formatter);
        $log->pushHandler($handler);

        $this->log = $log;
    }

    /**
     * @param $message
     * @param array $context
     */
    public function info($message, $context = [])
    {
        $this->log->info($message, $context);
    }

    /**
     * @param $message
     * @param array $context
     */
    public function warning($message, $context = [])
    {
        $this->log->warning($message, $context);
    }

    /**
     * @param $message
     * @param array $context
     */
    public function error($message, $context = [])
    {
        $this->log->error($message, $context);
    }
}