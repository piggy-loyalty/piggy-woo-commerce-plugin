<?php

namespace PFW\Piggy;

use WP_Error;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class Error
 * @package PFW\Piggy
 */
class Error
{
    const INVALID_CREDENTIALS = 1001;
    const INVALID_INPUT = 1002;
    const INVALID_TOKEN = 2001;
    const HEALTH_CHECK_FAILED = 2002;
    const REQUIRES_MIN_PHP_VERSION = 3000;
    const REQUIRES_MIN_WC_VERSION = 3001;
    const DATABASE_INSERTION_FAILED = 4001;
    const DATABASE_TABLE_CREATION_FAILED = 4002;
    const WC_NOT_ACTIVATED = 3002;
    const OAUTH_TOKEN_INVALID = 6001;
    const OAUTH_TOKEN_NOT_FOUND = 6002;
    const SHOP_NOT_FOUND = 7001;
    const SHOP_ID_NOT_FOUND = 7002;
    const USER_NOT_FOUND = 8001;
    const USER_NOT_LINKED = 8002;
    const SHOP_NOT_LINKED_TO_LOYALTY_PROGRAM = 8102;
    const CAN_NOT_READ_LOGS = 9001;

    /**
     * @var array
     */
    private static $messages = [

        self::OAUTH_TOKEN_INVALID => 'OAuth token is not valid',
        self::SHOP_NOT_FOUND => 'Shop not found',
        self::USER_NOT_FOUND => 'User not found.',
        self::USER_NOT_LINKED => 'User has not linked piggy account yet.',
        self::CAN_NOT_READ_LOGS => 'Failed to read logging files.',
        self::INVALID_CREDENTIALS => 'Invalid key or secret.',
        self::INVALID_TOKEN => 'Invalid token.',
        self::HEALTH_CHECK_FAILED => 'Health check failed.',
        self::INVALID_INPUT => 'Invalid input.',
        self::SHOP_ID_NOT_FOUND => 'Shop ID not found.',
        self::OAUTH_TOKEN_NOT_FOUND => 'OAuthToken not found.',
        self::REQUIRES_MIN_PHP_VERSION => 'Piggy for Woocommerce requires at least PHP ' . PFW_MIN_PHP_VERSION,
        self::REQUIRES_MIN_WC_VERSION => 'Piggy for Woocommerce requires at least Woocommerce version ' . PFW_MIN_WOOCOMMERCE_VERSION,
        self::WC_NOT_ACTIVATED => 'The woocommerce plugin needs to be activated before Piggy for Woocommerce can be used.',
        self::SHOP_NOT_LINKED_TO_LOYALTY_PROGRAM => 'Shop not linked to a Loyalty Program',
        self::DATABASE_INSERTION_FAILED => 'Could not insert row into the database',
        self::DATABASE_TABLE_CREATION_FAILED => 'Could not create table'
    ];

    /**
     * @param $code
     * @return mixed|string
     */
    public static function getMessage($code)
    {
        // Error message returnen
        if (array_key_exists($code, self::$messages) && self::$messages[$code]) {
            return self::$messages[$code];
        }

        // Return undefined, met check of die bestaat
        if (array_key_exists(0, self::$messages)) {
            return self::$messages[0];
        }

        return false;
    }

    /**
     * @param $error
     * @param $message
     * @return WP_Error
     */
    public function make($error, $message)
    {
        $nestingLevel = 4;
        $backTrace = debug_backtrace(false);

        $stackTrace = "";

        for ($i = 0; $i <= $nestingLevel; $i++) {

            $line = $backTrace[$i]["line"];
            $function = $backTrace[$i]["function"];
            $class = $backTrace[$i]["class"];

            $stackTrace .= "Line: {$line} | Class: {$class} | Function: {$function}" . PHP_EOL;
        }

        $errorMessage = self::getMessage($error);

        if (!$errorMessage) {
            $errorMessage = $message;
        }

        $logger = new Logger();
        $logger->error("Error: " . $errorMessage . PHP_EOL . $stackTrace);

        $wpError = new WP_Error($error, $errorMessage, $stackTrace);

        return $wpError;
    }
}