<?php

namespace PFW\Piggy;
defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class Session
 * @package PFW\Piggy
 */
class Session
{
    /**
     * @param $key
     * @param $value
     */
    public function put($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function get($key)
    {
        return $_SESSION[$key];
    }

    /**
     * @param $message
     */
    public function success($message)
    {
        $this->put("toasts", [
            "success" => $message,
        ]);
    }

    /**
     * @param $message
     */
    public function error($message)
    {
        $this->put("toasts", ["error" => $message]);
    }

    /**
     * @param $errors
     */
    public function withErrors($errors)
    {
        foreach($errors as $error ) {
            $this->error($error);
        }
    }
}