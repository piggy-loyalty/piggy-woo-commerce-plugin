<?php

namespace PFW\Piggy\Services;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

use Exception;
use PFW\Piggy\Error;
use PFW\Piggy\Logger;
use PFW\Piggy\Models\Interfaces\OAuthTokenInterface;
use PFW\Piggy\Models\OAuthToken;
use PFW\Piggy\Repositories\OAuthTokenRepository;
use PFW\Container\Piggy\Api\Exceptions\PiggyApiException;
use PFW\Container\Piggy\Api\Model\Customer;
use PFW\Container\Piggy\Api\PiggyApi;

/**
 * Class OAuthService
 * @package PFW\Piggy\Services
 */
class OAuthService extends PiggyApiService
{
    /**
     * @var OAuthTokenRepository
     */
    private $oauthTokenRepository;

    /**
     * OAuthService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->oauthTokenRepository = new OAuthTokenRepository();
    }

    /**
     * @param string $code
     * @return mixed
     * @throws Exception
     */
    public function authenticate(string $code)
    {
        $userId = get_current_user_id();
        $OAuthToken = $this->oauthTokenRepository->findOneBy([
            "user_id" => $userId
        ]);
        if($OAuthToken instanceof OAuthToken) {
            // Remove old OAuthToken of customer.
            $this->oauthTokenRepository->remove([
                "user_id" => $userId
            ]);
        }

        try {
            $this->logger->info("Set redirect uri for retrieving oauth code: [redirect uri] " .  $this->getRedirectUri());
            $this->piggyApi->customer->setRedirectUri($this->getRedirectUri());
            $this->logger->info("Request oauth token with piggy api sdk: [code] " . $code);
            $token = $this->piggyApi->customer->requestAuthenticationToken($code);
            $this->logger->info("Retrieved oauth token is: [token] " . print_r($token, true));
        } catch (PiggyApiException $exception) {
            $this->logger->info("Requesting oauth token failed critical, exception is thrown: [Exception] " . $exception->getMessage());
            throw new Exception($exception->getMessage(), $exception->getCode(), $exception);
        }

        if (!isset($token)) {
            return (new Error())->make(Error::OAUTH_TOKEN_NOT_FOUND, Error::getMessage(Error::OAUTH_TOKEN_NOT_FOUND));
        }

        $this->logger->info("Save created token in database with user id: [userId] " . $userId);

        $saved = $this->oauthTokenRepository->save([
            "user_id" => $userId,
            "access_token" => $token->getAccessToken(),
            "refresh_token" => $token->getRefreshToken(),
            "expires_in" => $token->getExpiresIn()
        ]);
        
        return $saved ? $token : null;
    }

    /**
     * @return Customer|void
     */
    public function profile($userId)
    {
        if(!$this->isUserLinked($userId)) {
            $this->logger->error(Error::getMessage(Error::USER_NOT_LINKED) . ', ' . Error::USER_NOT_LINKED);
            return null;
        }

        try {
            return $this->piggyApi->customer->profile();
        } catch(Exception $exception) {
            $this->logger->error($exception->getCode() . ', ' . $exception->getMessage());
            return null;
        }
    }

    /**
     * @param $userId
     * @return bool
     */
    public function isUserLinked($userId) {
        $oauthToken = $this->oauthTokenRepository->findOneBy(["user_id" => $userId]);

        if(!$oauthToken instanceof OAuthTokenInterface) {
            return false;
        }

        $this->piggyApi->setAccessToken($oauthToken->getAccessToken());

        return true;
    }

    /**
     * @return string
     */
    public function generateAuthenticationUri()
    {
        return $this->piggyApi->customer->generateAuthenticationUrl($this->getRedirectUri());
    }

    /**
     * @return string
     */
    public function getRedirectUri()
    {
        if(get_post(get_option('woocommerce_myaccount_page_id'))) {
            if(isset(get_post(get_option('woocommerce_myaccount_page_id'))->post_name)) {
                return get_permalink( get_post(get_option('woocommerce_myaccount_page_id') )->ID) . "pfw-oauth-callback";
            }
        }

        return "";
    }

    /**
     * @param PiggyApi $piggyApi
     */
    public function setPiggyApi($piggyApi)
    {
        $this->piggyApi = $piggyApi;
    }

    /**
     * @param OAuthTokenRepository $oauthTokenRepository
     */
    public function setOauthTokenRepository($oauthTokenRepository)
    {
        $this->oauthTokenRepository = $oauthTokenRepository;
    }

    /**
     * @param Logger $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }
}