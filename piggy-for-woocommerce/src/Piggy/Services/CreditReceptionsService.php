<?php

namespace PFW\Piggy\Services;

use PFW\Piggy\Logger;
use PFW\Container\Piggy\Api\Exceptions\PiggyApiException;
use PFW\Container\Piggy\Api\PiggyApi;

/**
 * Class CreditReceptionsService
 * @package PFW\Piggy\Services
 */
class CreditReceptionsService extends PiggyApiService
{
    /**
     * @param $email
     * @param $purchaseAmount
     */
    public function handle($email, $purchaseAmount)
    {
        $shopId = get_option(PFW_SLUG . '_shop_id');

        try {
            $creditReception = $this->piggyApi->client->creditReceptions->create($email, $purchaseAmount, $shopId);
            $this->log('info', 'Added ' . $creditReception->getCredits()  . ' credits to ' . $email);
        } catch (PiggyApiException $exception) {
            $this->log('error', $exception->getCode() . ', ' . $exception->getMessage());
        }
    }

    /**
     * @param $amount
     * @param $taxAmount
     * @param $shippingCost
     * @return float
     */
    public function calculateCredits($amount, $taxAmount, $shippingCost)
    {

        $includeTaxes = get_option(PFW_SLUG . "_credits_include_taxes");
        $includeShippingCost = get_option(PFW_SLUG . "_credits_include_shipping_cost");

        $amountOfCredits = $amount;

        if($includeTaxes) {
            $amountOfCredits = $amount + $taxAmount;
            $this->logger->info("Credits calculation includes taxes, amount: " . $amountOfCredits );
        }

        if($includeShippingCost) {
            $amountOfCredits = $amount + $shippingCost;
            $this->logger->info("Credits calculation includes shipping cost, amount: " . $amountOfCredits);
        }

        if($includeTaxes && $includeShippingCost) {
            $amountOfCredits = $amount + $taxAmount + $shippingCost;
            $this->logger->info("Credits calculation includes shipping cost and taxes, amount: " . $amountOfCredits);
        }

        return ceil($amountOfCredits);
    }

    /**
     * @param $type
     * @param $message
     */
    public function log($type, $message)
    {
        $this->logger->$type($message);
    }

    /**
     * @param PiggyApi $piggyApi
     */
    public function setPiggyApi($piggyApi)
    {
        $this->piggyApi = $piggyApi;
    }

    /**
     * @param Logger $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }
}