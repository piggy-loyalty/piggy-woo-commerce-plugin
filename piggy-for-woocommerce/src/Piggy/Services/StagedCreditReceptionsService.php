<?php

namespace PFW\Piggy\Services;

use PFW\Piggy\Logger;
use PFW\Container\Piggy\Api\Exceptions\PiggyApiException;
use PFW\Container\Piggy\Api\Model\StagedCreditReception;
use PFW\Container\Piggy\Api\PiggyApi;

/**
 * Class StagedCreditReceptionsService
 * @package PFW\Piggy\Services
 */
class StagedCreditReceptionsService extends PiggyApiService
{
    /**
     * @param $email
     * @param $purchaseAmount
     * @param null $credits
     * @return \Exception|PiggyApiException|StagedCreditReception
     */
    public function handle($email, $purchaseAmount, $credits = null)
    {
        $shopId = get_option(PFW_SLUG . '_shop_id');
        $language = get_option(PFW_SLUG . '_language');

        try {
            $staged = $this->piggyApi->client->stagedCreditReceptions->create($shopId, $purchaseAmount, $credits);
            $this->log('info', 'Created a staged credit reception with hash ' . $staged->getHash());
            $this->log('info', 'Going to send a staged credit reception to ' . $email);
            $this->piggyApi->client->stagedCreditReceptions->send($staged->getHash(), $email, $language);
            $this->log('info', 'Succesfully send a staged credit reception to ' . $email);
            return $staged;
        } catch (PiggyApiException $exception) {
            $this->log('error', $exception->getCode() . ', ' . $exception->getMessage());
            return $exception;
        }
    }

    /**
     * @param $type
     * @param $message
     */
    public function log($type, $message)
    {
        $this->logger->$type($message);
    }

    /**
     * @param PiggyApi $piggyApi
     */
    public function setPiggyApi($piggyApi)
    {
        $this->piggyApi = $piggyApi;
    }

    /**
     * @param Logger $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

}