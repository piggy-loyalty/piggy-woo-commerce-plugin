<?php

namespace PFW\Piggy\Services;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

use PFW\Piggy\Logger;
use PFW\Container\Piggy\Api\PiggyApi;

//include_once WP_PLUGIN_DIR .'/woocommerce/woocommerce.php';

class PiggyApiService
{

    /**
     * @var PiggyApi
     */
    protected $piggyApi;

    /**
     * @var Logger
     */
    protected $logger;

    public function __construct()
    {
        global $wp_version;

        if ( function_exists( 'is_woocommerce_activated' ) ) {
            $wc_version = WC()->version;
        } else {
            $wc_version = 0;
        }

        $this->logger = new Logger();

        $this->piggyApi = new PiggyApi(get_option(PFW_SLUG . "_client_id"), get_option(PFW_SLUG . "_client_secret"), PFW_ENV);

        $this->piggyApi->addHeader("PHP-Version", phpversion());
        $this->piggyApi->addHeader("WordPress-Version", $wp_version);
        $this->piggyApi->addHeader("WooCommerce-Version", $wc_version);
        $this->piggyApi->addHeader("Piggy-For-WooCommerce-Version", PFW_VERSION);

        $this->piggyApi->setClientToken(get_option(PFW_SLUG . "_client_token"));
    }

}