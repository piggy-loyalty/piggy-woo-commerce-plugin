<?php

namespace PFW\Piggy\Controllers;

use PFW\Piggy\Error;
use PFW\Piggy\Mappers\OAuthTokenMapper;
use PFW\Piggy\Models\Interfaces\OAuthTokenInterface;
use PFW\Piggy\Repositories\OAuthTokenRepository;
use PFW\Piggy\Repositories\UserRepository;
use PFW\Piggy\Services\OAuthService;
use WP_Error;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class AuthController
 * @package PFW\Piggy\Controllers
 */
class AuthController extends BaseController
{
    /**
     * @var string
     */
    protected $table_name;
    /**
     * @var UserRepository
     */
    protected $userRepository;
    /**
     * @var OAuthTokenRepository
     */
    protected $OAuthTokenRepository;

    /**
     * @var OAuthTokenMapper
     */
    protected $OAuthTokenMapper;

    /**
     * @var OAuthService
     */
    protected $OAuthService;

    /**
     * AuthController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->table_name = 'wp_' . PFW_SLUG . '_oauth_tokens';
        $this->userRepository = new UserRepository();
        $this->OAuthTokenRepository = new OAuthTokenRepository();
        $this->OAuthTokenMapper = new OAuthTokenMapper();
        $this->OAuthService = new OAuthService();
    }

    public function getAuthenticationUri()
    {
        $authUri = $this->OAuthService->generateAuthenticationUri();

        return $this->response($authUri);
    }

    /**
     * @return WP_Error|\WP_REST_Response
     */
    public function delete()
    {
        $OAuthToken = $this->OAuthTokenRepository->findOneBy([
            "user_id" => get_current_user_id()
        ]);
        if (!$OAuthToken instanceof OAuthTokenInterface) {
            return $this->errorResponse(Error::getMessage(Error::USER_NOT_LINKED), Error::USER_NOT_LINKED);
        }

        $this->OAuthTokenRepository->remove(["id" => $OAuthToken->getId()]);

        return $this->response([]);
    }

    /**
     * @param UserRepository $userRepository
     */
    public function setUserRepository($userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param OAuthTokenRepository $OAuthTokenRepository
     */
    public function setOAuthTokenRepository($OAuthTokenRepository)
    {
        $this->OAuthTokenRepository = $OAuthTokenRepository;
    }

    /**
     * @param OAuthService $OAuthService
     */
    public function setOAuthService($OAuthService)
    {
        $this->OAuthService = $OAuthService;
    }
}