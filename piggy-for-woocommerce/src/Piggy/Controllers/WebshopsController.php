<?php

namespace PFW\Piggy\Controllers;

use PFW\Piggy\Error;
use PFW\Container\Piggy\Api\Exceptions\PiggyApiException;
use PFW\Container\Piggy\Api\Mappers\WebshopMapper;
use PFW\Container\Piggy\Api\Mappers\WebshopsMapper;
use WP_Error;
use WP_REST_Response;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class WebshopsController
 * @package PFW\Piggy\Controllers
 */
class WebshopsController extends BaseController
{
    /**
     * @return WP_Error|WP_REST_Response
     */
    public function index()
    {
        try {
            $webshops = $this->piggyApi->client->webshops->index();
        } catch (PiggyApiException $exception) {
            return $this->errorResponse($exception->getMessage(), $exception->getCode());
        }

        $webshopsMapper = new WebshopsMapper();

        return $this->response($webshopsMapper->mapToResponse($webshops));
    }

    /**
     * @return WP_Error|WP_REST_Response
     */
    public function show()
    {
        $shopId = get_option(PFW_SLUG . '_shop_id');

        if (!$shopId) {
            return $this->errorResponse(Error::getMessage(Error::SHOP_ID_NOT_FOUND), Error::SHOP_ID_NOT_FOUND);
        }

        try {
            $webshop = $this->piggyApi->client->webshops->show($shopId);
        } catch (PiggyApiException $exception) {
            return $this->errorResponse($exception->getMessage(), $exception->getCode());
        }

        return $this->response([
            "id" => $webshop->getId(),
            "name" => $webshop->getName(),
        ]);
    }
}