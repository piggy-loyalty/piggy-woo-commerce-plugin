<?php

namespace PFW\Piggy\Controllers;

use Exception;
use PFW\Piggy\Error;
use PFW\Piggy\Repositories\OAuthTokenRepository;
use PFW\Piggy\Services\CreditReceptionsService;
use PFW\Container\Piggy\Api\Exceptions\PiggyApiException;
use PFW\Container\Piggy\Api\Mappers\CustomerMapper;
use WC_Session_Handler;
use WP_REST_Response;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class CheckoutController
 * @package PFW\Piggy\Controllers
 */
class CheckoutController extends BaseController
{
    /**
     * @var OAuthTokenRepository
     */
    protected $OAuthTokenRepository;

    /**
     * CustomerController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->OAuthTokenRepository = new OAuthTokenRepository();
    }

    /**
     * @return WP_REST_Response
     * @throws PiggyApiException
     */
    public function show()
    {
        if (!$this->verifyCustomer(get_current_user_id())) {
            return $this->errorResponse(Error::getMessage(Error::USER_NOT_LINKED), Error::USER_NOT_LINKED);
        }

        $this->logger->info('customer is verified, information for checkout');

        try {
            $customer = $this->piggyApi->customer->profile();
        } catch(Exception $exception) {
            return $this->errorResponse($exception->getMessage(), $exception->getCode());
        }

        $response["customer"] = (new CustomerMapper())->mapToResponse($customer);

        try {
            $response["credit_balance"] = $this->piggyApi->customer->creditBalances->show(get_option(PFW_SLUG . '_shop_id'));
        } catch(Exception $exception) {
            return $this->errorResponse($exception->getMessage(), $exception->getCode());
        }

        $this->logger->info('Going to get cart information for checkout page');

        // Get an instance of the WC_Session_Handler Object
        $session_handler = new WC_Session_Handler();
        // Get the user session from user ID:
        $session = $session_handler->get_session(get_current_user_id());

        $amount = (maybe_unserialize($session["cart_totals"])["subtotal"]);
        $taxAmount = (maybe_unserialize($session["cart_totals"])["subtotal_tax"]);
        $shippingCost = (maybe_unserialize($session["cart_totals"])["shipping_total"]);

        $response["purchase_amount"]  = (new CreditReceptionsService())->calculateCredits($amount, $taxAmount, $shippingCost);

        $this->logger->info('Got all the information, going to sent it to front end!');

        return $this->response($response);
    }
}