<?php

namespace PFW\Piggy\Controllers;

use DateTime;
use Exception;
use PFW\Piggy\Error;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use WP_REST_Response;
use ZipArchive;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class LogController
 * @package PFW\Piggy\Controllers
 */
class LogController extends BaseController
{
    /**
     * @var string
     */
    protected $path;

    /**
     * LogController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->path = PFW_PLUGIN_DIR . 'storage/' . PFW_SLUG . '-logs';
    }


    /**
     * @param int $page
     * @param int $limit
     * @return WP_REST_Response
     */
    public function index($request)
    {
        $page = $request['page'] ? $request['page'] : 1;
        $limit = $request['limit'] ? $request['limit'] : 10;

        $files = $this->getLogsSortedByDate();
        $count = count($files);
        $files = $this->paginate($files, $page, $limit);
        $files = $this->getFilesContent($files);

        $response['count'] = $count;
        $response['files'] = $files;

        return $this->response($response);
    }

    public function download()
    {
        $zip = new ZipArchive();
        $pathToFiles = PFW_PLUGIN_DIR . 'storage/' . PFW_SLUG . '-logs/';
        $pathToStorage = PFW_PLUGIN_DIR . 'storage/downloads/';
        if (!file_exists($pathToStorage)) {
            mkdir($pathToStorage, 0777, true);
        }
        $zip->open($pathToStorage . 'logs.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);

        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($pathToFiles),
            RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $name => $file) {
            // Skip directories (they would be added automatically)
            if (!$file->isDir())
            {
                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($pathToFiles) + 1);

                // Add current file to archive
                $zip->addFile($filePath, $relativePath);
            }
        }

        $zip->close();

        $downloadUrl = site_url() . '/wp-content/plugins/' . PFW_PLUGIN_NAME . '/storage/downloads/logs.zip';

        return $this->response($downloadUrl);
    }

    /**
     * @return array|WP_REST_Response
     */
    private function getLogsSortedByDate()
    {
        $files = scandir($this->path);
        $files = array_slice($files, 2);
        $selectedFiles = [];

        foreach ($files as $file) {
            $file = explode(".", $file);
            $fileName = $file[0];
            try {
                $dateTime = new DateTime($fileName);
            } catch (Exception $exception) {
                return $this->errorResponse($exception->getMessage(), $exception->getCode());
            }
            $selectedFiles[] = $fileName;
        }

        $ord = [];
        foreach ($selectedFiles as $log => $value){
            $ord[] = strtotime($value);
        }
        array_multisort($ord, SORT_DESC, $selectedFiles);

        return $selectedFiles;
    }

    /**
     * @param $files
     * @param $page
     * @param $limit
     * @return array
     */
    private function paginate($files, $page, $limit)
    {
        return array_slice($files, ($page * 10) - 10, $limit);
    }

    /**
     * @param $files
     * @return array|WP_REST_Response
     */
    private function getFilesContent($files)
    {
        $logs = [];
        foreach ($files as $file) {
            $log = [];
            $content = [];

            $log["name"] = $file;

            $filePath = $this->path . '/' . $file . '.log';

            if (file_exists($filePath)) {
                $handle = fopen($filePath, 'r');

                if ($handle) {
                    while (($line = fgets($handle)) !== false) {
                        array_push($content, $line);
                    }
                    fclose($handle);
                } else {
                    return $this->errorResponse(Error::getMessage(Error::CAN_NOT_READ_LOGS), Error::CAN_NOT_READ_LOGS);
                }
            }

            $log["content"] = $content;
            $logs[] = $log;
        }
        return $logs;
    }
}