<?php

namespace PFW\Piggy\Controllers;

use Exception;
use PFW\Piggy\Error;
use PFW\Piggy\Repositories\OAuthTokenRepository;
use PFW\Container\Piggy\Api\Exceptions\PiggyApiException;
use PFW\Container\Piggy\Api\Mappers\CustomerMapper;
use WP_REST_Response;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class CustomerController
 * @package PFW\Piggy\Controllers
 */
class CustomerController extends BaseController
{
    /**
     * @var OAuthTokenRepository
     */
    protected $OAuthTokenRepository;

    /**
     * CustomerController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->OAuthTokenRepository = new OAuthTokenRepository();
    }

    /**
     * @return WP_REST_Response
     * @throws PiggyApiException
     */
    public function show()
    {
        if (!$this->verifyCustomer(get_current_user_id())) {
            return $this->errorResponse(Error::getMessage(Error::USER_NOT_LINKED), Error::USER_NOT_LINKED);
        }

        $this->logger->info('customer is verified, lets get profile information...');

        try {
            $customer = $this->piggyApi->customer->profile();
        } catch(Exception $exception) {
            return $this->errorResponse($exception->getMessage(), $exception->getCode());
        }

        $response["customer"] = (new CustomerMapper())->mapToResponse($customer);

        try {
            $response["credit_balance"] = $this->piggyApi->customer->creditBalances->show(get_option(PFW_SLUG . '_shop_id'));
        } catch(Exception $exception) {
            $this->logger->error("code: " . $exception->getCode() . ", message: " . $exception->getMessage());
            return $this->response($response);
        }

        return $this->response($response);
    }
}