<?php

namespace PFW\Piggy\Controllers;

use PFW\Piggy\Error;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class CheckboxSettingsController
 * @package PFW\Piggy\Controllers
 */
class CheckboxSettingsController extends BaseController
{
    public function index()
    {
        $stagedCheckboxText = get_option(PFW_SLUG . "_staged_checkbox_text") ? get_option(PFW_SLUG . "_staged_checkbox_text") : "Ja, ik wil graag punten sparen.";
        $stagedCheckboxUrl = get_option(PFW_SLUG . "_staged_checkbox_url");
        $stagedCheckboxUrlText = get_option(PFW_SLUG . "_staged_checkbox_url_text");

        return $this->response([
            "staged_checkbox_text" => $stagedCheckboxText,
            "staged_checkbox_url" => $stagedCheckboxUrl,
            "staged_checkbox_url_text" => $stagedCheckboxUrlText
        ]);
    }

    public function update($request)
    {
        $stagedCheckboxText = $request['staged_checkbox_text'];
        $stagedCheckboxUrl = $request['staged_checkbox_url'];
        $stagedCheckboxUrlText = $request['staged_checkbox_url_text'];

        // TODO: Create validation function for this.
        if (!isset($stagedCheckboxText) || !isset($stagedCheckboxUrl) || !isset($stagedCheckboxUrlText))  {
            return $this->errorResponse(Error::getMessage(Error::INVALID_INPUT), Error::INVALID_INPUT);
        }

        update_option(PFW_SLUG . "_staged_checkbox_text", $stagedCheckboxText);
        update_option(PFW_SLUG . "_staged_checkbox_url", $stagedCheckboxUrl);
        update_option(PFW_SLUG . "_staged_checkbox_url_text", $stagedCheckboxUrlText);

        return $this->response([
            "staged_checkbox_text" => $stagedCheckboxText,
            "staged_checkbox_url" => $stagedCheckboxUrl,
            "staged_checkbox_url_text" => $stagedCheckboxUrlText
        ]);
    }
}