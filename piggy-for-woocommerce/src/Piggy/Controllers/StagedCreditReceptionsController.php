<?php

namespace PFW\Piggy\Controllers;

use Exception;
use PFW\Piggy\Error;
use PFW\Piggy\Services\StagedCreditReceptionsService;
use PFW\Container\Piggy\Api\Exceptions\PiggyApiException;
use WP_Error;
use WP_REST_Response;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class StagedCreditReceptionsController
 * @package PFW\Piggy\Controllers
 */
class StagedCreditReceptionsController extends BaseController
{
    protected $stagedCreditReceptionService;

    /**
     * StagedCreditReceptionsController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->stagedCreditReceptionService = new StagedCreditReceptionsService();
    }

        /**
     * @param $request
     * @return WP_Error|WP_REST_Response
     */
    public function send($request)
    {
        $email = $request['email'];
        $purchaseAmount = $request['purchase_amount'];
        $credits = null;

        if (!isset($email)) {
            return $this->errorResponse(Error::getMessage(Error::INVALID_INPUT), Error::INVALID_INPUT);
        }

        if (!isset($purchaseAmount) || $purchaseAmount < 1) {
            return $this->errorResponse(Error::getMessage(Error::INVALID_INPUT), Error::INVALID_INPUT);
        }

        if(isset($request['credits'])) {
            $credits = $request['credits'] ? $request['credits'] : null;
        }

        $staged = $this->stagedCreditReceptionService->handle($email, $purchaseAmount, $credits);

        if($staged instanceof PiggyApiException) {
            return $this->errorResponse($staged->getMessage(), $staged->getCode());
        }

        return $this->response( [
            "email" => $email,
            "staged_credit_reception" => [
                "hash" => $staged->getHash(),
                "credits" => $staged->getCredits()
            ]
        ]);
    }

    /**
     * @return StagedCreditReceptionsService
     */
    public function getStagedCreditReceptionService()
    {
        return $this->stagedCreditReceptionService;
    }

    /**
     * @param StagedCreditReceptionsService $stagedCreditReceptionService
     */
    public function setStagedCreditReceptionService($stagedCreditReceptionService)
    {
        $this->stagedCreditReceptionService = $stagedCreditReceptionService;
    }

}