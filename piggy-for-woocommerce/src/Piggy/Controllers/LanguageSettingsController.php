<?php

namespace PFW\Piggy\Controllers;

use PFW\Piggy\Error;
use WP_Rewrite;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class LanguageSettingsController
 * @package PFW\Piggy\Controllers
 */
class LanguageSettingsController extends BaseController
{
    public function index()
    {
        $language = get_option(PFW_SLUG . "_language");
        $languageOptions = [
            ["id" => "nl", "label" => "dutch"],
            ["id" => "en", "label" => "english"],
            ["id" => "de", "label" => "german"],
        ];

        return $this->response([
            "language" => $language,
            "language_options" => $languageOptions,
        ]);
    }

    public function update($request)
    {
        $language = $request['language'];

        // TODO: Create validation function for this.
        if (!isset($language))  {
            return $this->errorResponse(Error::getMessage(Error::INVALID_INPUT), Error::INVALID_INPUT);
        }

        update_option(PFW_SLUG . "_language", $language);

        return $this->response([
            "language" => $language
        ]);
    }
}