<?php

namespace PFW\Piggy\Controllers;

use Exception;
use PFW\Piggy\Error;
use PFW\Container\Piggy\Api\Model\LoyaltyProgram;
use PFW\Container\Piggy\Api\Model\Webshop;
use WP_Error;
use WP_REST_Response;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class SetupController
 * @package PFW\Piggy\Controllers
 */
class SetupController extends BaseController
{
    /**
     * @param $request
     * @return WP_Error|WP_REST_Response
     */
    public function authenticate($request)
    {
        $clientId = $request['key'];
        $clientSecret = $request['secret'];

        if (!isset($clientId) && !isset($clientSecret)) {
            return $this->errorResponse(Error::getMessage(Error::INVALID_INPUT), Error::INVALID_INPUT);
        }

        $this->piggyApi->setClientId($clientId);
        $this->piggyApi->setClientSecret($clientSecret);

        try {
            $token = $this->piggyApi->client->requestClientToken();
        } catch (Exception $exception) {
            return $this->errorResponse($exception->getMessage(), $exception->getCode());
        }

        if (!$token) {
            return $this->errorResponse(Error::getMessage(Error::INVALID_TOKEN), Error::INVALID_TOKEN);
        }

        update_option(PFW_SLUG . '_client_id', $clientId);
        update_option(PFW_SLUG . '_client_secret', $clientSecret);
        update_option(PFW_SLUG . '_client_token', $token['access_token']);

        return $this->response(["token" => $token['access_token']]);
    }

    /**
     * @return WP_Error|WP_REST_Response
     */
    public function ping()
    {
        try {
            $healthCheck = (bool)$this->piggyApi->client->healthCheck();
            if (!$healthCheck) {
                return $this->errorResponse(Error::getMessage(Error::HEALTH_CHECK_FAILED), Error::HEALTH_CHECK_FAILED);
            }
        } catch (Exception $exception) {
            return $this->errorResponse($exception->getMessage(), $exception->getCode());
        }

        return $this->response($healthCheck);
    }

    /**
     * @param $request
     * @return WP_Error|WP_REST_Response
     */
    public function update($request)
    {
        $shopId = $request['shop_id'];

        if (!isset($shopId)) {
            return $this->errorResponse(Error::getMessage(Error::SHOP_ID_NOT_FOUND), Error::SHOP_ID_NOT_FOUND);
        }

        try {
            $shop = $this->piggyApi->client->webshops->show($shopId);

            if (!$shop instanceof Webshop) {
                return $this->errorResponse(Error::getMessage(Error::SHOP_NOT_FOUND), Error::SHOP_NOT_FOUND);
            }

            if(!$shop->getLoyaltyProgram() instanceof LoyaltyProgram)
            {
                return $this->errorResponse(Error::getMessage(Error::SHOP_NOT_LINKED_TO_LOYALTY_PROGRAM), Error::SHOP_NOT_LINKED_TO_LOYALTY_PROGRAM);
            }


            update_option(PFW_SLUG . '_shop_id', $shop->getId());
        } catch (Exception $exception) {
            return $this->errorResponse($exception->getMessage(), $exception->getCode());
        }

        return $this->response(["redirect_url" => admin_url("/admin.php?page=pfw-admin")]);
    }

    /**
     * @return WP_REST_Response
     */
    public function delete()
    {
        update_option(PFW_SLUG . '_client_token', "");
        update_option(PFW_SLUG . '_shop_id', "");
        $this->logger->info("Deleted configuration");

        return $this->response(["redirect_url" => admin_url("/admin.php?page=pfw-setup")]);
    }
}