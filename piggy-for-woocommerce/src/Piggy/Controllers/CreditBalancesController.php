<?php

namespace PFW\Piggy\Controllers;

use Exception;
use PFW\Piggy\Error;
use WP_Error;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class CreditBalancesController
 * @package PFW\Piggy\Controllers
 */
class CreditBalancesController extends BaseController
{
    /**
     * @return WP_Error|\WP_REST_Response
     */
    public function show()
    {
        if (!$this->verifyCustomer(get_current_user_id())) {
            return $this->errorResponse(Error::getMessage(Error::USER_NOT_LINKED), Error::USER_NOT_LINKED);
        }

        try {
            $response["credit_balance"] = $this->piggyApi->customer->creditBalances->show(get_option(PFW_SLUG . '_shop_id'));
        } catch (Exception $exception) {
            return $this->errorResponse($exception->getMessage(), $exception->getCode());
        }

        return $this->response($response);
    }
}