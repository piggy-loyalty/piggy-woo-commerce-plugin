<?php

namespace PFW\Piggy\Controllers;

use PFW\Piggy\Error;
use WP_Rewrite;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class CreditsSettingsController
 * @package PFW\Piggy\Controllers
 */
class CreditsSettingsController extends BaseController
{
    public function index()
    {
        $includeTaxes = get_option(PFW_SLUG . "_credits_include_taxes");
        $includeShippingCost = get_option(PFW_SLUG . "_credits_include_shipping_cost");

        return $this->response([
            "credits_include_taxes" => $includeTaxes,
            "credits_include_shipping_cost" => $includeShippingCost,
        ]);
    }

    public function update($request)
    {
        $includeTaxes = $request['credits_include_taxes'];
        $includeShippingCost = $request['credits_include_shipping_cost'];

        // TODO: Create validation function for this.
        if (!isset($includeTaxes) || !isset($includeShippingCost) )  {
            return $this->errorResponse(Error::getMessage(Error::INVALID_INPUT), Error::INVALID_INPUT);
        }

        update_option(PFW_SLUG . "_credits_include_taxes", $includeTaxes);
        update_option(PFW_SLUG . "_credits_include_shipping_cost", $includeShippingCost);

        return $this->response([
            "credits_include_taxes" => $includeTaxes,
            "credits_include_shipping_cost" => $includeShippingCost,
        ]);
    }
}