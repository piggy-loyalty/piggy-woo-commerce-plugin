<?php

namespace PFW\Piggy\Controllers;

use PFW\Piggy\Error;
use WP_Rewrite;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class FlowSettingsController
 * @package PFW\Piggy\Controllers
 */
class FlowSettingsController extends BaseController
{
    public function index()
    {
        $allowLinking = get_option(PFW_SLUG . "_allow_linking");
        $guestFlow = get_option(PFW_SLUG . "_guest_flow_option");
        $userFlow = get_option(PFW_SLUG . "_user_flow_option");

        return $this->response([
            "guest_flow" => $guestFlow,
            "user_flow" => $userFlow,
            "allow_linking" => $allowLinking,
        ]);
    }

    public function update($request)
    {
        $guestFlow = $request['guest_flow'];
        $userFlow = $request['user_flow'];
        $allowLinking = $request['allow_linking'];

        // TODO: Create validation function for this.
        if (!isset($guestFlow) || !isset($allowLinking) || !isset($userFlow) )  {
            return $this->errorResponse(Error::getMessage(Error::INVALID_INPUT), Error::INVALID_INPUT);
        }

        if($allowLinking) {
            flush_rewrite_rules();
            update_option(PFW_SLUG . "_flushed_rewrite_rules", true);
        }
        update_option(PFW_SLUG . "_allow_linking", $allowLinking);
        update_option(PFW_SLUG . "_guest_flow_option", $guestFlow);
        update_option(PFW_SLUG . "_user_flow_option", $userFlow);

        return $this->response([
            "guest_flow" => $guestFlow,
            "user_flow" => $userFlow,
            "allow_linking" => $allowLinking,
        ]);
    }
}