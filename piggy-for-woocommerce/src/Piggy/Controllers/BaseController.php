<?php

namespace PFW\Piggy\Controllers;

use Exception;
use PFW\Piggy\Error;
use PFW\Piggy\Logger;
use PFW\Piggy\Models\Interfaces\OAuthTokenInterface;
use PFW\Piggy\Repositories\OAuthTokenRepository;
use PFW\Piggy\Session;
use PFW\Container\Piggy\Api\Exceptions\PiggyApiException;
use PFW\Container\Piggy\Api\PiggyApi;
use WP_Error;
use WP_REST_Response;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class BaseController
 * @package PFW\Admin\Controllers
 */
class BaseController
{
    /**
     * @var PiggyApi
     */
    protected $piggyApi;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var int
     */
    protected $shopId;

    /**
     * @var OAuthTokenRepository
     */
    private $oauthTokenRepository;

    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        $this->piggyApi = new PiggyApi(get_option(PFW_SLUG . "_client_id"), get_option(PFW_SLUG . "_client_secret"), PFW_ENV);

        $this->piggyApi->addHeader("PHP-Version", phpversion());

        if(get_option(PFW_SLUG . '_client_token')) {
            $this->piggyApi->setClientToken(get_option(PFW_SLUG . '_client_token'));
        }

        $this->oauthTokenRepository = new OAuthTokenRepository();

        $this->logger = new Logger();
        $this->session = new Session();
    }

    /**
     * @param $type
     * @param $message
     */
    public function log($type, $message)
    {
        $this->logger->$type($message);
    }

    /**
     * @param $data
     * @param int $statusCode
     * @param bool $exception
     * @return WP_REST_Response
     */
    public function response($data, $statusCode = 200, $exception = false)
    {
        if($exception) {
            $data = \PFW\Container\GuzzleHttp\json_decode($data);
            $statusCode = \PFW\Container\GuzzleHttp\json_decode($statusCode);
        }

        return new WP_REST_Response([
            "data" => $data
        ], $statusCode);
    }


    /**
     * @param $errorMessage
     * @param $errorCode
     * @param int $statusCode
     * @return WP_REST_Response
     */
    public function errorResponse($errorMessage, $errorCode, $statusCode = 400)
    {
        $this->log('error', $errorCode . ', ' . $errorMessage);
        return new WP_REST_Response([
            "message" => $errorMessage,
            "code" => $errorCode
        ], $statusCode);
    }

    /**
     * @param $userId
     * @return bool
     */
    public function verifyCustomer($userId)
    {
        $OAuthToken = $this->oauthTokenRepository->findOneBy(["user_id" => $userId]);

        if(!$OAuthToken instanceof OAuthTokenInterface) {
            return false;
        }

        $this->piggyApi->setAccessToken($OAuthToken->getAccessToken());

        return true;
    }

    /**
     * @param Logger $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param OAuthTokenRepository $oauthTokenRepository
     */
    public function setOauthTokenRepository($oauthTokenRepository)
    {
        $this->oauthTokenRepository = $oauthTokenRepository;
    }

    /**
     * @param PiggyApi $piggyApi
     */
    public function setPiggyApi($piggyApi)
    {
        $this->piggyApi = $piggyApi;
    }
}