<?php

namespace PFW\Piggy\Controllers;

use Exception;
use PFW\Piggy\Error;
use WP_Error;
use WP_REST_Response;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class StatusController
 * @package PFW\Piggy\Controllers
 */
class StatusController extends BaseController
{
    /**
     * @return WP_Error|WP_REST_Response
     */
    public function index()
    {
        // Ping to piggy
        try {
            $healthCheck = (bool)$this->piggyApi->client->healthCheck();
            if (!$healthCheck) {
                return $this->errorResponse(Error::getMessage(Error::HEALTH_CHECK_FAILED), Error::HEALTH_CHECK_FAILED);
            }
        } catch (Exception $exception) {
            return $this->errorResponse($exception->getMessage(), $exception->getCode());
        }

        // Check if table exists
        try {
            global $wpdb;
            $tableName = $wpdb->prefix . PFW_SLUG . "_oauth_tokens";
            // Check if table exists
            $query = $wpdb->prepare( 'SHOW TABLES LIKE %s', $wpdb->esc_like( $tableName ) );
            // If not exists
            if ( ! $wpdb->get_var( $query ) == $tableName ) {
                $wpdb->show_errors();
                // Create table
                $query = "CREATE TABLE `$tableName` (
                `id` INT NOT NULL AUTO_INCREMENT,
                `user_id` INT NOT NULL UNIQUE,
                `access_token` VARCHAR(1000) NOT NULL,
                `refresh_token` VARCHAR(1000) NOT NULL,
                `expires_in` INT NOT NULL,
                `updated_at` TIMESTAMP NOT NULL DEFAULT 0,
                `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                KEY `wp_users` (`user_id`) USING BTREE,
                PRIMARY KEY (`id`))";
                require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
                dbDelta( $query );

                // Check again if table exists
                $query = $wpdb->prepare( 'SHOW TABLES LIKE %s', $wpdb->esc_like( $tableName ) );

                // If still not exists
                if ( ! $wpdb->get_var( $query ) == $tableName ) {
                    return $this->errorResponse(Error::getMessage(Error::DATABASE_TABLE_CREATION_FAILED), Error::DATABASE_TABLE_CREATION_FAILED);
                }
            }
        } catch (Exception $exception) {
            return $this->errorResponse($exception->getMessage(), $exception->getCode());
        }

        return $this->response($healthCheck);
    }
}