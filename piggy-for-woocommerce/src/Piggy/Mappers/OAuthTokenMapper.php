<?php

namespace PFW\Piggy\Mappers;

use PFW\Piggy\Models\OAuthToken;
use PFW\Piggy\Repositories\UserRepository;
use stdClass;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class OAuthTokenMapper
 * @package PFW\Piggy\Mappers
 */
class OAuthTokenMapper {

    /**
     * @param $results
     * @return array
     */
    public function mapAll($results) {
        $tokens = [];
        foreach ($results as $item) {
            $tokens[] = $this->map($item);
        }

        return $tokens;
    }

    /**
     * @param $result
     * @return OAuthToken|null
     */
    public function map($result)
    {
        if(!$result) {
            return null;
        }

        $result = get_object_vars($result);

        $token = new OAuthToken();
        $token->setId($result["id"]);

        $user = new UserRepository();
        $token->setUser($user->find($result["user_id"]));
        $token->setAccessToken($result["access_token"]);
        $token->setRefreshToken($result["refresh_token"]);
        $token->setExpiresIn($result["expires_in"]);
        $token->setUpdatedAt($result["updated_at"]);
        $token->setCreatedAt($result["created_at"]);

        return $token;
    }


}