<?php

namespace PFW\Piggy\Mappers;

use PFW\Piggy\Models\User;
use stdClass;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class UserMapper
 * @package PFW\Piggy\Mappers
 */
class UserMapper {

    /**
     * @param $results
     * @return array
     */
    public function mapAll($results) {
        $users = [];
        foreach ($results as $item) {
            $users[] = $this->map($item);
        }

        return $users;
    }

    /**
     * @param $result
     * @return User|null
     */
    public function map($result)
    {
        if(!$result) {
            return null;
        }

        $result = get_object_vars($result);

        $user = new User();

        $user->setId($result["ID"]);
        $user->setEmail($result["user_email"]);
        $user->setSlug($result["user_nicename"]);
        $user->setStatus($result["user_status"]);
        $user->setUsername($result["user_login"]);

        return $user;
    }

}