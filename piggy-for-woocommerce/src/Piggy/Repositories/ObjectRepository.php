<?php

namespace PFW\Piggy\Repositories;

use PFW\Piggy\Logger;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class ObjectRepository
 * @package PFW\Piggy\Repositories
 */
class ObjectRepository implements ObjectRepositoryInterface
{
    /**
     * @var
     */
    protected $entity;

    /**
     * @var \wpdb
     */
    protected $db;

    /**
     * @var
     */
    public $table;

    /**
     * @var
     */
    protected $mapper;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * ObjectRepository constructor.
     */
    public function __construct()
    {
        global $wpdb;
        $this->db = $wpdb;

        $this->logger = new Logger();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        $query = $this->db->prepare("SELECT * FROM {$this->table} WHERE id = %d", $id);
        $result = $this->db->get_row($query);

        return $this->mapper->map($result);
    }

    /**
     * @return mixed
     */
    public function findAll()
    {
        $query = $this->db->prepare("SELECT * FROM {$this->table} LIMIT 99");
        $result = $this->db->get_results($query);

        return $this->mapper->mapAll($result);
    }

    /**
     * @param array $criteria
     * @return mixed
     */
    public function findOneBy(array $criteria)
    {
        $query = "SELECT * FROM `$this->table`";

        $i = 0;
        foreach($criteria as $key => $item) {
            $key = esc_sql($key);
            $item = esc_sql($item);

            if($i === 0) {
                $query .= " WHERE {$key} = '" . $item . "'";
            } else {
                $query .= " AND {$key} = '" . $item . "'";
            }
            $i++;
        }

        $result = $this->db->get_row($query);

        return $this->mapper->map($result);
    }

    /**
     * @param $arrayToSave
     * @return bool
     */
    public function save($arrayToSave)
    {
        return (bool)$this->db->insert(
            $this->table,
            $arrayToSave
        );

//        if(!$saved) {
//            $this->db->show_errors();
//            $error = $this->db->print_error();
//            $this->logger->info("Error in db insertion: " . print_r($error, true));
//            $this->db->hide_errors();
//        }
//        return $saved;
    }

    /**
     * @param $arrayToDelete
     * @return bool
     */
    public function remove($arrayToDelete)
    {
        return (bool)$this->db->delete(
            $this->table,
            $arrayToDelete
        );
    }
}