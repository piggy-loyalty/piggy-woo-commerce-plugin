<?php

namespace PFW\Piggy\Repositories;


use PFW\Piggy\Mappers\OAuthTokenMapper;
use PFW\Piggy\Models\OAuthToken;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class OAuthTokenRepository
 * @package PFW\Piggy\Repositories
 */
class OAuthTokenRepository extends ObjectRepository {

    /**
     * OAuthTokenRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->entity = new OAuthToken();
        $this->table = $this->db->prefix  . "pfw_oauth_tokens";
        $this->mapper = new OAuthTokenMapper();
    }
}