<?php

namespace PFW\Piggy\Repositories;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Interface ObjectRepositoryInterface
 * @package PFW\Piggy\Repositories
 */
interface ObjectRepositoryInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function find($id);

    /**
     * @return mixed
     */
    public function findAll();

    /**
     * @param array $criteria
     * @return mixed
     */
    public function findOneBy(array $criteria);
}