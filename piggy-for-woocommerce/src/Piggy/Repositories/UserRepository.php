<?php

namespace PFW\Piggy\Repositories;

use PFW\Piggy\Mappers\UserMapper;
use PFW\Piggy\Models\User;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class UserRepository
 * @package PFW\Piggy\Repositories
 */
class UserRepository extends ObjectRepository
{
    /**
     * UserRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->entity = new User();
        $this->table = $this->db->prefix . "users";
        $this->mapper = new UserMapper();
    }
}