<?php

namespace PFW\OAuth;

use PFW\Piggy\Controllers\AuthController;
use WP_REST_Controller;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

class Routes extends WP_REST_Controller
{
    protected $namespace;

    protected $base;

    protected $authController;

    /**
     * Routes constructor.
     */
    public function __construct()
    {
        $this->namespace = PFW_SLUG . '/v1';
        $this->authController = new AuthController();
    }

    public function register_routes()
    {
        /**
         * /wp-json/pfw/v1/oauth
         */
        register_rest_route( $this->namespace, '/oauth', [
            [
                'methods'   => \WP_REST_Server::READABLE,
                'callback'  => [$this->authController, 'getAuthenticationUri'],
                'args'      => $this->get_endpoint_args_for_item_schema(true),
                'permission_callback' => [],
            ]
        ]);
        
        /**
         * /wp-json/pfw/v1/oauth/delete
         */
        register_rest_route( $this->namespace, '/oauth/delete', [
            [
                'methods'   => \WP_REST_Server::CREATABLE,
                'callback'  => [$this->authController, 'delete'],
                'args'      => $this->get_endpoint_args_for_item_schema(true),
                'permission_callback' => [],
            ]
        ]);

    }
}