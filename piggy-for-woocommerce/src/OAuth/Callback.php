<?php

namespace PFW\OAuth;

use PFW\Piggy\Logger;
use PFW\Piggy\Services\OAuthService;
use PFW\Container\Piggy\Api\Model\OAuthToken;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class Callback
 * @package PFW\OAuth
 */
class Callback
{
    /**
     * @var string
     */
    private $pageSlug = PFW_SLUG . '-oauth-callback';

    /**
     * @var OAuthService
     */
    private $oauthService;

    /**
     * @var Logger $log
     */
    private $log;

    /**
     * Callback constructor.
     */
    public function __construct()
    {
        $this->add_callback_page();
        $this->oauthService = new OAuthService();
        $this->log = new Logger();
    }

    /**
     *
     */
    public function add_callback_page()
    {
        add_action('init', [$this, 'add_endpoint']);
        add_filter('woocommerce_account_' . $this->pageSlug . '_endpoint', [$this, 'handle_callback']);
    }

    /**
     * @throws \Exception
     */
    public function handle_callback()
    {
        if (isset($_GET['code']) && get_current_user_id()) {
            $code = $_GET['code'];

            $this->log->info("Just recieved code from callback. Now trying to authenticate with this code.");
            $token = $this->oauthService->authenticate($code);

            if($token instanceof OAuthToken) {
                $this->log->info("Authentication succeeded");
                echo "<script type='text/javascript'>localStorage.setItem('piggy-link-event-successful', 'true')</script>";
            } else {
                $this->log->info("Authentication failed. Retrieve a new code.");
                $this->log->info("Received error is: " . print_r($token, true));
                echo "<script type='text/javascript'>localStorage.setItem('piggy-link-event-successful', 'false')</script>";
            }

            echo "<script type='text/javascript'>window.close();</script>";

            return $token;
        }

        return null;
    }

    /**
     * @param mixed $oauthService
     */
    public function setOauthService($oauthService)
    {
        $this->oauthService = $oauthService;
    }

    /**
     *
     */
    public function add_endpoint()
    {
        add_rewrite_endpoint($this->pageSlug, EP_PAGES);
    }
}
