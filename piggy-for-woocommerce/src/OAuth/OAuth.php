<?php

namespace PFW\OAuth;
defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

use PFW\Base;

/**
 * Class OAuth
 * @package PFW\Customer
 */
class OAuth extends Base
{
    /**
     * @return null
     */
    public function add_callback_page()
    {
        if($this->pluginIsConfigured() && get_option(PFW_SLUG . "_allow_linking")) {
            new Callback();
        }
    }

}