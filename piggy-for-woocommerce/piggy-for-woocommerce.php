<?php

/**
 * @link              https://www.piggy.nl
 * @since             1.0.0
 * @package           Piggy_For_Woocommerce
 *
 * @wordpress-plugin
 * Plugin Name:       Piggy for WooCommerce
 * Plugin URI:        https://www.piggy.eu
 * Description:       Piggy Integration for WooCommerce
 * Version:           1.2.1
 * Author:            Piggy Loyalty
 * Author URI:        https://www.piggy.eu
 * Text Domain:       piggy-for-woocommerce
 * Domain Path:       /languages
 * License: GPLv2 or later
 * WC requires at least: 4.2
 * WC tested up to: 4.5
 */

/**
 * Current plugin version.
 */
define( 'PFW_VERSION', '1.2.1' );


use PFW\Includes\Core\ActivationService;
use PFW\Includes\Core\DeactivationService;
use PFW\Includes\Plugin;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}



/**
 * Global variables.
 * (might define in different file, but we need plugin_dir_path from root)
 */
define( 'PFW_PLUGIN_DIR', plugin_dir_path( __FILE__ ));
define( 'PFW_PLUGIN_NAME', 'piggy-for-woocommerce');
define( 'PFW_SLUG', 'pfw');
define( 'PFW_PUBLIC_PATH',  plugin_dir_url( __FILE__ ) . '/public');
define( 'PFW_STYLES_PATH', plugin_dir_url( __FILE__ ) . '/public/');
define( 'PFW_SCRIPTS_PATH', plugin_dir_url( __FILE__ ) . '/public/');
define( 'PFW_MIN_PHP_VERSION', '7.2');
define( 'PFW_MIN_WOOCOMMERCE_VERSION', '4.2.1');
define( 'PFW_ENV', 'production');
/**
 * @throws Exception
 */
function activatePFW() {
    require_once plugin_dir_path( __FILE__ ) . 'includes/Core/ActivationService.php';
    $activationService = new ActivationService();
    try {
        $activationService->activate();
    } catch (Exception $exception) {
        deactivate_plugins( basename( __FILE__ ) );
        die($exception->getMessage() . ", please try again or contact us.");
    }
}

/**
 *
 */
function deactivatePFW() {
    require_once plugin_dir_path( __FILE__ ) . 'includes/Core/DeactivationService.php';
    $deactivationService = new DeactivationService();
    $deactivationService->deactivate();
}

///**
// * @param $upgrader_object
// * @param $options
// */
//function upgradePFW($upgrader_object, $options) {
//    require_once plugin_dir_path( __FILE__ ) . 'includes/core/upgrade.php';
//    Upgrade::upgrade($upgrader_object, $options);
//}

register_activation_hook( __FILE__, 'activatePFW');
register_uninstall_hook( __FILE__, 'deactivatePFW');

//add_action( 'upgrader_process_complete', 'upgrade_PFW', 10, 2);

require plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';

function runPFW() {
    $plugin = new Plugin();
    $plugin->run();
}

runPFW();
