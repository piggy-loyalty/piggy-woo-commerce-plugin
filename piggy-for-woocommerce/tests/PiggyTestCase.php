<?php

namespace Test;

use WP_UnitTestCase;

class PiggyTestCase extends WP_UnitTestCase
{
    protected function skipForWCVersions(array $versions)
    {
        if (in_array(WC()->version, $versions)) {
            $this->markTestSkipped("Skip test for WooCommerce version " . WC()->version);
        }
    }

}