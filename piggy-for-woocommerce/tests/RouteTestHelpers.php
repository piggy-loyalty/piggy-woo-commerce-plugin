<?php

namespace Test;

use WP_REST_Server;

trait RouteTestHelpers
{
    /**
     * @param WP_REST_Server $server
     * @param $expectedCallback
     * @return bool
     */
    protected function has_callback_in_route($server, $namespacedRoute, $expectedCallback) {
        $test = false;

        $routes = $server->get_routes();
        foreach( $routes as $route => $route_config ) {
            if($route == $namespacedRoute) {
                foreach( $route_config as $i => $endpoint ) {
                    $callbackName = $endpoint['callback'][1];

                    if($callbackName == $expectedCallback) {
                        $test = true;
                    }
                }
            }
        }

        return $test;
    }

    /**
     * @param WP_REST_Server $server
     * @param $expectedCallback
     * @param $expectedPermission
     * @return bool
     */
    protected function callback_has_permission($server, $namespacedRoute, $expectedCallback, $expectedPermission) {
        $test = false;

        $routes = $server->get_routes();
        foreach( $routes as $route => $route_config ) {
            if($route == $namespacedRoute) {
                foreach( $route_config as $i => $endpoint ) {
                    $callbackName = $endpoint['callback'][1];
                    $permissionCheck = $endpoint['permission_callback'][1];

                    if(($callbackName == $expectedCallback) && ($permissionCheck == $expectedPermission)
                    ) {
                        $test = true;
                    }
                }
            }
        }

        return $test;
    }

    /**
     * @param WP_REST_Server $server
     * @param $expectedCallback
     * @param $expectedMethod
     * @return bool
     */
    protected function callback_has_method($server, $namespacedRoute, $expectedCallback, $expectedMethod) {
        $test = false;

        $routes = $server->get_routes();
        foreach( $routes as $route => $route_config ) {
            if($route == $namespacedRoute) {
                foreach( $route_config as $i => $endpoint ) {
                    $callbackName = $endpoint['callback'][1];

                    if($callbackName == $expectedCallback) {
                        $methods = "";
                        foreach($endpoint['methods'] as $method => $bool) {
                            if($methods == "") {
                                $methods = $method;
                            } else {
                                $methods = $methods . ", " . $method;
                            }
                        }

                        if($methods == $expectedMethod) {
                            $test = true;

                        }
                    }
                }
            }
        }

        return $test;
    }
}