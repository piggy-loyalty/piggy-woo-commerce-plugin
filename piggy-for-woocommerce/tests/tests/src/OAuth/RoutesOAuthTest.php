<?php

use Test\PiggyTestCase;
use Test\RouteTestHelpers;

class RoutesOAuthTest extends PiggyTestCase
{
    use RouteTestHelpers;

    /**
     * Test REST Server
     * @var WP_REST_Server
     */
    protected $server;

    protected $oauthRoute = '/pfw/v1/oauth';

    public function setUp() {
        parent::setUp();

        /** @var WP_REST_Server $wp_rest_server */
        $this->server = rest_get_server();
        do_action( 'rest_api_init' );

        update_option(PFW_SLUG . "_client_token", "1");
        update_option(PFW_SLUG . "_shop_id", "1");
        update_option(PFW_SLUG . "_flow_option", "oauth");

//      https://wordpress.org/support/topic/notice-register_rest_route-was-called-incorrectly/
        $this->skipForWCVersions(["4.2.1", "4.3.1"]);
    }

    /** @test */
    public function it_registered_the_oauth_routes()
    {
        $routes = $this->server->get_routes();
        $this->assertArrayHasKey( $this->oauthRoute, $routes );
    }

    /** @test */
    public function it_has_the_right_callback_method_and_permissions_for_get_authentication_url()
    {
        $callback = "getAuthenticationUri";
        $method = WP_REST_Server::READABLE;

        $this->assertTrue($this->has_callback_in_route($this->server, $this->oauthRoute, $callback));
        $this->assertTrue($this->callback_has_method($this->server, $this->oauthRoute, $callback, $method));
    }

    /** @test */
    public function it_has_the_right_callback_method_and_permissions_for_delete()
    {
        $callback = "delete";
        $method = WP_REST_Server::CREATABLE;
        $endpointUrl = $this->oauthRoute . "/delete";

        $this->assertTrue($this->has_callback_in_route($this->server, $endpointUrl, $callback));
        $this->assertTrue($this->callback_has_method($this->server, $endpointUrl, $callback, $method));
    }
}