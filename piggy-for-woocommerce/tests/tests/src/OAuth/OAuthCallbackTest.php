<?php

use PFW\Includes\Plugin;
use PFW\OAuth\Callback;
use PFW\Piggy\Controllers\AuthController;
use PFW\Piggy\Services\OAuthService;
use PHPUnit\Framework\MockObject\MockObject;
use PFW\Container\Piggy\Api\Model\OAuthToken;
use Test\PiggyTestCase;

class OAuthCallbackTest extends PiggyTestCase {

    public function setUp()
    {
        parent::setUp();

        update_option(PFW_SLUG . "_client_token", "1");
        update_option(PFW_SLUG . "_shop_id", "1");
        update_option(PFW_SLUG . '_flow_option', "oauth");
    }

    /** @test */
    public function it_returns_null_when_flow_is_staged()
    {
        update_option(PFW_SLUG . "_client_token", "");
        update_option(PFW_SLUG . "_shop_id", "");
        update_option(PFW_SLUG . '_flow_option', "staged");

        $plugin = new Plugin();
        $oauth = $plugin->getOAuth();

        $this->assertEquals($oauth->add_callback_page(), null);
    }

//    /** @test */
//    public function it_returns_the_customer_page_when_linking_account_is_allowed()
//    {
//        $plugin = new Plugin();
//        $page = new Callback();
//        $oauth = $plugin->getOAuth();
//
//        update_option(PFW_SLUG . "_allow_linking", "true");
//
//        $this->assertEquals($page, $oauth->add_callback_page());
//    }

    /** @test
     * @throws Exception
     */
    public function it_returns_null_when_code_not_in_request()
    {
        $page = new Callback();
        $this->assertNull($page->handle_callback());
    }

    /** @test
     * @throws Exception
     */
    public function it_returns_an_script_to_set_localstorage_with_value_false()
    {
        $page = new Callback();
        $mockedOAuthService = $this->createMock(OAuthService::class);

        $_GET['code'] = "123";
        wp_set_current_user( 1 );
        $mockedOAuthService->expects($this->once())->method('authenticate')->willReturn("123");
        $page->setOauthService($mockedOAuthService);

        ob_start();
        $page->handle_callback();
        $result = ob_get_contents();
        ob_end_clean();

        $expected = '<script type=\'text/javascript\'>localStorage.setItem(\'piggy-link-event-successful\', \'false\')</script>';
        $expected .= '<script type=\'text/javascript\'>window.close();</script>';

        $this->assertSame( trim( $expected ), trim( $result ) );
    }

    /** @test
     * @throws Exception
     */
    public function it_returns_an_script_to_set_localstorage_with_value_true()
    {
        $page = new Callback();
        $mockedOAuthService = $this->createMock(OAuthService::class);

        $token = new OAuthToken();
        $token->setAccessToken("123");
        $token->setExpiresIn("234");
        $token->setRefreshToken("345");

        $_GET['code'] = "123";
        wp_set_current_user( 1 );
        $mockedOAuthService->expects($this->once())->method('authenticate')->willReturn($token);
        $page->setOauthService($mockedOAuthService);

        ob_start();
        $actualToken = $page->handle_callback();
        $result = ob_get_contents();
        ob_end_clean();

        $expected = '<script type=\'text/javascript\'>localStorage.setItem(\'piggy-link-event-successful\', \'true\')</script>';
        $expected .= '<script type=\'text/javascript\'>window.close();</script>';

        $this->assertSame( trim( $expected ), trim( $result ) );
        $this->assertEquals($token, $actualToken);
    }
}