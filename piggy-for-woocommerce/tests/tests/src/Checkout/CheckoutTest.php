<?php

use PFW\Checkout\Checkbox;
use PFW\Checkout\CustomerWidget;
use PFW\Checkout\StagedCheckbox;
use PFW\Includes\Plugin;
use PFW\Piggy\Logger;
use PFW\Piggy\Services\CreditReceptionsService;
use PFW\Piggy\Services\OAuthService;
use PFW\Piggy\Services\StagedCreditReceptionsService;
use PHPUnit\Framework\MockObject\MockObject;
use PFW\Container\Piggy\Api\Model\Customer;
use Test\PiggyTestCase;

class CheckoutTest extends PiggyTestCase
{

    public function setUp()
    {
        parent::setUp();

        update_option(PFW_SLUG . "_client_token", "1");
        update_option(PFW_SLUG . "_shop_id", "1");
    }

//    /** @test */
//    public function it_loads_the_script_files_to_the_queue()
//    {
//        $plugin = new Plugin();
//        $checkout = $plugin->getCheckout();
//
//        $scriptName = PFW_SLUG . "-customer-script";
//        $status = "queue";
//
//        $_SERVER['REQUEST_URI'] = "/checkout/";
//
//        $checkout->enqueue_scripts();
//
//        $this->assertTrue(wp_script_is($scriptName, $status));
//
//        wp_dequeue_script($scriptName);
//    }

    /** @test */
    public function it_returns_checkbox_when_flow_is_staged()
    {
        $plugin = new Plugin();
        $checkout = $plugin->getCheckout();
        update_option(PFW_SLUG . "_guest_flow_option", "optional");

        /** @var StagedCheckbox|MockObject $mockedCheckbox */
        $mockedCheckbox = $this->createMock(StagedCheckbox::class);
        $mockedCheckbox->expects($this->once())->method('render_checkbox');
        $checkout->setStagedCheckbox($mockedCheckbox);

        $checkout->add_to_form(new WC_Form_Handler());

        update_option(PFW_SLUG . "_guest_flow_option", "");
    }

    /** @test */
    public function it_returns_an_oauth_widget_when_flow_is_oauth()
    {
        update_option(PFW_SLUG . "_allow_linking", "true");
        update_option(PFW_SLUG . "_client_token", "1");
        update_option(PFW_SLUG . "_shop_id", "1");
        wp_set_current_user( 1 );

        $plugin = new Plugin();
        $checkout = $plugin->getCheckout();

        /** @var OAuthService|MockObject $mockedAuthService */
        $mockedAuthService = $this->createMock(OAuthService::class);
        $mockedAuthService->expects($this->once())->method('isUserLinked')->willReturn(true);
        $checkout->setAuthService($mockedAuthService);

        /** @var CustomerWidget|MockObject $mockedOAuthWidget */
        $mockedOAuthWidget = $this->createMock(CustomerWidget::class);
        $mockedOAuthWidget->expects($this->once())->method('render_component_library');
        $checkout->setCustomerWidget($mockedOAuthWidget);

        $checkout->add_to_form(new WC_Form_Handler());
    }

    /** @test */
    public function it_logs_an_error_when_order_is_not_instance_of_wc_order()
    {
        $plugin = new Plugin();
        $checkout = $plugin->getCheckout();

        /** @var MockObject|Logger $mockedLogger */
        $mockedLogger = $this->createMock(Logger::class);
        $checkout->setLogger($mockedLogger);

        $message = "Order not found for order: 1";
        $mockedLogger->expects($this->once())->method('error')->with($message);

        $order = $checkout->save_order_meta(1);
    }

    /** @test */
    public function it_sets_order_meta_data_with_staged_checkbox_true()
    {
        $plugin = new Plugin();
        $checkout = $plugin->getCheckout();

        /** @var WC_Order $order */
        $order = wc_create_order();

        $_POST['pfw-staged-credit-reception-checkbox'] = 'true';

        $checkout->save_order_meta($order->get_id());

        $enabled_staged = $order->get_meta_data()[0]->get_data()["value"];


        $this->assertEquals(true, $enabled_staged);
    }

    /** @test */
    public function it_sets_order_meta_data_with_staged_checkbox_false()
    {
        $plugin = new Plugin();
        $checkout = $plugin->getCheckout();

        /** @var WC_Order $order */
        $order = wc_create_order();

        $checkout->save_order_meta($order->get_id());

        $enabled_staged = $order->get_meta_data()[0]->get_data()["value"];
        $this->assertEquals(false, $enabled_staged);
    }

    /** @test */
    public function it_returns_error_when_order_is_no_wc_order()
    {
        $plugin = new Plugin();
        $checkout = $plugin->getCheckout();

        $mockedLogger = $this->createMock(Logger::class);
        $mockedLogger->expects($this->once())->method('error')->with("Order not found.");

        /** @var Logger $mockedLogger */
        $checkout->setLogger($mockedLogger);

        $checkout->on_payment_completed(null);
    }

    /** @test */
    public function it_sends_credit_receptions_to_an_linked_customer()
    {
        global $wpdb;
        $wpdb->suppress_errors(true);

        $plugin = new Plugin();
        $checkout = $plugin->getCheckout();

        $mockedCreditsReceptionService = $this->createMock(CreditReceptionsService::class);
        $mockedCreditsReceptionService->expects($this->once())->method('handle');

        $customer = new Customer();
        $customer->setId(1);
        $customer->setEmail("peter@peter.nl");

        /** @var OAuthService|MockObject $mockedAuthService */
        $mockedAuthService = $this->createMock(OAuthService::class);
        $mockedAuthService->expects($this->once())->method('profile')->willReturn($customer);
        $checkout->setOAuthService($mockedAuthService);

        /** @var CreditReceptionsService $mockedCreditsReceptionService */
        $checkout->setCreditReceptionsService($mockedCreditsReceptionService);

        /** @var WC_Order $order */
        $order = wc_create_order();
        $order->set_total("100");
        $order->set_billing_email("peter@test.nl");

        $allowLinking = update_option(PFW_SLUG . "_allow_linking", "true");
        $flow = update_option(PFW_SLUG . "_guest_flow_option", "optional");

        $checkout->on_payment_completed($order);

        $wpdb->suppress_errors(false);
    }

    /** @test */
    public function it_returns_nothing_when_guest_flow_option_is_never()
    {
        global $wpdb;
        $wpdb->suppress_errors(true);

        $plugin = new Plugin();
        $checkout = $plugin->getCheckout();

        $mockedStagedCreditReceptionService = $this->createMock(StagedCreditReceptionsService::class);
        $mockedStagedCreditReceptionService->expects($this->never())->method('handle');
        /** @var StagedCreditReceptionsService $mockedStagedCreditReceptionService */
        $checkout->setStagedCreditReceptionsService($mockedStagedCreditReceptionService);

        /** @var WC_Order $order */
        $order = wc_create_order();
        $order->set_total("100");
        $order->set_billing_email("peter@test.nl");

        $allowLinking = update_option(PFW_SLUG . "_allow_linking", "true");
        $flow = update_option(PFW_SLUG . "_guest_flow_option", "never");

        $this->assertNull($checkout->on_payment_completed($order));

        $wpdb->suppress_errors(false);
    }

    /** @test */
    public function it_writes_to_log_when_checkbox_is_not_checked_and_flow_is_optional()
    {
        global $wpdb;
        $wpdb->suppress_errors(true);

        $plugin = new Plugin();
        $checkout = $plugin->getCheckout();

        $email = "peter@test.nl";

        $mockedLogger = $this->createMock(Logger::class);
        $mockedLogger->expects($this->once())->method('info')->with("Customer decided to not check the checkbox for staged: ");
        /** @var Logger $mockedLogger */
        $checkout->setLogger($mockedLogger);

        /** @var WC_Order $order */
        $order = wc_create_order();
        $order->set_total("100");
        $order->set_billing_email($email);
        $order->add_meta_data(PFW_SLUG . "_has_enabled_staged_checkbox", false);

        $allowLinking = update_option(PFW_SLUG . "_allow_linking", "true");
        $flow = update_option(PFW_SLUG . "_guest_flow_option", "optional");

        $checkout->on_payment_completed($order);

        $wpdb->suppress_errors(false);
    }

    /** @test */
    public function it_sends_staged_credit_reception_when_guest_flow_option_is_optional_and_checkbox_has_been_checked()
    {
        global $wpdb;
        $wpdb->suppress_errors(true);

        $plugin = new Plugin();
        $checkout = $plugin->getCheckout();

        $email = "peter@test.nl";

        $mockedStagedCreditReceptionService = $this->createMock(StagedCreditReceptionsService::class);
        $mockedStagedCreditReceptionService->expects($this->once())->method('handle');
        /** @var StagedCreditReceptionsService $mockedStagedCreditReceptionService */
        $checkout->setStagedCreditReceptionsService($mockedStagedCreditReceptionService);

        $order = wc_create_order();
        $order->set_total("100");
        $order->set_billing_email($email);
        $order->add_meta_data(PFW_SLUG . "_has_enabled_staged_checkbox", true);
        $order->save();

        update_option(PFW_SLUG . "_allow_linking", "true");
        update_option(PFW_SLUG . "_guest_flow_option", "optional");

        $checkout->on_payment_completed($order);

        $wpdb->suppress_errors(false);
    }

    /** @test */
    public function it_sends_staged_credit_reception_when_guest_flow_option_is_always()
    {
        global $wpdb;
        $wpdb->suppress_errors(true);

        $plugin = new Plugin();
        $checkout = $plugin->getCheckout();

        $email = "peter@test.nl";

        $mockedStagedCreditReceptionService = $this->createMock(StagedCreditReceptionsService::class);
        $mockedStagedCreditReceptionService->expects($this->once())->method('handle');
        /** @var StagedCreditReceptionsService $mockedStagedCreditReceptionService */
        $checkout->setStagedCreditReceptionsService($mockedStagedCreditReceptionService);

        $order = wc_create_order();
        $order->set_total("100");
        $order->set_billing_email($email);
        $order->add_meta_data(PFW_SLUG . "_has_enabled_staged_checkbox", false);
        $order->save();

        update_option(PFW_SLUG . "_allow_linking", "true");
        update_option(PFW_SLUG . "_guest_flow_option", "always");

        $checkout->on_payment_completed($order);

        $wpdb->suppress_errors(false);
    }

    /** @test */
    public function it_returns_nothing_when_user_flow_option_is_never()
    {
        global $wpdb;
        $wpdb->suppress_errors(true);

        $plugin = new Plugin();
        $checkout = $plugin->getCheckout();

        $mockedStagedCreditReceptionService = $this->createMock(StagedCreditReceptionsService::class);
        $mockedStagedCreditReceptionService->expects($this->never())->method('handle');
        /** @var StagedCreditReceptionsService $mockedStagedCreditReceptionService */
        $checkout->setStagedCreditReceptionsService($mockedStagedCreditReceptionService);

        $email = "peter@test.nl";
        $order = wc_create_order();
        $order->set_customer_id(1);
        $order->set_total("100");
        $order->set_billing_email($email);
        $order->add_meta_data(PFW_SLUG . "_has_enabled_staged_checkbox", false);
        $order->save();

        wp_set_current_user(1);

        update_option(PFW_SLUG . "_allow_linking", "true");
        update_option(PFW_SLUG . "_user_flow_option", "never");

        $this->assertNull($checkout->on_payment_completed($order));

        $wpdb->suppress_errors(false);
    }

    /** @test */
    public function it_writes_to_log_when_checkbox_is_not_checked_and_flow_is_optional_for_logged_in_user()
    {
        global $wpdb;
        $wpdb->suppress_errors(true);

        $plugin = new Plugin();
        $checkout = $plugin->getCheckout();

        $mockedStagedCreditReceptionService = $this->createMock(StagedCreditReceptionsService::class);
        $mockedStagedCreditReceptionService->expects($this->never())->method('handle');

        /** @var StagedCreditReceptionsService $mockedStagedCreditReceptionService */
        $checkout->setStagedCreditReceptionsService($mockedStagedCreditReceptionService);

        $email = "peter@test.nl";
        $order = wc_create_order();
        $order->set_customer_id(1);
        $order->set_total("100");
        $order->set_billing_email($email);
        $order->add_meta_data(PFW_SLUG . "_has_enabled_staged_checkbox", false);
        $order->save();

        $mockedLogger = $this->createMock(Logger::class);

        $mockedLogger->expects($this->exactly(2))->method('info');
        /** @var Logger $mockedLogger */
        $checkout->setLogger($mockedLogger);

        wp_set_current_user(1);

        update_option(PFW_SLUG . "_allow_linking", "true");
        update_option(PFW_SLUG . "_user_flow_option", "optional");

        $checkout->on_payment_completed($order);

        $wpdb->suppress_errors(false);
    }

    /** @test */
    public function it_sends_staged_credit_receptions_when_user_flow_option_is_optional_and_checkbox_is_checked()
    {
        global $wpdb;
        $wpdb->suppress_errors(true);

        $plugin = new Plugin();
        $checkout = $plugin->getCheckout();

        $mockedStagedCreditReceptionService = $this->createMock(StagedCreditReceptionsService::class);
        $mockedStagedCreditReceptionService->expects($this->once())->method('handle');
        /** @var StagedCreditReceptionsService $mockedStagedCreditReceptionService */
        $checkout->setStagedCreditReceptionsService($mockedStagedCreditReceptionService);

        $email = "peter@test.nl";
        $order = wc_create_order();
        $order->set_total("100");
        $order->set_billing_email($email);
        $order->add_meta_data(PFW_SLUG . "_has_enabled_staged_checkbox", true);
        $order->save();

        wp_set_current_user(1);

        update_option(PFW_SLUG . "_allow_linking", "true");
        update_option(PFW_SLUG . "_user_flow_option", "optional");

        $checkout->on_payment_completed($order);

        $wpdb->suppress_errors(false);
    }

    /** @test */
    public function it_sends_staged_credit_reception_when_user_flow_option_is_always()
    {
        global $wpdb;
        $wpdb->suppress_errors(true);

        $plugin = new Plugin();
        $checkout = $plugin->getCheckout();

        $mockedStagedCreditReceptionService = $this->createMock(StagedCreditReceptionsService::class);
        $mockedStagedCreditReceptionService->expects($this->once())->method('handle');
        /** @var StagedCreditReceptionsService $mockedStagedCreditReceptionService */
        $checkout->setStagedCreditReceptionsService($mockedStagedCreditReceptionService);

        $email = "peter@test.nl";
        $order = wc_create_order();
        $order->set_total("100");
        $order->set_billing_email($email);
        $order->save();

        wp_set_current_user(1);

        update_option(PFW_SLUG . "_allow_linking", "true");
        update_option(PFW_SLUG . "_user_flow_option", "always");

        $checkout->on_payment_completed($order);

        $wpdb->suppress_errors(false);
    }
}