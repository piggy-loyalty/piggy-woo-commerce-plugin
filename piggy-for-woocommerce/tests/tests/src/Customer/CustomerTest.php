<?php

use PFW\Customer\Customer;
use PFW\Includes\Plugin;
use Test\PiggyTestCase;

class CustomerTest extends PiggyTestCase
{

    public function setUp()
    {
        parent::setUp();

        update_option(PFW_SLUG . "_client_token", "1");
        update_option(PFW_SLUG . "_shop_id", "1");
    }

//    NOTE:     See story in Jira - WSP 74
//    /** @test */
//    public function it_loads_the_script_files_to_the_queue()
//    {
//        $plugin = new Plugin();
//        $customer = $plugin->getCustomer();
//
//        $scriptName = PFW_SLUG . "-customer-script";
//        $status = "queue";
//
//        $customer->enqueue_scripts();
//
//        $this->assertTrue(wp_script_is($scriptName, $status));
//
//        wp_dequeue_script($scriptName);
//    }

    /** @test */
    public function it_returns_the_customer_page_when_linking_account_is_allowed()
    {
        update_option(PFW_SLUG . '_allow_linking', "true");

        $plugin = new Plugin();
        $page = (new Customer())->add_customer_page();
        $customer = $plugin->getCustomer();

        $this->assertEquals($page, $customer->add_customer_page());
    }

    /** @test */
    public function it_adds_piggy_account_to_wc_my_account_menu()
    {
        $plugin = new Plugin();
        $customer = $plugin->getCustomer();

        $menuItems = wc_get_account_menu_items();

        $this->assertEquals(6, count($menuItems));

        $customer = new Customer();
        $menuItems = $customer->add_menu_item($menuItems);

        $this->assertEquals(7, count($menuItems));
    }

    /** @test */
    public function it_returns_a_div_for_the_frontend_to_be_loaded()
    {
        $customer = new Customer();

        ob_start();
        $customer->render_component_library();
        $result = ob_get_contents();
        ob_end_clean();

        $componentId = PFW_SLUG . '-customer';
        $expected = '<div id="' . $componentId . '" class="wrap"></div>';

        $this->assertSame( trim( $expected ), trim( $result ) );
    }
}