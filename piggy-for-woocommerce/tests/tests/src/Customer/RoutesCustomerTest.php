<?php

use Test\PiggyTestCase;
use Test\RouteTestHelpers;

class RoutesCustomerTest extends PiggyTestCase
{
    use RouteTestHelpers;

    /**
     * Test REST Server
     * @var WP_REST_Server
     */
    protected $server;

    protected $customerRoute = '/pfw/v1/customer';

    public function setUp() {
        parent::setUp();

        /** @var WP_REST_Server $wp_rest_server */
        global $wp_rest_server;
        $this->server = $wp_rest_server = new WP_REST_Server;
        do_action( 'rest_api_init' );

        update_option(PFW_SLUG . "_client_token", "1");
        update_option(PFW_SLUG . "_shop_id", "1");
        update_option(PFW_SLUG . "_flow_option", "oauth");

//      https://wordpress.org/support/topic/notice-register_rest_route-was-called-incorrectly/
        $this->skipForWCVersions(["4.2.1", "4.3.1"]);
    }

    /** @test */
    public function it_registered_the_setup_routes()
    {
        $routes = $this->server->get_routes();
        $this->assertArrayHasKey( $this->customerRoute . '/profile', $routes );
    }

    /** @test */
    public function it_has_the_right_callback_method_and_permissions_for_profile_show() {
        $callback = "show";
        $method = WP_REST_Server::READABLE;

        $this->assertTrue($this->has_callback_in_route($this->server, $this->customerRoute . '/profile', $callback));
        $this->assertTrue($this->callback_has_method($this->server, $this->customerRoute . '/profile', $callback, $method));
    }
}
