<?php

use PFW\Piggy\Controllers\FlowSettingsController;
use PFW\Piggy\Error;
use Test\PiggyTestCase;

class FlowSettingsControllerTest extends PiggyTestCase {

    /** @test */
    public function it_returns_the_flow_options_and_settings()
    {
        $userFlow = "optional";
        $guestFlow = "optional";
        $allowLinking = true;

        update_option(  PFW_SLUG . "_user_flow_option", $userFlow);
        update_option( PFW_SLUG . "_guest_flow_option", $guestFlow);
        update_option( PFW_SLUG . "_allow_linking", $allowLinking);

        $controller = new FlowSettingsController();

        $response = $controller->index();

        $this->assertEquals($response, $controller->response([
            "guest_flow" => $guestFlow,
            "user_flow" => $userFlow,
            "allow_linking" => $allowLinking,
        ]));
    }

    /** @test */
    public function it_returns_error_when_input_is_invalid()
    {
        $controller = new FlowSettingsController();
        $error = $controller->update([]);

        $this->assertEquals(Error::INVALID_INPUT, $error->data["code"]);
        $this->assertEquals(Error::getMessage(Error::INVALID_INPUT), $error->data["message"]);
    }

    /** @test */
    public function it_updates_the_flow_option_and_settings()
    {
        $controller = new FlowSettingsController();

        update_option(  PFW_SLUG . "_user_flow_option", "1");
        update_option( PFW_SLUG . "_guest_flow_option", "1");
        update_option( PFW_SLUG . "_allow_linking", "1");

        $userFlow = "optional1";
        $guestFlow = "optional1";
        $allowLinking = false;

        $response = $controller->update([
            "guest_flow" => $guestFlow,
            "user_flow" => $userFlow,
            "allow_linking" => $allowLinking,
        ]);

        $this->assertEquals($controller->response([
            "guest_flow" => $guestFlow,
            "user_flow" => $userFlow,
            "allow_linking" => $allowLinking,
        ]), $response);

    }
}