<?php

use PFW\Piggy\Controllers\BaseController;
use PFW\Piggy\Models\OAuthToken;
use PFW\Piggy\Repositories\OAuthTokenRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PFW\Container\Piggy\Api\Customer\PiggyCustomers;
use Test\PiggyTestCase;

class BaseControllerTest extends PiggyTestCase
{

    /** @test */
    public function it_returns_false_when_user_not_found()
    {
        $controller = new BaseController();

        /** @var MockObject|OAuthTokenRepository $mockedOAuthTokenRepository */
        $mockedOAuthTokenRepository = $this->createMock(OAuthTokenRepository::class);
        $mockedOAuthTokenRepository->expects($this->once())->method('findOneBy')->willReturn(false);
        $controller->setOauthTokenRepository($mockedOAuthTokenRepository);

        $actual = $controller->verifyCustomer(1);

        $this->assertFalse($actual);
    }

    /** @test */
    public function it_returns_true_when_user_is_found()
    {
        $controller = new BaseController();

        $mockedOAuthToken = $this->createMock(OAuthToken::class);
        $mockedOAuthToken->expects($this->once())->method('getAccessToken')->willReturn('1');

        /** @var MockObject|OAuthTokenRepository $mockedOAuthTokenRepository */
        $mockedOAuthTokenRepository = $this->createMock(OAuthTokenRepository::class);
        $mockedOAuthTokenRepository->expects($this->once())->method('findOneBy')->willReturn($mockedOAuthToken);
        $controller->setOauthTokenRepository($mockedOAuthTokenRepository);

        $actual = $controller->verifyCustomer(1);

        $this->assertTrue($actual);
    }
}