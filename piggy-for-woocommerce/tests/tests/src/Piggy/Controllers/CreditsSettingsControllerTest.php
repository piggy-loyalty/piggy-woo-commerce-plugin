<?php

use PFW\Piggy\Controllers\CreditsSettingsController;
use PFW\Piggy\Error;
use Test\PiggyTestCase;

class CreditsSettingsControllerTest extends PiggyTestCase {

    /** @test */
    public function it_returns_the_flow_options_and_settings()
    {
        $creditsIncludeTaxes = true;
        $creditsIncludeShippingCost = true;

        update_option(PFW_SLUG . "_credits_include_taxes", $creditsIncludeTaxes);
        update_option(PFW_SLUG . "_credits_include_shipping_cost", $creditsIncludeShippingCost);

        $controller = new CreditsSettingsController();

        $response = $controller->index();

        $this->assertEquals($response, $controller->response([
            "credits_include_taxes" => $creditsIncludeTaxes,
            "credits_include_shipping_cost" => $creditsIncludeShippingCost,
        ]));
    }

    /** @test */
    public function it_returns_error_when_input_is_invalid()
    {
        $controller = new CreditsSettingsController();
        $error = $controller->update([]);

        $this->assertEquals(Error::INVALID_INPUT, $error->data["code"]);
        $this->assertEquals(Error::getMessage(Error::INVALID_INPUT), $error->data["message"]);
    }

    /** @test */
    public function it_updates_the_flow_option_and_settings()
    {
        $controller = new CreditsSettingsController();

        update_option(PFW_SLUG . "_credits_include_taxes", "1");
        update_option(PFW_SLUG . "_credits_include_shipping_cost", "1");

        $creditsIncludeTaxes = false;
        $creditsIncludeShippingCost = false;

        $response = $controller->update([
            "credits_include_taxes" => $creditsIncludeTaxes,
            "credits_include_shipping_cost" => $creditsIncludeShippingCost,
        ]);

        $this->assertEquals($controller->response([
            "credits_include_taxes" => $creditsIncludeTaxes,
            "credits_include_shipping_cost" => $creditsIncludeShippingCost,
        ]), $response);

    }
}