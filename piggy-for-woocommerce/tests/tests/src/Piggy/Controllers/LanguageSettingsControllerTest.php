<?php

use PFW\Piggy\Controllers\LanguageSettingsController;
use PFW\Piggy\Error;
use Test\PiggyTestCase;

class LanguageSettingsControllerTest extends PiggyTestCase {

    /** @test */
    public function it_returns_the_flow_options_and_settings()
    {
        $language = "en";
        $language_options = [
            ["id" => "nl", "label" => "dutch"],
            ["id" => "en", "label" => "english"],
            ["id" => "de", "label" => "german"],
        ];

        update_option( PFW_SLUG . "_language", $language);

        $controller = new LanguageSettingsController();

        $response = $controller->index();

        $this->assertEquals($response, $controller->response([
            "language" => $language,
            "language_options" => $language_options
        ]));
    }

    /** @test */
    public function it_returns_error_when_input_is_invalid()
    {
        $controller = new LanguageSettingsController();
        $error = $controller->update([]);

        $this->assertEquals(Error::INVALID_INPUT, $error->data["code"]);
        $this->assertEquals(Error::getMessage(Error::INVALID_INPUT), $error->data["message"]);
    }

    /** @test */
    public function it_updates_the_flow_option_and_settings()
    {
        $controller = new LanguageSettingsController();

        update_option( PFW_SLUG . "_language", "de");

        $language = "en";

        $response = $controller->update([
            "language" => $language
        ]);

        $this->assertEquals($controller->response([
            "language" => $language,
        ]), $response);

    }
}