<?php

use PFW\Piggy\Controllers\AuthController;
use PFW\Piggy\Error;
use PFW\Piggy\Repositories\OAuthTokenRepository;
use PFW\Piggy\Services\OAuthService;
use PHPUnit\Framework\MockObject\MockObject;
use PFW\Container\Piggy\Api\Http\Customer;
use PFW\Container\Piggy\Api\PiggyApi;
use Test\PiggyTestCase;

class AuthControllerTest extends PiggyTestCase
{

    /** @test */
    public function it_returns_the_authentication_url()
    {
        $controller = new AuthController();

        /** @var MockObject|OAuthService $mockedOAuthService */
        $mockedOAuthService = $this->createMock(OAuthService::class);

        $mockedOAuthService->expects($this->once())->method('generateAuthenticationUri')->willReturn("http://localhost/");
        $controller->setOAuthService($mockedOAuthService);

        $response = $controller->getAuthenticationUri();
        $url = $response->data["data"];

        $this->assertEquals("http://localhost/", $url);
    }

    /** @test */
    public function it_returns_error_when_oauth_token_invalid_when_deleting_oauth_token()
    {
        $controller = new AuthController();

        /** @var MockObject|OAuthTokenRepository $mockedOAuthTokenRepository */
        $mockedOAuthTokenRepository = $this->createMock(OAuthTokenRepository::class);
        $mockedOAuthTokenRepository->expects($this->once())->method('findOneBy')->willReturn(null);
        $controller->setOAuthTokenRepository($mockedOAuthTokenRepository);

        $error = $controller->delete();

        $this->assertEquals(Error::USER_NOT_LINKED, $error->data["code"]);
        $this->assertEquals(Error::getMessage(Error::USER_NOT_LINKED), $error->data["message"]);
    }

    /** @test */
    public function it_deletes_oauth_token_from_database()
    {
        $controller = new AuthController();

        $OAuthToken = new \PFW\Piggy\Models\OAuthToken();
        $OAuthToken->setAccessToken('123');

        /** @var MockObject|OAuthTokenRepository $mockedOAuthTokenRepository */
        $mockedOAuthTokenRepository = $this->createMock(OAuthTokenRepository::class);
        $mockedOAuthTokenRepository->expects($this->once())->method('findOneBy')->willReturn($OAuthToken);
        $mockedOAuthTokenRepository->expects($this->once())->method('remove');
        $controller->setOAuthTokenRepository($mockedOAuthTokenRepository);

        $controller->delete();
    }
}