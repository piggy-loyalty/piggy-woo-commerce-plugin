<?php

use GuzzleHttp\Exception\GuzzleException;
use PFW\Piggy\Controllers\SetupController;
use PFW\Piggy\Error;
use PFW\Piggy\Logger;
use PHPUnit\Framework\MockObject\MockObject;
use PFW\Container\Piggy\Api\Http\Client;
use PFW\Container\Piggy\Api\Model\LoyaltyProgram;
use PFW\Container\Piggy\Api\Model\Webshop;
use PFW\Container\Piggy\Api\PiggyApi;
use PFW\Container\Piggy\Api\Resources\WebshopsResource;
use Test\PiggyTestCase;

class SetupControllerTest extends PiggyTestCase {

    /** @test
     * @throws GuzzleException
     */
    public function it_returns_wp_error_when_key_or_secret_not_set()
    {
        $controller = new SetupController();

        $error = $controller->authenticate([]);

        $this->assertEquals(Error::INVALID_INPUT, $error->data["code"]);
        $this->assertEquals(Error::getMessage(Error::INVALID_INPUT), $error->data["message"]);
    }

    /** @test
     * @throws GuzzleException
     */
    public function it_returns_wp_error_when_authentication_failed()
    {
        /** @var MockObject|PiggyApi $mockedPiggyApi */
        $mockedPiggyApi = $this->createMock(PiggyApi::class);
        $mockedPiggyClient = $this->createMock(Client::class);
        $mockedPiggyApi->client = $mockedPiggyClient;

        $controller = new SetupController();
        $controller->setPiggyApi($mockedPiggyApi);

        $mockedPiggyClient->expects($this->once())->method('requestClientToken')->willReturn(null);

        $error = $controller->authenticate(['key' => '1', 'secret' => '2']);

        $this->assertEquals(Error::INVALID_TOKEN, $error->data["code"]);
        $this->assertEquals(Error::getMessage(Error::INVALID_TOKEN), $error->data["message"]);
    }

    /** @test
     * @throws GuzzleException
     */
    public function it_sets_authentication_client_token()
    {
        /** @var MockObject|PiggyApi $mockedPiggyApi */
        $mockedPiggyApi = $this->createMock(PiggyApi::class);
        $mockedPiggyClient = $this->createMock(Client::class);
        $mockedPiggyApi->client = $mockedPiggyClient;

        $controller = new SetupController();
        $controller->setPiggyApi($mockedPiggyApi);

        $token = ["access_token" => "123ABC"];

        $mockedPiggyClient->expects($this->once())->method('requestClientToken')->willReturn($token);

        $controller->authenticate(['key' => '1', 'secret' => '2']);

        $this->assertEquals(get_option(PFW_SLUG . '_client_token'), $token["access_token"]);

        delete_option(PFW_SLUG . '_client_token');
    }

    /** @test
     * @throws GuzzleException
     */
    public function it_returns_response_with_client_token()
    {
        /** @var MockObject|PiggyApi $mockedPiggyApi */
        $mockedPiggyApi = $this->createMock(PiggyApi::class);
        $mockedPiggyClient = $this->createMock(Client::class);
        $mockedPiggyApi->client = $mockedPiggyClient;

        $controller = new SetupController();
        $controller->setPiggyApi($mockedPiggyApi);

        $token = ["access_token" => "123ABC"];

        $mockedPiggyClient->expects($this->once())->method('requestClientToken')->willReturn($token);

        $response = $controller->authenticate(['key' => '1', 'secret' => '2']);

        $this->assertEquals($controller->response(["token" => $token["access_token"]]), $response);

        delete_option(PFW_SLUG . '_client_token');
    }


    /** @test */
    public function it_returns_error_when_health_check_fails()
    {
        /** @var MockObject|PiggyApi $mockedPiggyApi */
        $mockedPiggyApi = $this->createMock(PiggyApi::class);
        $mockedPiggyClient = $this->createMock(Client::class);
        $mockedPiggyApi->client = $mockedPiggyClient;

        $controller = new SetupController();
        $controller->setPiggyApi($mockedPiggyApi);

        $mockedPiggyClient->expects($this->once())->method('healthCheck')->willReturn(null);

        $error = $controller->ping();

        $this->assertEquals(Error::HEALTH_CHECK_FAILED, $error->data["code"]);
        $this->assertEquals(Error::getMessage(Error::HEALTH_CHECK_FAILED), $error->data["message"]);
    }

    /** @test */
    public function it_returns_shop_id_not_set_error_when_shop_id_not_set()
    {
        $controller = new SetupController();

        $error = $controller->update([]);

        $this->assertEquals(Error::SHOP_ID_NOT_FOUND, $error->data["code"]);
        $this->assertEquals(Error::getMessage(Error::SHOP_ID_NOT_FOUND), $error->data["message"]);
    }

    /** @test */
    public function it_returns_error_when_shop_not_found()
    {
        /** @var MockObject|PiggyApi $mockedPiggyApi */
        $mockedPiggyApi = $this->createMock(PiggyApi::class);
        $mockedPiggyClient = $this->createMock(Client::class);
        $mockedWebshopsResource = $this->createMock(WebshopsResource::class);
        $mockedPiggyClient->webshops = $mockedWebshopsResource;
        $mockedPiggyApi->client = $mockedPiggyClient;

        $controller = new SetupController();
        $controller->setPiggyApi($mockedPiggyApi);

        $mockedWebshopsResource->expects($this->once())->method('show')->willReturn(null);

        $error = $controller->update(['shop_id' => '1']);

        $this->assertEquals(Error::SHOP_NOT_FOUND, $error->data["code"]);
        $this->assertEquals(Error::getMessage(Error::SHOP_NOT_FOUND), $error->data["message"]);
    }

    /** @test */
    public function it_returns_error_when_chosen_shop_is_not_linkted_to_an_loyalty_program()
    {
        /** @var MockObject|PiggyApi $mockedPiggyApi */
        $mockedPiggyApi = $this->createMock(PiggyApi::class);
        $mockedPiggyClient = $this->createMock(Client::class);
        $mockedWebshopsResource = $this->createMock(WebshopsResource::class);
        $mockedPiggyClient->webshops = $mockedWebshopsResource;
        $mockedPiggyApi->client = $mockedPiggyClient;

        $controller = new SetupController();
        $controller->setPiggyApi($mockedPiggyApi);

        $webshop1 = new Webshop();
        $webshop1->setId(1);
        $webshop1->setName("webshop1");

        $mockedWebshopsResource->expects($this->once())->method('show')->willReturn($webshop1);

        $error = $controller->update(['shop_id' => $webshop1->getId()]);

        $this->assertEquals(Error::SHOP_NOT_LINKED_TO_LOYALTY_PROGRAM, $error->data["code"]);
        $this->assertEquals(Error::getMessage(Error::SHOP_NOT_LINKED_TO_LOYALTY_PROGRAM), $error->data["message"]);
    }

    /** @test */
    public function it_sets_the_shop_id_and_returns_the_redirect_url()
    {
        /** @var MockObject|PiggyApi $mockedPiggyApi */
        $mockedPiggyApi = $this->createMock(PiggyApi::class);
        $mockedPiggyClient = $this->createMock(Client::class);
        $mockedWebshopsResource = $this->createMock(WebshopsResource::class);
        $mockedPiggyClient->webshops = $mockedWebshopsResource;
        $mockedPiggyApi->client = $mockedPiggyClient;

        $controller = new SetupController();
        $controller->setPiggyApi($mockedPiggyApi);

        $loyaltyProgram = new LoyaltyProgram();
        $loyaltyProgram->setId(1);
        $loyaltyProgram->setName('loyaltyProgram');

        $webshop1 = new Webshop();
        $webshop1->setId(1);
        $webshop1->setName("webshop1");
        $webshop1->setLoyaltyProgram($loyaltyProgram);

        $mockedWebshopsResource->expects($this->once())->method('show')->willReturn($webshop1);

        $redirectUrl = $controller->update(['shop_id' => $webshop1->getId()]);

        $this->assertEquals($redirectUrl, $controller->response([
            "redirect_url" => admin_url("/admin.php?page=pfw-admin")
        ]));

        delete_option(PFW_SLUG . '_shop_id');
    }

    /** @test */
    public function it_deletes_the_client_token_and_shop_id_and_returns_the_redirect_url()
    {
        update_option( PFW_SLUG . '_client_token', "1");
        update_option(PFW_SLUG . '_shop_id', "1");

        $controller = new SetupController();
        $redirectUrl = $controller->delete();

        $this->assertEmpty(get_option(PFW_SLUG . '_client_token'));
        $this->assertEmpty(get_option(PFW_SLUG . '_shop_id'));

        $this->assertEquals($redirectUrl, $controller->response([
            "redirect_url" => admin_url("/admin.php?page=pfw-setup")
        ]));
    }

    /** @test */
    public function it_writes_to_the_logging_file()
    {
        /** @var MockObject|Logger $mockedLogger */
        $mockedLogger = $this->createMock(Logger::class);

        $controller = new SetupController();
        $controller->setLogger($mockedLogger);

        $message = "Deleted configuration";
        $mockedLogger->expects($this->once())->method('info')->with($message);

        $controller->delete();
    }
}