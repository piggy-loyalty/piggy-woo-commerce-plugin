<?php

use PFW\Piggy\Error;
use PFW\Piggy\Controllers\StagedCreditReceptionsController;
use PFW\Piggy\Services\StagedCreditReceptionsService;
use PHPUnit\Framework\MockObject\MockObject;
use PFW\Container\Piggy\Api\Http\Client;
use PFW\Container\Piggy\Api\Model\StagedCreditReception;
use PFW\Container\Piggy\Api\PiggyApi;
use PFW\Container\Piggy\Api\Resources\StagedCreditReceptionsResource;
use Test\PiggyTestCase;

/**
 * Class StagedCreditReceptionControllerTest
 */
class StagedCreditReceptionControllerTest extends PiggyTestCase
{
    public function setUp()
    {
        parent::setUp();

        update_option(PFW_SLUG . "_client_token", "1");
        update_option(PFW_SLUG . "_shop_id", "1");
    }

    /**
     * @test
     */
    public function it_returns_error_if_email_not_set()
    {
        $controller = new StagedCreditReceptionsController();

        $error = $controller->send([
            "email" => null,
            "purchase_amount" => 10,
        ]);

        $this->assertEquals(Error::INVALID_INPUT, $error->data["code"]);
        $this->assertEquals(Error::getMessage(Error::INVALID_INPUT), $error->data["message"]);
    }

    /**
     * @test
     */
    public function it_returns_an_error_if_purchase_amount_not_set()
    {
        $controller = new StagedCreditReceptionsController();

        $error = $controller->send([
            "email" => "peter@piggy.nl",
            "purchase_amount" => null,
        ]);

        $this->assertEquals(Error::INVALID_INPUT, $error->data["code"]);
        $this->assertEquals(Error::getMessage(Error::INVALID_INPUT), $error->data["message"]);
    }

    /** @test */
    public function it_calls_the_staged_credit_reception_service_to_create_staged_credit_receptions()
    {
        /** @var PiggyApi|MockObject $mockedPiggyApi */
        $mockedPiggyApi = $this->createMock(PiggyApi::class);
        $mockedPiggyClient = $this->createMock(Client::class);
        $mockedPiggyApi->client = $mockedPiggyClient;
        $mockedStagedCreditReceptionsService = $this->createMock(StagedCreditReceptionsService::class);
        $mockedPiggyClient->stagedCreditReceptions = $mockedStagedCreditReceptionsService;

        $email = "peter@piggy.nl";
        $purchaseAmount = "200";

        $controller = new StagedCreditReceptionsController();
        /** @var StagedCreditReceptionsService|MockObject $mockedStagedCreditReceptionsService */
        $controller->setStagedCreditReceptionService($mockedStagedCreditReceptionsService);

        $staged = new StagedCreditReception();
        $staged->setHash("1");
        $staged->setCredits("10");

        $mockedStagedCreditReceptionsService->expects($this->once())->method('handle')->willReturn($staged);

        $actual = $controller->send(["email" => $email, "purchase_amount" => $purchaseAmount]);

        $this->assertEquals($controller->response([
            "email" => $email,
            "staged_credit_reception" => [
                "hash" => $staged->getHash(),
                "credits" => $staged->getCredits(),
            ]
        ]), $actual);
    }
}