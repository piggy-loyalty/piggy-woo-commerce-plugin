<?php

use PFW\Piggy\Controllers\CustomerController;
use PFW\Piggy\Error;
use PFW\Piggy\Models\OAuthToken;
use PFW\Piggy\Repositories\OAuthTokenRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PFW\Container\Piggy\Api\Model\Customer;
use PFW\Container\Piggy\Api\PiggyApi;
use PFW\Container\Piggy\Api\Resources\CreditBalancesResource;
use Test\PiggyTestCase;

class CustomerControllerTest extends  PiggyTestCase
{
    /** @test */
    public function it_returns_error_when_user_not_linked()
    {
        $controller = new CustomerController();

        /** @var MockObject|OAuthTokenRepository $mockedOAuthTokenRepository */
        $mockedOAuthTokenRepository = $this->createMock(OAuthTokenRepository::class);
        $mockedOAuthTokenRepository->expects($this->once())->method('findOneBy')->willReturn(false);
        $controller->setOauthTokenRepository($mockedOAuthTokenRepository);

        $error = $controller->show();

        $this->assertEquals(Error::USER_NOT_LINKED, $error->data["code"]);
        $this->assertEquals(Error::getMessage(Error::USER_NOT_LINKED), $error->data["message"]);
    }

    /** @test */
    public function it_returns_customer_and_credit_balance()
    {
        $controller = new CustomerController();

        $customer = new Customer();
        $customer->setId(1);
        $customer->setEmail("email");
        $customer->setFirstName("peter");
        $customer->setLastName("jan");

        $OAuthToken = new OAuthToken();
        $OAuthToken->setAccessToken('1');

        /** @var MockObject|OAuthTokenRepository $mockedOAuthTokenRepository */
        $mockedOAuthTokenRepository = $this->createMock(OAuthTokenRepository::class);
        $mockedOAuthTokenRepository->expects($this->once())->method('findOneBy')->willReturn($OAuthToken);
        $controller->setOauthTokenRepository($mockedOAuthTokenRepository);

        /** @var MockObject|PiggyApi $mockedPiggyApi */
        $mockedPiggyApi = $this->createMock(PiggyApi::class);

        $mockedPiggyHttpCustomer = $this->createMock(\Piggy\Api\Http\Customer::class);
        $mockedPiggyHttpCustomer->expects($this->once())->method('profile')->willReturn($customer);

        $mockedCreditBalancesResource = $this->createMock(CreditBalancesResource::class);
        $mockedCreditBalancesResource->expects($this->once())->method('show')->willReturn('2');

        $mockedPiggyHttpCustomer->creditBalances = $mockedCreditBalancesResource;
        $mockedPiggyApi->customer = $mockedPiggyHttpCustomer;

        $controller->setPiggyApi($mockedPiggyApi);
        $response = $controller->show();

        $this->assertEquals($controller->response([
            'customer' => [
                "id" => $customer->getId(),
                "email" => $customer->getEmail(),
                "first_name" => $customer->getFirstName(),
                "last_name" => $customer->getLastName()
            ],
            'credit_balance' => '2',
        ]), $response);
    }
}