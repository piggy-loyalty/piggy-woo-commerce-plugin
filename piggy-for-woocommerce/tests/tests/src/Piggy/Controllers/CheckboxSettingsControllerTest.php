<?php

use PFW\Piggy\Controllers\CheckboxSettingsController;
use PFW\Piggy\Error;
use Test\PiggyTestCase;

class CheckboxSettingsControllerTest extends PiggyTestCase {

    /** @test */
    public function it_returns_checkbox_settings()
    {
        $stagedCheckboxText = "Yes I would like to save points.";
        $stagedCheckboxUrl = "https://piggy.eu/";
        $stagedCheckboxUrlText = "More information";

        update_option(PFW_SLUG . "_staged_checkbox_text", $stagedCheckboxText);
        update_option(PFW_SLUG . "_staged_checkbox_url", $stagedCheckboxUrl);
        update_option(PFW_SLUG . "_staged_checkbox_url_text", $stagedCheckboxUrlText);

        $controller = new CheckboxSettingsController();

        $response = $controller->index();

        $this->assertEquals($response, $controller->response([
            "staged_checkbox_text" => $stagedCheckboxText,
            "staged_checkbox_url" => $stagedCheckboxUrl,
            "staged_checkbox_url_text" => $stagedCheckboxUrlText
        ]));
    }

    /** @test */
    public function it_sets_default_text_when_checkbox_text_not_set()
    {
        $stagedCheckboxUrl = "https://piggy.eu/";
        $stagedCheckboxUrlText = "More information";

        update_option(PFW_SLUG . "_staged_checkbox_url", $stagedCheckboxUrl);
        update_option(PFW_SLUG . "_staged_checkbox_url_text", $stagedCheckboxUrlText);

        $controller = new CheckboxSettingsController();

        $response = $controller->index();

        $this->assertEquals($response, $controller->response([
            "staged_checkbox_text" => "Ja, ik wil graag punten sparen.",
            "staged_checkbox_url" => $stagedCheckboxUrl,
            "staged_checkbox_url_text" => $stagedCheckboxUrlText
        ]));
    }

    /** @test */
    public function it_returns_error_when_input_is_invalid()
    {
        $controller = new CheckboxSettingsController();
        $error = $controller->update([]);

        $this->assertEquals(Error::INVALID_INPUT, $error->data["code"]);
        $this->assertEquals(Error::getMessage(Error::INVALID_INPUT), $error->data["message"]);
    }

    /** @test */
    public function it_updates_the_flow_option_and_settings()
    {
        $controller = new CheckboxSettingsController();

        update_option(PFW_SLUG . "_staged_checkbox_text", "Yes I would like to save points.");
        update_option(PFW_SLUG . "_staged_checkbox_url", "https://piggy.eu/");
        update_option(PFW_SLUG . "_staged_checkbox_url_text", "More information");

        $stagedCheckboxText = "Ja ik wil graag punten sparen.";
        $stagedCheckboxUrl = "https://piggy.eu/solutions/member-app";
        $stagedCheckboxUrlText = "Meer informatie";

        $response = $controller->update([
            "staged_checkbox_text" => $stagedCheckboxText,
            "staged_checkbox_url" => $stagedCheckboxUrl,
            "staged_checkbox_url_text" => $stagedCheckboxUrlText
        ]);

        $this->assertEquals($controller->response([
            "staged_checkbox_text" => $stagedCheckboxText,
            "staged_checkbox_url" => $stagedCheckboxUrl,
            "staged_checkbox_url_text" => $stagedCheckboxUrlText
        ]), $response);

    }
}