<?php

use PFW\Piggy\Error;
use PFW\Piggy\Controllers\WebshopsController;
use PHPUnit\Framework\MockObject\MockObject;
use PFW\Container\Piggy\Api\Http\Client;
use PFW\Container\Piggy\Api\Mappers\WebshopMapper;
use PFW\Container\Piggy\Api\Mappers\WebshopsMapper;
use PFW\Container\Piggy\Api\Model\Webshop;
use PFW\Container\Piggy\Api\PiggyApi;
use PFW\Container\Piggy\Api\Resources\WebshopsResource;
use Test\PiggyTestCase;

class WebshopsControllerTest extends PiggyTestCase
{
    /** @test */
    public function it_returns_multiple_webshops()
    {
        /** @var MockObject|PiggyApi $mockedPiggyApi */
        $mockedPiggyApi = $this->createMock(PiggyApi::class);
        $mockedPiggyClient = $this->createMock(Client::class);
        $mockedPiggyApi->client = $mockedPiggyClient;
        $mockedWebshopsResource = $this->createMock(WebshopsResource::class);
        $mockedPiggyClient->webshops = $mockedWebshopsResource;

        $controller = new WebshopsController();
        $controller->setPiggyApi($mockedPiggyApi);

        $webshop1 = new Webshop();
        $webshop1->setId(1);
        $webshop1->setName("webshop1");

        $webshop2 = new Webshop();
        $webshop2->setId(2);
        $webshop2->setName("webshop2");

        $mockedWebshopsResource->expects($this->once())->method("index")->willReturn([$webshop1, $webshop2]);

        $webshopsMapper = new WebshopsMapper();

        $this->assertEquals($controller->index(), $controller->response($webshopsMapper->mapToResponse([$webshop1, $webshop2])));
    }

    /** @test */
    public function it_returns_an_error_if_shop_id_not_found()
    {
        $controller = new WebshopsController();

        $error = $controller->show();
        $this->assertEquals(Error::SHOP_ID_NOT_FOUND, $error->data["code"]);
        $this->assertEquals(Error::getMessage(Error::SHOP_ID_NOT_FOUND), $error->data["message"]);
    }

    /** @test */
    public function it_returns_a_webshop()
    {
        update_option(PFW_SLUG . '_shop_id', 1);
        /** @var MockObject|PiggyApi $mockedPiggyApi */
        $mockedPiggyApi = $this->createMock(PiggyApi::class);
        $mockedPiggyClient = $this->createMock(Client::class);
        $mockedPiggyApi->client = $mockedPiggyClient;
        $mockedWebshopsResource = $this->createMock(WebshopsResource::class);
        $mockedPiggyClient->webshops = $mockedWebshopsResource;

        $controller = new WebshopsController();
        $controller->setPiggyApi($mockedPiggyApi);

        $webshop = new Webshop();
        $webshop->setId(1);
        $webshop->setName('webshop');

        $mockedWebshopsResource->expects($this->once())->method(    "show")->willReturn($webshop);

        $webshopMapper = new WebshopMapper();

        $this->assertEquals($controller->show(), $controller->response($webshopMapper->mapToResponse($webshop)));

        delete_option(PFW_SLUG . '_shop_id');
    }
}