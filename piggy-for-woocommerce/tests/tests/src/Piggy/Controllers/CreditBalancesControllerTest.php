<?php

use PFW\Piggy\Controllers\CreditBalancesController;
use PFW\Piggy\Error;
use PFW\Piggy\Models\OAuthToken;
use PFW\Piggy\Repositories\OAuthTokenRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PFW\Container\Piggy\Api\Customer\PiggyCustomers;
use PFW\Container\Piggy\Api\Http\Customer;
use PFW\Container\Piggy\Api\PiggyApi;
use PFW\Container\Piggy\Api\Resources\CreditBalancesResource;
use PFW\Container\Piggy\Api\Resources\Customer\CustomerResource;
use Test\PiggyTestCase;

class CreditBalancesControllerTest extends PiggyTestCase
{
    /** @test */
    public function it_returns_error_when_user_not_linked()
    {
        $controller = new CreditBalancesController();

        /** @var MockObject|OAuthTokenRepository $mockedOAuthTokenRepository */
        $mockedOAuthTokenRepository = $this->createMock(OAuthTokenRepository::class);
        $mockedOAuthTokenRepository->expects($this->once())->method('findOneBy')->willReturn(false);
        $controller->setOauthTokenRepository($mockedOAuthTokenRepository);

        $error = $controller->show();

        $this->assertEquals(Error::USER_NOT_LINKED, $error->data["code"]);
        $this->assertEquals(Error::getMessage(Error::USER_NOT_LINKED), $error->data["message"]);
    }

    /** @test */
    public function it_returns_credit_balances_for_piggy_customer()
    {
        $controller = new CreditBalancesController();

        $OAuthToken = new OAuthToken();
        $OAuthToken->setAccessToken('1');

        /** @var MockObject|OAuthTokenRepository $mockedOAuthTokenRepository */
        $mockedOAuthTokenRepository = $this->createMock(OAuthTokenRepository::class);
        $mockedOAuthTokenRepository->expects($this->once())->method('findOneBy')->willReturn($OAuthToken);
        $controller->setOauthTokenRepository($mockedOAuthTokenRepository);

        /** @var MockObject|PiggyApi $mockedPiggyApi */
        $mockedPiggyApi = $this->createMock(PiggyApi::class);
        $mockedPiggyHttpCustomer = $this->createMock(Customer::class);
        $mockedPiggyApi->customer = $mockedPiggyHttpCustomer;
        $mockedCreditBalancesResource = $this->createMock(CreditBalancesResource::class);
        $mockedCreditBalancesResource->expects($this->once())->method('show')->willReturn('2');
        $mockedPiggyHttpCustomer->creditBalances = $mockedCreditBalancesResource;

        $controller->setPiggyApi($mockedPiggyApi);

        $actual = $controller->show();

        $this->assertEquals($controller->response([
            'credit_balance' => '2',
        ]), $actual);
    }
}