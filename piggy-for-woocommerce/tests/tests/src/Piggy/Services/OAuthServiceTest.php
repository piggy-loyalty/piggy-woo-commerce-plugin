<?php

use PFW\Piggy\Models\OAuthToken;
use PFW\Piggy\Repositories\OAuthTokenRepository;
use PFW\Piggy\Services\OAuthService;
use PHPUnit\Framework\MockObject\MockObject;
use PFW\Container\Piggy\Api\Http\Customer;
use PFW\Container\Piggy\Api\PiggyApi;
use Test\PiggyTestCase;

class OAuthServiceTest extends PiggyTestCase
{

    public function setUp()
    {
        wp_set_current_user(1);

        parent::setUp();
    }

    /** @test */
    public function it_returns_authentication_url()
    {
        $service = new OAuthService();

        /** @var PiggyApi $mockedPiggyApi */
        $mockedPiggyApi = $this->createMock(PiggyApi::class);
        $mockedPiggyHttpCustomer = $this->createMock(Customer::class);
        $mockedPiggyHttpCustomer->expects($this->once())->method("generateAuthenticationUrl")->willReturn("http://localhost/");
        $mockedPiggyApi->customer = $mockedPiggyHttpCustomer;

        $service->setPiggyApi($mockedPiggyApi);

        $this->assertEquals("http://localhost/", $service->generateAuthenticationUri());
    }

    /** @test
     * @throws Exception
     */
    public function it_removes_oauth_token_of_existing_user()
    {
        $service = new OAuthService();

        $oauthToken = new OAuthToken();
        $oauthToken->setAccessToken("123");
        $oauthToken->setRefreshToken("1234");
        $oauthToken->setExpiresIn("12345");

        /** @var OAuthTokenRepository|MockObject $mockedOAuthTokenRepository */
        $mockedOAuthTokenRepository = $this->createMock(OAuthTokenRepository::class);
        $mockedOAuthTokenRepository->expects($this->once())
            ->method('findOneBy')->willReturn($oauthToken);
        $mockedOAuthTokenRepository->expects($this->once())
            ->method('remove')->willReturn(null);
        $mockedOAuthTokenRepository->expects($this->once())
            ->method('save')->willReturn(false);
        $service->setOauthTokenRepository($mockedOAuthTokenRepository);

        /** @var PiggyApi $mockedPiggyApi */
        $mockedPiggyApi = $this->createMock(PiggyApi::class);
        $mockedPiggyHttpCustomer = $this->createMock(Customer::class);
        $mockedPiggyHttpCustomer->expects($this->once())->method("requestAuthenticationToken")->willReturn($oauthToken);
        $mockedPiggyApi->customer = $mockedPiggyHttpCustomer;

        $service->setPiggyApi($mockedPiggyApi);

        $actual = $service->authenticate("123");

        $this->assertNull($actual);
    }

    /** @test
     * @throws Exception
     */
    public function it_returns_error_when_requesting_oauth_token_failed()
    {
        $service = new OAuthService();

        /** @var OAuthTokenRepository|MockObject $mockedOAuthTokenRepository */
        $mockedOAuthTokenRepository = $this->createMock(OAuthTokenRepository::class);
        $mockedOAuthTokenRepository->expects($this->once())->method('findOneBy')->willReturn(null);

        $service->setOauthTokenRepository($mockedOAuthTokenRepository);

        /** @var PiggyApi $mockedPiggyApi */
        $mockedPiggyApi = $this->createMock(PiggyApi::class);
        $mockedPiggyHttpCustomer = $this->createMock(Customer::class);
        $mockedPiggyHttpCustomer->expects($this->once())->method("requestAuthenticationToken")->willReturn(null);
        $mockedPiggyApi->customer = $mockedPiggyHttpCustomer;

        $service->setPiggyApi($mockedPiggyApi);

        $error = $service->authenticate("123");

        $this->assertWPError($error);
    }

    /** @test
     * @throws Exception
     */
    public function it_returns_null_when_oauth_token_could_not_be_saved_in_db()
    {
        $service = new OAuthService();

        $oauthToken = new OAuthToken();
        $oauthToken->setAccessToken("123");
        $oauthToken->setRefreshToken("1234");
        $oauthToken->setExpiresIn("12345");

        /** @var OAuthTokenRepository|MockObject $mockedOAuthTokenRepository */
        $mockedOAuthTokenRepository = $this->createMock(OAuthTokenRepository::class);
        $mockedOAuthTokenRepository->expects($this->once())->method('findOneBy')->willReturn(null);
        $mockedOAuthTokenRepository->expects($this->once())->method('save')->willReturn(false);

        $service->setOauthTokenRepository($mockedOAuthTokenRepository);

        /** @var PiggyApi $mockedPiggyApi */
        $mockedPiggyApi = $this->createMock(PiggyApi::class);
        $mockedPiggyHttpCustomer = $this->createMock(Customer::class);
        $mockedPiggyHttpCustomer->expects($this->once())->method("requestAuthenticationToken")->willReturn($oauthToken);
        $mockedPiggyApi->customer = $mockedPiggyHttpCustomer;

        $service->setPiggyApi($mockedPiggyApi);

        $actual = $service->authenticate("123");

        $this->assertNull($actual);
    }

    /** @test
     * @throws Exception
     */
    public function it_requests_oauth_token_and_saves_this_in_the_db()
    {
        $service = new OAuthService();

        $oauthToken = new OAuthToken();
        $oauthToken->setAccessToken("123");
        $oauthToken->setRefreshToken("1234");
        $oauthToken->setExpiresIn("12345");

        /** @var OAuthTokenRepository|MockObject $mockedOAuthTokenRepository */
        $mockedOAuthTokenRepository = $this->createMock(OAuthTokenRepository::class);
        $mockedOAuthTokenRepository->expects($this->once())->method('findOneBy')->willReturn(null);
        $mockedOAuthTokenRepository->expects($this->once())->method('save')->willReturn($oauthToken);

        $service->setOauthTokenRepository($mockedOAuthTokenRepository);

        /** @var PiggyApi $mockedPiggyApi */
        $mockedPiggyApi = $this->createMock(PiggyApi::class);
        $mockedPiggyHttpCustomer = $this->createMock(Customer::class);
        $mockedPiggyHttpCustomer->expects($this->once())->method("requestAuthenticationToken")->willReturn($oauthToken);
        $mockedPiggyApi->customer = $mockedPiggyHttpCustomer;

        $service->setPiggyApi($mockedPiggyApi);

        $actual = $service->authenticate("123");

        $this->assertEquals($oauthToken, $actual);
    }

    /** @test */
    public function it_returns_true_when_oauth_token_is_found_in_db()
    {
        $service = new OAuthService();

        $oauthToken = new OAuthToken();
        $oauthToken->setAccessToken("123");
        $oauthToken->setRefreshToken("1234");
        $oauthToken->setExpiresIn("12345");

        /** @var OAuthTokenRepository|MockObject $mockedOAuthTokenRepository */
        $mockedOAuthTokenRepository = $this->createMock(OAuthTokenRepository::class);
        $mockedOAuthTokenRepository->expects($this->once())->method('findOneBy')->willReturn($oauthToken);

        $service->setOauthTokenRepository($mockedOAuthTokenRepository);

        $bool = $service->isUserLinked(1);

        $this->assertTrue($bool);
    }
}
