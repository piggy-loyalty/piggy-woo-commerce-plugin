<?php

use PFW\Piggy\Logger;
use PFW\Piggy\Services\StagedCreditReceptionsService;
use PHPUnit\Framework\MockObject\MockObject;
use PFW\Container\Piggy\Api\Exceptions\PiggyApiException;
use PFW\Container\Piggy\Api\Http\Client;
use PFW\Container\Piggy\Api\Model\StagedCreditReception;
use PFW\Container\Piggy\Api\PiggyApi;
use PFW\Container\Piggy\Api\Resources\StagedCreditReceptionsResource;
use Test\PiggyTestCase;

class StagedCreditReceptionsServiceTest extends PiggyTestCase
{

    /** @test */
    public function it_logs_an_error_when_staged_credit_reception_could_not_be_created()
    {
        $service = new StagedCreditReceptionsService();

        /** @var MockObject|Logger $mockedLogger */
        $mockedLogger = $this->createMock(Logger::class);
        $service->setLogger($mockedLogger);

        /** @var PiggyApi $mockedPiggyApi */
        $mockedPiggyApi = $this->createMock(PiggyApi::class);
        $mockedStagedCreditReceptions = $this->createMock(StagedCreditReceptionsResource::class);
        $mockedStagedCreditReceptions->expects($this->once())->method('create')->willThrowException(new PiggyApiException("something", 100));
        $mockedPiggyHttpClient = $this->createMock(Client::class);
        $mockedPiggyHttpClient->stagedCreditReceptions = $mockedStagedCreditReceptions;
        $mockedPiggyApi->client = $mockedPiggyHttpClient;
        $service->setPiggyApi($mockedPiggyApi);

        $mockedLogger->expects($this->once())->method('error')->with("100, something");

        update_option(PFW_SLUG . '_shop_id', "1");

        $service->handle("peter@test.nl", "1");

        update_option(PFW_SLUG . '_shop_id', "");
    }

    /** @test */
    public function it_writes_to_the_logs_when_creating_and_sending_staged_credit_receptions()
    {
        $service = new StagedCreditReceptionsService();

        $mockedCreditReception = $this->createMock(StagedCreditReception::class);
        $mockedCreditReception->expects($this->exactly(2))->method('getHash')->willReturn('123');

        /** @var MockObject|Logger $mockedLogger */
        $mockedLogger = $this->createMock(Logger::class);
        $service->setLogger($mockedLogger);

        /** @var PiggyApi $mockedPiggyApi */
        $mockedPiggyApi = $this->createMock(PiggyApi::class);
        $mockedStagedCreditReceptions = $this->createMock(StagedCreditReceptionsResource::class);
        $mockedStagedCreditReceptions->expects($this->once())->method('create')->willReturn($mockedCreditReception);
        $mockedPiggyHttpClient = $this->createMock(Client::class);
        $mockedPiggyHttpClient->stagedCreditReceptions = $mockedStagedCreditReceptions;
        $mockedPiggyApi->client = $mockedPiggyHttpClient;
        $service->setPiggyApi($mockedPiggyApi);

        $mockedLogger->expects($this->exactly(3))->method('info');

        update_option(PFW_SLUG . '_shop_id', "1");

        $service->handle("peter@test.nl", "1");

        update_option(PFW_SLUG . '_shop_id', "");
    }

}