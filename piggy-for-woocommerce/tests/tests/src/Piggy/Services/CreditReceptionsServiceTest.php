<?php

use PFW\Piggy\Logger;
use PFW\Piggy\Services\CreditReceptionsService;
use PHPUnit\Framework\MockObject\MockObject;
use PFW\Container\Piggy\Api\Exceptions\PiggyApiException;
use PFW\Container\Piggy\Api\Http\Client;
use PFW\Container\Piggy\Api\Model\CreditReception;
use PFW\Container\Piggy\Api\PiggyApi;
use PFW\Container\Piggy\Api\Resources\CreditReceptionsResource;
use Test\PiggyTestCase;

class CreditReceptionsServiceTest extends PiggyTestCase
{

    /** @test */
    public function it_logs_an_error_when_credit_reception_could_not_be_created()
    {
        $service = new CreditReceptionsService();

        /** @var MockObject|Logger $mockedLogger */
        $mockedLogger = $this->createMock(Logger::class);
        $service->setLogger($mockedLogger);

        /** @var PiggyApi $mockedPiggyApi */
        $mockedPiggyApi = $this->createMock(PiggyApi::class);
        $mockedCreditReceptionsResource = $this->createMock(CreditReceptionsResource::class);
        $mockedCreditReceptionsResource->expects($this->once())->method('create')->willThrowException(new PiggyApiException("something", 100));
        $mockedPiggyHttpClient = $this->createMock(Client::class);
        $mockedPiggyHttpClient->creditReceptions = $mockedCreditReceptionsResource;
        $mockedPiggyApi->client = $mockedPiggyHttpClient;
        $service->setPiggyApi($mockedPiggyApi);

        $mockedLogger->expects($this->once())->method('error')->with("100, something");

        update_option(PFW_SLUG . '_shop_id', "1");

        $service->handle("peter@test.nl", "1");

        update_option(PFW_SLUG . '_shop_id', "");
    }

    /** @test */
    public function it_writes_to_the_logs_when_creating_credit_receptions()
    {
        $service = new CreditReceptionsService();

        $email = "peter@test.nl";

        $mockedCreditReception = $this->createMock(CreditReception::class);
        $mockedCreditReception->expects($this->once())->method('getCredits')->willReturn('1');

        /** @var MockObject|Logger $mockedLogger */
        $mockedLogger = $this->createMock(Logger::class);
        $service->setLogger($mockedLogger);

        /** @var PiggyApi $mockedPiggyApi */
        $mockedPiggyApi = $this->createMock(PiggyApi::class);
        $mockedCreditReceptionsResource = $this->createMock(CreditReceptionsResource::class);
        $mockedCreditReceptionsResource->expects($this->once())->method('create')->willReturn($mockedCreditReception);
        $mockedPiggyHttpClient = $this->createMock(Client::class);
        $mockedPiggyHttpClient->creditReceptions = $mockedCreditReceptionsResource;
        $mockedPiggyApi->client = $mockedPiggyHttpClient;
        $service->setPiggyApi($mockedPiggyApi);

        $mockedLogger->expects($this->once())->method('info');

        update_option(PFW_SLUG . '_shop_id', "1");

        $service->handle($email, "1");

        update_option(PFW_SLUG . '_shop_id', "");
    }
}