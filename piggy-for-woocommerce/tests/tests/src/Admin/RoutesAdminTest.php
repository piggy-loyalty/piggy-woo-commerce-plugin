<?php

use PFW\Includes\Plugin;
use Test\PiggyTestCase;
use Test\RouteTestHelpers;

class RoutesAdminTest extends PiggyTestCase {

    use RouteTestHelpers;

    /**
     * Test REST Server
     * @var WP_REST_Server
     */
    protected $server;

    protected $adminRoute = '/pfw/v1/admin';

    protected $loggingRoute = '/pfw/v1/admin/logs';

    protected $stagedCreditReceptionsRoute = '/pfw/v1/admin/staged-credit-receptions';

    protected $settingsFlowRoute = '/pfw/v1/admin/settings/flow';

    protected $setupRoute = '/pfw/v1/setup';

    public function setUp() {
        parent::setUp();
        /** @var WP_REST_Server $wp_rest_server */
        global $wp_rest_server;
        $this->server = $wp_rest_server = new WP_REST_Server;
        do_action( 'rest_api_init' );

        update_option(PFW_SLUG . "_client_token", "1");
        update_option(PFW_SLUG . "_shop_id", "1");

//      https://wordpress.org/support/topic/notice-register_rest_route-was-called-incorrectly/
        $this->skipForWCVersions(["4.2.1", "4.3.1"]);
    }

    /** @test */
    public function it_registered_the_admin_routes()
    {
        $routes = $this->server->get_routes();
        $this->assertArrayHasKey( $this->adminRoute, $routes );
        $this->assertArrayHasKey( $this->loggingRoute, $routes );
        $this->assertArrayHasKey( $this->stagedCreditReceptionsRoute, $routes );
        $this->assertArrayHasKey( $this->settingsFlowRoute, $routes );
        $this->assertArrayHasKey( $this->setupRoute, $routes );
    }

    /** @test */
    public function it_has_the_right_callback_method_and_permissions_for_admin_show() {
        $callback = "show";
        $permission = "admin_permissions_check";
        $method = WP_REST_Server::READABLE;

        $this->assertTrue($this->has_callback_in_route($this->server, $this->adminRoute, $callback));
        $this->assertTrue($this->callback_has_permission($this->server, $this->adminRoute, $callback, $permission));
        $this->assertTrue($this->callback_has_method($this->server, $this->adminRoute, $callback, $method));
    }

    /** @test */
    public function it_has_the_right_callback_method_and_permissions_for_admin_logs_show() {
        $callback = "index";
        $permission = "admin_permissions_check";
        $method = WP_REST_Server::READABLE;

        $this->assertTrue($this->has_callback_in_route($this->server, $this->loggingRoute, $callback));
        $this->assertTrue($this->callback_has_permission($this->server, $this->loggingRoute, $callback, $permission));
        $this->assertTrue($this->callback_has_method($this->server, $this->loggingRoute, $callback, $method));
    }

    /** @test */
    public function it_has_the_right_callback_method_and_permissions_for_admin_staged_cr_send() {
        $callback = "send";
        $permission = "admin_permissions_check";
        $method = WP_REST_Server::CREATABLE;

        $this->assertTrue($this->has_callback_in_route($this->server, $this->stagedCreditReceptionsRoute, $callback));
        $this->assertTrue($this->callback_has_permission($this->server, $this->stagedCreditReceptionsRoute, $callback, $permission));
        $this->assertTrue($this->callback_has_method($this->server, $this->stagedCreditReceptionsRoute, $callback, $method));
    }

    /** @test */
    public function it_has_the_right_callback_method_and_permissions_for_admin_settings_flow_index() {
        $callback = "index";
        $permission = "admin_permissions_check";
        $method = WP_REST_Server::READABLE;

        $this->assertTrue($this->has_callback_in_route($this->server, $this->settingsFlowRoute, $callback));
        $this->assertTrue($this->callback_has_permission($this->server, $this->settingsFlowRoute, $callback, $permission));
        $this->assertTrue($this->callback_has_method($this->server, $this->settingsFlowRoute, $callback, $method));
    }

    /** @test */
    public function it_has_the_right_callback_method_and_permissions_for_admin_settings_flow_update() {
        $callback = "update";
        $permission = "admin_permissions_check";
        $method = WP_REST_Server::CREATABLE;
        $endpointUrl = $this->settingsFlowRoute . "/update";

        $this->assertTrue($this->has_callback_in_route($this->server, $endpointUrl, $callback));
        $this->assertTrue($this->callback_has_permission($this->server, $endpointUrl, $callback, $permission));
        $this->assertTrue($this->callback_has_method($this->server, $endpointUrl, $callback, $method));
    }

    /** @test */
    public function it_has_the_right_callback_method_and_permissions_for_setup_ping() {
        $callback = "ping";
        $permission = "setup_permission_check";
        $method = WP_REST_Server::READABLE;

        $this->assertTrue($this->has_callback_in_route($this->server, $this->setupRoute, $callback));
        $this->assertTrue($this->callback_has_permission($this->server, $this->setupRoute, $callback, $permission));
        $this->assertTrue($this->callback_has_method($this->server, $this->setupRoute, $callback, $method));
    }

    /** @test */
    public function it_has_the_right_callback_method_and_permissions_for_setup_authenticate() {
        $callback = "authenticate";
        $permission = "setup_permission_check";
        $method = WP_REST_Server::CREATABLE;
        $endpointUrl = $this->setupRoute . "/authenticate";

        $this->assertTrue($this->has_callback_in_route($this->server, $endpointUrl, $callback));
        $this->assertTrue($this->callback_has_permission($this->server, $endpointUrl, $callback, $permission));
        $this->assertTrue($this->callback_has_method($this->server, $endpointUrl, $callback, $method));
    }

    /** @test */
    public function it_has_the_right_callback_method_and_permissions_for_setup_delete() {
        $callback = "delete";
        $permission = "delete_configuration_permissions_check";
        $method = WP_REST_Server::CREATABLE;
        $endpointUrl = $this->setupRoute . "/delete";

        $this->assertTrue($this->has_callback_in_route($this->server, $endpointUrl, $callback));
        $this->assertTrue($this->callback_has_permission($this->server, $endpointUrl, $callback, $permission));
        $this->assertTrue($this->callback_has_method($this->server, $endpointUrl, $callback, $method));
    }

    /** @test */
    public function it_has_the_right_callback_method_and_permissions_for_setup_update() {
        $callback = "update";
        $permission = "setup_permission_check";
        $method = WP_REST_Server::CREATABLE;
        $endpointUrl = $this->setupRoute . "/update";

        $this->assertTrue($this->has_callback_in_route($this->server, $endpointUrl, $callback));
        $this->assertTrue($this->callback_has_permission($this->server, $endpointUrl, $callback, $permission));
        $this->assertTrue($this->callback_has_method($this->server, $endpointUrl, $callback, $method));
    }

    /** @test */
    public function it_returns_false_if_user_is_not_administrator()
    {
        $plugin = new Plugin();
        $adminRoutes = $plugin->getAdminRoutes();

        $user_id = $this->factory->user->create();
        $user = wp_set_current_user( $user_id );

        $this->assertFalse($adminRoutes->admin_permissions_check());
    }

    /** @test */
    public function it_returns_true_if_plugin_is_configured_and_user_is_administrator()
    {
        $plugin = new Plugin();
        $adminRoutes = $plugin->getAdminRoutes();

        $user_id = $this->factory->user->create( array( 'role' => 'administrator' ) );
        $user = wp_set_current_user( $user_id );
        set_current_screen( 'edit-post' );

        $this->assertTrue($adminRoutes->admin_permissions_check());
    }
}