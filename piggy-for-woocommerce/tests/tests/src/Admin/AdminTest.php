<?php

use PFW\Admin\Admin;
use PFW\Includes\Plugin;
use Test\PiggyTestCase;

class AdminTest extends PiggyTestCase {

    public function setUp()
    {
        parent::setUp();

        update_option(PFW_SLUG . "_client_token", "1");
        update_option(PFW_SLUG . "_shop_id", "1");
    }

    /** @test */
    public function it_loads_the_script_files_to_the_queue()
    {
        $plugin = new Plugin();
        $admin = $plugin->getAdmin();

        $scriptName = PFW_SLUG . "-admin-script";
        $status = "queue";

        $admin->enqueue_scripts();

        $this->assertTrue(wp_script_is( $scriptName, $status));

        wp_dequeue_script($scriptName);
    }

    /** @test */
    public function it_creates_admin_page()
    {
        $this->assertNull($GLOBALS['admin_page_hooks']['pfw-admin']);

        $plugin = new Plugin();
        $page = $plugin->getAdmin();
        $page->add_to_menu();

        $this->assertNotNull($GLOBALS['admin_page_hooks']['pfw-admin']);
    }

    /** @test */
    public function it_returns_null_if_plugin_is_configured()
    {
        $plugin = new Plugin();

        delete_option(PFW_SLUG . "_client_token");
        delete_option(PFW_SLUG . "_shop_id");

        $this->assertEquals($plugin->getAdmin()->add_to_menu(), null);
    }

    /** @test */
    public function component_id_returns_admin_when_plugin_is_configured()
    {
        $plugin = new Plugin();
        $admin = $plugin->getAdmin();

        $this->assertEquals("pfw-admin", $admin->getComponentId());
        $this->assertEquals("admin.js", $admin->getScript());
    }

    /** @test */
    public function component_id_returns_setup_when_plugin_is_not_configured()
    {
        delete_option(PFW_SLUG . "_client_token");
        delete_option(PFW_SLUG . "_shop_id");

        $plugin = new Plugin();
        $admin = $plugin->getAdmin();

        $this->assertEquals("pfw-setup", $admin->getComponentId());
        $this->assertEquals("setup.js", $admin->getScript());
    }

    /** @test */
    public function it_returns_the_setup_page_if_plugin_not_configured()
    {
        $plugin = new Plugin();
        $page = $plugin->getAdmin();

        $this->assertEquals($plugin->getAdmin()->add_to_menu(), $page->add_to_menu());

        delete_option(PFW_SLUG . "_client_token");
        delete_option(PFW_SLUG . "_shop_id");

        $plugin = new Plugin();

        $this->assertEquals($plugin->getAdmin()->add_to_menu(), null);
    }

    /** @test */
    public function it_returns_a_div_for_the_frontend_to_be_loaded()
    {
        $adminPage = new Admin();

        ob_start();
        $adminPage->render_component_library();
        $result = ob_get_contents();
        ob_end_clean();

        $componentId = PFW_SLUG . '-admin';
        $expected = '<div id="' . $componentId . '" class="wrap"></div>';

        $this->assertSame( trim( $expected ), trim( $result ) );
    }

    public function tearDown()
    {
        parent::tearDown();

        delete_option(PFW_SLUG . "_client_token");
        delete_option(PFW_SLUG . "_shop_id");
    }
}