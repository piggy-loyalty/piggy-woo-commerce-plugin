<?php

use PFW\Includes\Plugin;
use PFW\Piggy\Services\OAuthService;
use PHPUnit\Framework\MockObject\MockObject;
use Test\PiggyTestCase;

class BaseTest extends PiggyTestCase
{

    /** @test */
    public function it_returns_true_if_the_plugin_is_configured()
    {
        $plugin = new Plugin();
        $base = $plugin->getBase();

        update_option(PFW_SLUG . "_client_token", "1");
        update_option(PFW_SLUG . "_shop_id", "1");

        $this->assertTrue($base->pluginIsConfigured());
    }

//    /** @test */
//    public function it_enqueues_vendor_scripts_when_user_is_admin()
//    {
//        global $current_screen;
//        $plugin = new Plugin();
//        $base = $plugin->getBase();
//
//        $scriptName = PFW_SLUG . '-vendor-script';
//        $status = "queue";
//
//        $user_id = $this->factory->user->create( array( 'role' => 'administrator' ) );
//        $user = wp_set_current_user( $user_id );
//
//        $screen = WP_Screen::get('pfw-admin');
//        var_dump($screen);
//
//        $base->enqueue_scripts();
//
//        $this->assertTrue(wp_script_is( $scriptName, $status));
//
//        wp_dequeue_script($scriptName);
//        $current_screen = null;
//    }

//    /** @test */
//    public function it_enqueues_vendor_scripts_when_user_is_on_checkout_page()
//    {
//
//    }
//
//    /** @test */
//    public function it_enqueues_vendor_scripts_when_user_is_on_account_page()
//    {
//
//    }

//
//    /** @test */
//    public function it_enqueues_styles_for_base_class_with_request_uri_condition()
//    {
//        $plugin = new Plugin();
//        $base = $plugin->getBase();
//
//        $scriptName = PFW_SLUG . '-style';
//        $status = "queue";
//
//        $_SERVER['REQUEST_URI'] = "/my-account/piggy-account/";
//
//        $base->enqueue_styles();
//
//        $this->assertTrue(wp_style_is( $scriptName, $status));
//
//        $_SERVER['REQUEST_URI'] = "";
//        wp_dequeue_style($scriptName);
//    }

    /** @test */
    public function it_returns_true_when_user_is_logged_in_and_user_flow_option_is_optional()
    {
        $plugin = new Plugin();
        $base = $plugin->getBase();

        $bool = $base->shouldAddStagedWidget();
        $this->assertFalse($bool);

        wp_set_current_user( 1 );

        $bool = $base->shouldAddStagedWidget();
        $this->assertFalse($bool);

        update_option(PFW_SLUG . "_user_flow_option", "optional");

        $bool = $base->shouldAddStagedWidget();
        $this->assertTrue($bool);

        update_option(PFW_SLUG . "_user_flow_option", "");
    }

    /** @test */
    public function it_returns_true_when_guest_flow_option_is_optional()
    {
        $plugin = new Plugin();
        $base = $plugin->getBase();

        $bool = $base->shouldAddStagedWidget();
        $this->assertFalse($bool);

        update_option(PFW_SLUG . "_guest_flow_option", "optional");

        $bool = $base->shouldAddStagedWidget();
        $this->assertTrue($bool);

        update_option(PFW_SLUG . "_guest_flow_option", "");
    }

    /** @test */
    public function it_returns_true_when_linking_account_is_allowed_and_user_is_linked()
    {
        global $wpdb;
        $wpdb->suppress_errors(true);

        $plugin = new Plugin();
        $base = $plugin->getBase();

        $bool = $base->shouldAddOAuthWidget();
        $this->assertFalse($bool);

        update_option(PFW_SLUG . "_allow_linking", "true");

        $bool = $base->shouldAddOAuthWidget();
        $this->assertFalse($bool);

        /** @var OAuthService|MockObject $mockedAuthService */
        $mockedAuthService = $this->createMock(OAuthService::class);
        $mockedAuthService->expects($this->once())->method('isUserLinked')->willReturn(true);
        $base->setAuthService($mockedAuthService);

        $bool = $base->shouldAddOAuthWidget();
        $this->assertFalse($bool);

        wp_set_current_user( 1 );

        $bool = $base->shouldAddOAuthWidget();
        $this->assertTrue($bool);

        $wpdb->suppress_errors(false);

        update_option(PFW_SLUG . "_allow_linking", "");
    }
}