<?php

use PFW\Includes\Core\Activate;
use PFW\Includes\Core\ActivationService;
use Test\PiggyTestCase;

class ActivateTest extends PiggyTestCase
{

    /** @test */
    public function it_creates_migrations_table()
    {
        global $wpdb;

        $table_name = $wpdb->prefix . PFW_SLUG . "_migrations";

        $activationService = new ActivationService();
        $activationService->initMigrationsDb();

        $wpdb->insert($table_name, [
            "version" => "1234"
        ]);

        $result = $wpdb->get_row("SELECT * FROM {$table_name}");

        $this->assertEquals("1234", $result->version);
    }

//    /** @test */
//    public function it_inserts_the_migrations_into_the_migrations_table()
//    {
//        global $wpdb;
//
//        $this->assertEmpty(get_option(PFW_SLUG . "_flow_option"));
//
//        $table_name = $wpdb->prefix . PFW_SLUG . "_migrations";
//
//        $files = scandir(PFW_PLUGIN_DIR . "/includes/migrations");
//        $files = array_slice($files,2);
//
//        $expected = [];
//        foreach($files as $file) {
//            $file = explode(".", $file);
//            $expected[] = $file[0];
//        }
//
//        Activate::init_migrations_db();
//        Activate::run_migrations_up();
//
//        $results = $wpdb->get_results("SELECT * FROM {$table_name}");
//
//        $actual = [];
//        foreach($results as $result) {
//            $actual[] = $result->version;
//        }
//
//        $this->assertNotEmpty(get_option(PFW_SLUG . "_flow_option"));
//        $this->assertEquals($expected, $actual);
//    }
}
