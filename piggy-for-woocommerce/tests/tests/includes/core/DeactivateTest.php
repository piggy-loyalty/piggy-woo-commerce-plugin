<?php

use PFW\Includes\Core\Activate;
use PFW\Includes\Core\ActivationService;
use PFW\Includes\Core\Deactivate;
use PFW\Includes\Core\DeactivationService;
use Test\PiggyTestCase;

class DeactivateTest extends PiggyTestCase {

    /** @test
     * @throws Exception
     */
    public function it_clears_all_tables_for_pfw_plugin()
    {
        global $wpdb;

        $table_name = $wpdb->prefix . PFW_SLUG . "_migrations";

        $activationService = new ActivationService();
        $activationService->activate(); // this creates tables.

        $wpdb->show_errors(false);

        // We insert a row in the database, if the table exists this should return to us the created entities' ID.
        $migrationId = $wpdb->insert($table_name, [
            "version" => "1234"
        ]);

        $this->assertEquals(1, $migrationId);

        // We now deactivate, which should remove all tables. Let's try to insert again to make sure this does not work.
        $deactivationService = new DeactivationService();
        $deactivationService->deactivate();

        // Suppress errors because this will cause a hard error normally but we want it to as well as not clutter our logs / output.
        $wpdb->suppress_errors(true);

        // This returns false if it cannot insert.
        $migrationId = $wpdb->insert($table_name, [
            "version" => "1234"
        ]);

        $wpdb->suppress_errors(false);

        // Assert the the migration id is equal to false, i.e. it has failed to insert.
        $this->assertEquals($migrationId, false);
    }
}