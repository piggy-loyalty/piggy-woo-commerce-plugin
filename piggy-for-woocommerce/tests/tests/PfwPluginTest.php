<?php

use PFW\Includes\Plugin;
use Test\PiggyTestCase;
use Test\Util;

class PfwPluginTest extends PiggyTestCase {

    /**
     * @test
     */
    public function test_constants () {
        $this->assertSame( 'piggy-for-woocommerce', PFW_PLUGIN_NAME);
        $this->assertSame( 'pfw', PFW_SLUG);
        $this->assertSame( 'http://localhost/wp-content/plugins/piggy-for-woocommerce//public', PFW_PUBLIC_PATH);
        $this->assertSame( 'http://localhost/wp-content/plugins/piggy-for-woocommerce//public/dist/', PFW_STYLES_PATH);
        $this->assertSame( 'http://localhost/wp-content/plugins/piggy-for-woocommerce//public/dist/', PFW_SCRIPTS_PATH);
    }

    /**
     * @test
     */
    public function test_pfw_option () {
        $this->assertTrue(get_option('pfw_test'));
    }

    /** @test */
    public function it_loads_all_admin_actions()
    {
        $plugin = new Plugin();
        $admin = $plugin->getAdmin();

        $actions = [
            "add_to_menu" => "admin_menu",
            "enqueue_scripts" => "admin_enqueue_scripts",
            "enqueue_styles" => "admin_enqueue_scripts",
        ];

        foreach ($actions as $function => $action) {
            $this->assertFalse(
                Util::has_action($action, $admin, $function)
            );
        }

        $plugin->run();

        foreach ($actions as $function => $action) {
            $this->assertTrue(
                Util::has_action($action, $admin, $function)
            );
        }
    }

    /** @test */
    public function it_loads_all_admin_routes()
    {
        $plugin = new Plugin();
        $adminRoutes = $plugin->getAdminRoutes();

        $this->assertFalse(
            Util::has_action('rest_api_init', $adminRoutes, 'register_routes')
        );

        $plugin->run();

        $this->assertTrue(
            Util::has_action('rest_api_init', $adminRoutes, 'register_routes')
        );
    }

    /** @test */
    public function it_loads_all_checkout_actions()
    {
        $plugin = new Plugin();
        $checkout = $plugin->getCheckout();

        $actions = [
//            "enqueue_scripts" => "wp_enqueue_scripts",
//            "enqueue_styles" => "wp_enqueue_scripts",
            "add_to_form" => "woocommerce_after_order_notes",
            "save_order_meta" => "woocommerce_checkout_update_order_meta",
            "on_payment_completed" => "woocommerce_payment_complete",
        ];

        foreach ($actions as $function => $action) {
            $this->assertFalse(
                Util::has_action($action, $checkout, $function)
            );
        }

        $plugin->run();

        foreach ($actions as $function => $action) {
            $this->assertTrue(
                Util::has_action($action, $checkout, $function)
            );
        }
    }

    /** @test */
    public function it_loads_all_base_actions()
    {
        $plugin = new Plugin();
        $base = $plugin->getBase();

        $actions = [
            "enqueue_scripts" => "admin_enqueue_scripts",
        ];

        foreach ($actions as $function => $action) {
            $this->assertFalse(
                Util::has_action($action, $base, $function)
            );
        }

        $plugin->run();

        foreach ($actions as $function => $action) {
            $this->assertTrue(
                Util::has_action($action, $base, $function)
            );
        }
    }

    /** @test */
    public function it_loads_all_customer_actions()
    {
        $plugin = new Plugin();
        $customer = $plugin->getCustomer();

        $actions = [
            "enqueue_scripts" => "wp_enqueue_scripts",
            "enqueue_styles" => "wp_enqueue_scripts",
        ];

        foreach ($actions as $function => $action) {
            $this->assertFalse(
                Util::has_action($action, $customer, $function)
            );
        }

        $plugin->run();

        foreach ($actions as $function => $action) {
            $this->assertTrue(
                Util::has_action($action, $customer, $function)
            );
        }
    }

    /** @test */
    public function it_loads_all_customer_routes()
    {
        $plugin = new Plugin();
        $customerRoutes = $plugin->getCustomerRoutes();

        $this->assertFalse(
            Util::has_action('rest_api_init', $customerRoutes, 'register_routes')
        );

        $plugin->run();

        $this->assertTrue(
            Util::has_action('rest_api_init', $customerRoutes, 'register_routes')
        );
    }

    /** @test */
    public function it_loads_all_oauth_routes()
    {
        $plugin = new Plugin();
        $oauthRoutes = $plugin->getOAuthRoutes();

        $this->assertFalse(
            Util::has_action('rest_api_init', $oauthRoutes, 'register_routes')
        );

        $plugin->run();

        $this->assertTrue(
            Util::has_action('rest_api_init', $oauthRoutes, 'register_routes')
        );
    }
}