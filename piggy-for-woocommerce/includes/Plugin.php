<?php

namespace PFW\Includes;
defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

use PFW\Admin\Admin;
use PFW\Admin\Routes as AdminRoutes;
use PFW\Base;
use PFW\Checkout\Checkout;
//use PFW\Checkout\CustomerWidget;
//use PFW\Checkout\StagedCheckbox;
use PFW\Customer\Customer;
use PFW\Customer\Routes as CustomerRoutes;
use PFW\Includes\Core\I18n;
use PFW\Includes\Core\Loader;
use PFW\OAuth\OAuth;
use PFW\OAuth\Routes as OAuthRoutes;

/**
 * Class Plugin
 * @package PFW\Includes
 */
class Plugin
{
    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @var Loader
     */
    protected $loader;

    /**
     * The unique identifier of this plugin.
     */
    protected $plugin_name;

    /**
     * The current version of the plugin.
     */
    protected $version;

    /**
     * @var Admin
     */
    protected $admin;

    /**
     * @var AdminRoutes
     */
    protected $adminRoutes;

    /**
     * @var Checkout
     */
    protected $checkout;

//    /**
//     * @var CustomerWidget
//     */
//    protected $customerWidget;
//
//    /**
//     * @var StagedCheckbox
//     */
//    protected $stagedCheckbox;

    /**
     * @var Base
     */
    protected $base;
    /**
     * @var Customer
     */
    private $customer;

    /**
     * Define the core functionality of the plugin.
     */
    public function __construct()
    {
        $this->base = new Base();
        $this->admin = new Admin();
        $this->adminRoutes = new AdminRoutes();
        $this->checkout = new Checkout();
//        $this->stagedCheckbox = new StagedCheckbox();
//        $this->customerWidget = new CustomerWidget();
        // rename to my account.
        $this->customer = new Customer();
        // rename to my account routes.
        $this->customerRoutes = new CustomerRoutes();
        $this->oAuth = new OAuth();
        $this->oAuthRoutes = new OAuthRoutes();

        $this->version = PFW_VERSION;
        $this->plugin_name = PFW_PLUGIN_NAME;

        $this->load_dependencies();
        $this->set_locale();

        $this->define_base_hooks();
        $this->define_admin_hooks();
//        $this->define_staged_checkbox_hooks();
//        $this->define_customer_widget_hooks();
        $this->define_checkout_hooks();
        $this->define_oauth_hooks();
        $this->define_my_account_hooks();
    }

    /**
     * Load the required dependencies for this plugin.
     */
    private function load_dependencies()
    {
        $this->loader = new Loader();
    }

    /**
     * Define the locale for this plugin for internationalization.
     */
    private function set_locale()
    {
        $plugin_i18n = new I18n();

        $this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');
    }

    /**
     *
     */
    private function define_base_hooks()
    {
        $this->loader->add_action('admin_enqueue_scripts', $this->base, 'enqueue_scripts');
    }

    /**
     * Register all of the hooks related to the admin area functionality of the plugin.
     */
    private function define_admin_hooks()
    {
        $this->loader->add_action('admin_menu', $this->admin, 'add_to_menu');
        $this->loader->add_action('admin_enqueue_scripts', $this->admin, 'enqueue_scripts');
        $this->loader->add_action('admin_enqueue_scripts', $this->admin, 'enqueue_styles');
        $this->loader->add_action('rest_api_init', $this->adminRoutes, 'register_routes');
    }
//
//    /**
//     * Register all of the hooks related to the customer widget found in the checkout form.
//     */
//    private function define_customer_widget_hooks()
//    {
//        $this->loader->add_action('wp_enqueue_scripts', $this->customerWidget, 'enqueue_scripts');
//        $this->loader->add_action('wp_enqueue_scripts', $this->customerWidget, 'enqueue_styles');
//    }

//    /**
//     * Register all of the hooks related to the staged checkbox found in the checkout form.
//     */
//    private function define_staged_checkbox_hooks()
//    {
////        $this->loader->add_action('woocommerce_checkout_update_order_meta', $this->checkout, 'save_order_meta');
//    }

    /**
     * Register all of the hooks related to the checkout functionality of the plugin.
     */
    private function define_checkout_hooks()
    {
        $this->loader->add_action('woocommerce_after_order_notes', $this->checkout, 'add_to_form');
        $this->loader->add_action('woocommerce_checkout_update_order_meta', $this->checkout, 'save_order_meta');
        $this->loader->add_action('woocommerce_payment_complete', $this->checkout, 'on_payment_completed');
        $this->loader->add_action('woocommerce_checkout_update_order_meta', $this->checkout, 'save_order_meta');
    }

    /**
     *
     */
    private function define_my_account_hooks()
    {
        $this->customer->add_customer_page();

        $this->loader->add_action('wp_enqueue_scripts', $this->customer, 'enqueue_scripts');
        $this->loader->add_action('wp_enqueue_scripts', $this->customer, 'enqueue_styles');
        $this->loader->add_action('rest_api_init', $this->customerRoutes, 'register_routes');
    }

    /**
     *
     */
    private function define_oauth_hooks()
    {
        $this->oAuth->add_callback_page();
        $this->loader->add_action('rest_api_init', $this->oAuthRoutes, 'register_routes');
    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     */
    public function run()
    {
        $this->loader->run();
    }

    /**
     * Just a view getters, nothing to special.
     */
    public function getPluginName()
    {
        return $this->plugin_name;
    }

    /**
     * @return Loader
     */
    public function getLoader()
    {
        return $this->loader;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }


    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return CustomerRoutes
     */
    public function getCustomerRoutes()
    {
        return $this->customerRoutes;
    }

    /**
     * @return OAuth
     */
    public function getOAuth()
    {
        return $this->oAuth;
    }

    /**
     * @return OAuthRoutes
     */
    public function getOAuthRoutes()
    {
        return $this->oAuthRoutes;
    }

    /**
     * @return Admin
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * @return AdminRoutes
     */
    public function getAdminRoutes()
    {
        return $this->adminRoutes;
    }

    /**
     * @return Checkout
     */
    public function getCheckout()
    {
        return $this->checkout;
    }

    /**
     * @return Base
     */
    public function getBase()
    {
        return $this->base;
    }
}