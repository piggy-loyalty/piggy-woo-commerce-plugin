<?php

namespace PFW\Includes\Core;
defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class Upgrade
 * @package PFW\Includes\Core
 */
class Upgrade
{
    public static function upgrade($upgrader_object, $options) {
        $current_plugin_path_name = plugin_basename( __FILE__ );

        if ($options['action'] == 'update' && $options['type'] == 'plugin' ){
            foreach($options['plugins'] as $each_plugin){
                if ($each_plugin==$current_plugin_path_name){
                    Activate::run_migrations_up();
                }
            }
        }
    }
}