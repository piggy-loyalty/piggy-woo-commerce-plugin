<?php

namespace PFW\Includes\Core;
use Exception;
use PFW\Includes\Exceptions\DatabaseException;
use WP_Error;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class DeactivateService
 * @package PFW\Includes\Core
 */
class DeactivationService
{

    /**
     *
     */
    public function deactivate() {
        $this->clearTables();
        $this->runMigrationsDown();
    }

    /**
     *
     */
    public function clearTables() {
        global $wpdb;

        $table_name = $wpdb->prefix . PFW_SLUG . "_migrations";

        $resultQuery = $wpdb->query("DROP TABLE IF EXISTS {$table_name}");
    }

    /**
     *
     */
    public function runMigrationsDown() {
        $files = scandir(PFW_PLUGIN_DIR . "/includes/Migrations");
        $files = array_slice($files,2);

        $classNames = [];
        foreach($files as $file) {
            $file = explode(".", $file);
            $classNames[] = $file[0];
        }

        foreach($classNames as $className) {
            $class = "PFW\Includes\Migrations\\" . $className;
            if(class_exists($class)) {
                $class = new $class;
                $class->down();
            }
        }
    }
}