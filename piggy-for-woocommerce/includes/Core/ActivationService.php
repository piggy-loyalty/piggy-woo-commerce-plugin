<?php

namespace PFW\Includes\Core;

use PFW\Includes\Exceptions\ActivationException;
use PFW\Includes\Exceptions\CompatibilityException;
use PFW\Includes\Exceptions\DatabaseException;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Class ActivationService
 * @package PFW\Includes\Core
 */
class ActivationService
{
    /**
     * @throws ActivationException
     * @throws CompatibilityException
     * @throws DatabaseException
     */
    public function activate()
    {
        $this->compatibilityCheck();

        $this->initMigrationsDb();

        $this->runMigrationsUp();
    }

    /**
     * @throws CompatibilityException
     */
    private function compatibilityCheck()
    {
        if(version_compare(phpversion(), PFW_MIN_PHP_VERSION, "<"))
        {
            throw new CompatibilityException('Please update your PHP version. Piggy for WooCommerce is compatible from ' . PFW_MIN_PHP_VERSION . ' and above.');
        }

        $isActive = is_plugin_active( 'woocommerce/woocommerce.php');

        if (is_wp_error($isActive)) {
            throw new CompatibilityException(implode(' - ', $isActive->get_error_messages()));
        }

        if(!$isActive) {
            throw new CompatibilityException('The WooCommerce plugin needs to be activated to start this plugin.');
        }

        global $woocommerce;
        if(version_compare($woocommerce->version, PFW_MIN_WOOCOMMERCE_VERSION, "<")) {
            throw new CompatibilityException('Piggy for WooCommerce is only compatible from ' . PFW_MIN_WOOCOMMERCE_VERSION . ' and above, to continue please update your WooCommerce version.');
        }
    }

    /**
     * @throws ActivationException
     * @throws DatabaseException
     */
    public function initMigrationsDb()
    {
        global $wpdb;

        $table_name = $wpdb->prefix . PFW_SLUG . "_migrations";

        $query = "CREATE TABLE `$table_name` (
          `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
          `executed_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
          PRIMARY KEY (`version`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        $resultFromQuery = dbDelta( $query );
        if(is_wp_error($resultFromQuery)) {
            throw new DatabaseException("Something went wrong in the database process: Query resulted in error.");
        }

        if(!array_key_exists("`$table_name`", $resultFromQuery)) {
            throw new ActivationException("Something went wrong in the activation process: Table creation failed.");
        }
    }

    /**
     * @throws ActivationException
     */
    public static function runMigrationsUp()
    {
        global $wpdb;

        $table_name = $wpdb->prefix . PFW_SLUG . "_migrations";

        $files = scandir(PFW_PLUGIN_DIR . "/includes/Migrations");
        $files = array_slice($files,2);

        $classNames = [];
        foreach($files as $file) {
            $file = explode(".", $file);
            $classNames[] = $file[0];
        }

        $migrationsFromDb = $wpdb->get_col("SELECT version FROM {$table_name}");

        foreach($classNames as $className) {
            $class = "PFW\Includes\Migrations\\" . $className;
            $migrationVersion = $className;

            if(!in_array($migrationVersion, $migrationsFromDb)) {
                if(class_exists($class)) {
                    $class = new $class;
                    $class->up();

                    $wpdb->insert(
                        $table_name,
                        array(
                            'version' => $migrationVersion,
                            'executed_at' => current_time('mysql'),
                        )
                    );
                }
            }
        }

        $migrationsFromDbCount = $wpdb->get_var("SELECT COUNT(*) FROM $table_name");
        if(!$migrationsFromDbCount == count($files)) {
            throw new ActivationException("Something went wrong in the activation process: run migrations failed.");
        }
    }
}