<?php

namespace PFW\Includes\Core;
defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

class I18n
{
    public function load_plugin_textdomain()
    {

        load_plugin_textdomain(
            'piggy-for-woocommerce',
            false,
            dirname(dirname(plugin_basename(__FILE__))) . '/languages/'
        );

    }
}