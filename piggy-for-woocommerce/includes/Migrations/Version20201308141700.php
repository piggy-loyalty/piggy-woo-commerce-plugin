<?php

namespace PFW\Includes\Migrations;
defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

class Version20201308141700
{
    public function up()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . PFW_SLUG . "_oauth_tokens";
        $query = "CREATE TABLE `$table_name` (
            `id` INT NOT NULL AUTO_INCREMENT,
            `user_id` INT NOT NULL UNIQUE,
            `access_token` VARCHAR(1000) NOT NULL,
            `refresh_token` VARCHAR(1000) NOT NULL,
            `expires_in` INT NOT NULL,
            `updated_at` DATETIME NOT NULL DEFAULT NOW(),
            `created_at` DATETIME NOT NULL DEFAULT NOW(),
            KEY `wp_users` (`user_id`) USING BTREE,
            PRIMARY KEY (`id`))";
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $query );
        
        add_option(PFW_SLUG . "_client_id");
        add_option(PFW_SLUG . "_client_secret");
    }

    public function down()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . PFW_SLUG . "_oauth_tokens";
        $wpdb->query("DROP TABLE IF EXISTS $table_name");
    }
}