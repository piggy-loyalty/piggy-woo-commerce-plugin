<?php

namespace PFW\Includes\Migrations;
defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

class Version20200810122300
{
    public function up()
    {
        add_option("pfw_client_token");
        add_option("pfw_shop_id");

        $checkboxLayout = [
            "title" => "Piggy",
            "titleColor" => "#ff4800",
            "subtitle" => "Yes, I would like to save piggy points",
            "subtitleColor" => "#000000",
        ];

        update_option(  PFW_SLUG . "_user_flow_option", "optional");
        update_option( PFW_SLUG . "_allow_linking", true);
        update_option( PFW_SLUG . "_guest_flow_option", "optional");
        update_option( PFW_SLUG . "_checkbox_layout", $checkboxLayout);
        update_option( PFW_SLUG . "_flushed_rewrite_rules", false);
        update_option(PFW_SLUG . "_credits_include_taxes", true);
        update_option(PFW_SLUG . "_credits_include_shipping_cost", true);
    }

    public function down()
    {
        delete_option(PFW_SLUG . "_client_token");
        delete_option(PFW_SLUG . "_shop_id");
        delete_option( PFW_SLUG . "_user_flow_option");
        delete_option( PFW_SLUG . "_allow_linking");
        delete_option( PFW_SLUG . "_guest_flow_option");
        delete_option( PFW_SLUG . "_checkbox_layout");
    }
}