<?php

namespace PFW\Includes\Migrations;
defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

class Version20201011201300
{
    public function up()
    {
        update_option(PFW_SLUG . "_language", "nl");
    }

    public function down()
    {
        delete_option(PFW_SLUG . "_language");
    }
}