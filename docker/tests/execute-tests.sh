#!/bin/sh

# Go to wordpress test folder.
cd /usr/src/wordpress/wp-content/plugins/piggy-for-woocommerce

# Execute phpunit to run the tests
./vendor/bin/phpunit

cd /usr/src/wordpress/wp-content/plugins/piggy-for-woocommerce/public

# Execute frontend tests
#yarn test

# This is only for development purposes.
php-fpm
