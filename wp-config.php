<?php
define('WP_CACHE', true); // WP-Optimize Cache
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );
/** MySQL database username */
define( 'DB_USER', 'root' );
/** MySQL database password */
define( 'DB_PASSWORD', 'root' );
/** MySQL hostname */
define( 'DB_HOST', 'dbserver' );
/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );
/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '-Ec[8.BOdYKLNEERNhg13UsN[{7=6pGh@/m{neyw-*9NBv]8b!Hk[jw2mylZH+%G' );
define( 'SECURE_AUTH_KEY',  'p po$)cS<mi{.EvU]DE3RDRk=igX-`w{{-=~IYRHoQ.I*<v]<_?O?]z g4v&~ozr' );
define( 'LOGGED_IN_KEY',    '(Vs$_kI23:^tvQo*s@fX@CX=0lHx4zRg`v7B5r{#8W[Wohy-:`]_baYc/Z3B|>l4' );
define( 'NONCE_KEY',        '6CIXG&zj3I*[#,(T(w$AX%aT+Q8k/[:yS-e9AGdqoV)T*&E]!B4Cw4fVIt5,/V@u' );
define( 'AUTH_SALT',        '^< |+2yF$KcBf6Aw.)9.,RK-nkMREXV6<)7VZ+[Nih4#s>s5M^Z+#PQiaA5Kd54]' );
define( 'SECURE_AUTH_SALT', '2o2<SiY >9c|{(Y;h,Oea0pSwi;;vAb-g#tHOLI?$$DLWj2m?(~KrLIxk#Yo3u6A' );
define( 'LOGGED_IN_SALT',   'k?Yv>^<L5vbmm@|Mg6Vy]Wx&UC;H 9(F*GySc-a$)dC`^:AMdSZKjw0Yz`e7!KN$' );
define( 'NONCE_SALT',       ',3i#jhIabkaLNIs@i2:1$MV.U+FWbD7fv4]vg~@:!rqR%bbpH#xGc<~{U}I@d)->' );
/**#@-*/
/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';
/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_LOG', true );
/* That's all, stop editing! Happy publishing. */
/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}
/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';